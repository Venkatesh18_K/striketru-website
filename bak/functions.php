<?php
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'themejunkie', TEMPLATEPATH . '/languages' );	

// Load functions and Widgets.
require_once(TEMPLATEPATH . '/functions/meta-box.php');
require_once(TEMPLATEPATH . '/functions/comments.php');
require_once(TEMPLATEPATH . '/functions/contact-form.php');
require_once(TEMPLATEPATH . '/functions/theme-options.php');
require_once(TEMPLATEPATH . '/functions/flickr-widget.php');
require_once(TEMPLATEPATH . '/functions/category-widget.php');

if (function_exists('create_initial_post_types')) create_initial_post_types(); //fix for wp 3.0

if (function_exists('add_post_type_support')) add_post_type_support( 'page', 'excerpt' );

// Add menus support
add_action( 'init', 'tj_register_my_menu' );
function tj_register_my_menu() {
   register_nav_menus(
      array(
         'header-pages' => __( 'Header Pages', 'themejunkie' ),
      )
   );
}


// Custom Conditional Tags
function tj_is_folio_cat($cid = false) {
	if($cid) 
		$cat = $cid;
	else
		$cat = get_query_var('cat');
	
	$folio_cat_ids = get_theme_mod('folio_cats');
	$folio_cat_arr = explode(',',$folio_cat_ids);
	
	if(in_array($cat,$folio_cat_arr)) {
		return true;
	} else {
	
		foreach($folio_cat_arr as $folio_cat) {
			if(cat_is_ancestor_of($folio_cat,$cat))
			return true;
			break;
		}
	}
	
}

function tj_in_folio_cat($pid = '') {
	global $post;		
	$cats = get_the_category($pid);
	
	foreach($cats as $cat) {
		$folio_cat_ids = get_theme_mod('folio_cats');
		$folio_cat_arr = explode(',',$folio_cat_ids);
					
		if(in_array($cat->cat_ID,$folio_cat_arr)) {
			return true;
			break;
		}
	}
}


// Filter to new excerpt length
function tj_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'tj_excerpt_length' );

// Filter to new excerpt more text
function tj_excerpt_more($post) {
	return '... <a class="meta-more" href="'. get_permalink($post->ID) . '">'.__('Read more <span class="meta-nav">&raquo;</span>','themejunkie').'</a>';
}
add_filter('excerpt_more', 'tj_excerpt_more');

// Filter to fix first page redirect
add_filter('redirect_canonical', 'fixpageone');

function fixpageone($redirect_url) {
	if(get_query_var('paged') == 1)
		$redirect_url = '';
	return $redirect_url;
}

// Tests if any of a post's assigned categories are descendants of target categories
function post_is_in_descendant_category( $cats, $_post = null ) {
	foreach ( (array) $cats as $cat ) {
		// get_term_children() accepts integer ID only
		$descendants = get_term_children( (int) $cat, 'category');
		if ( $descendants && in_category( $descendants, $_post ) )
			return true;
		}
	return false;
}

// Register Widgets
function tj_widgets_init() {

	// Blog Sidebar
	register_sidebar( array (
		'name' => __( 'Blog Sidebar', 'themejunkie' ),
		'id' => 'blog-sidebar',
		'description' => __( 'The blog widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Portfolio Sidebar
	register_sidebar( array (
		'name' => __( 'Portfolio Sidebar', 'themejunkie' ),
		'id' => 'folio-sidebar',
		'description' => __( 'The portfolio widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Page Sidebar
	register_sidebar( array (
		'name' => __( 'Page Sidebar', 'themejunkie' ),
		'id' => 'page-sidebar',
		'description' => __( 'The page widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 1
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 1', 'themejunkie' ),
		'id' => 'footer-widget-area-1',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 2
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 2', 'themejunkie' ),
		'id' => 'footer-widget-area-2',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 3
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 3', 'themejunkie' ),
		'id' => 'footer-widget-area-3',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 3
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 4', 'themejunkie' ),
		'id' => 'footer-widget-area-4',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'init', 'tj_widgets_init' );

function myFilter($query) {
	$paged = get_query_var('paged') ? get_query_var('paged'):1;
	
	$folio_showposts = get_theme_mod('folio_num');
	$folio_cat_ids = get_theme_mod('folio_cats');
	$folio_cat_arr = explode(',',$folio_cat_ids);
	
    if (is_search()) {
        $query->set('category__not_in',$folio_cat_arr);
    }

	if (is_date()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
	if (is_tag()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
	if (is_feed()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
return $query;
}
add_filter('pre_get_posts','myFilter');

// Custom styles and scripts
if(!is_admin()) {
	add_action( 'wp_print_styles', 'custom_styles', 100 );
	add_action( 'wp_print_scripts', 'custom_scripts', 100 );
}
function custom_styles() {
	$color_scheme = get_theme_mod('color_scheme');
	if($color_scheme == 'Blue') 
		wp_enqueue_style('blue',get_bloginfo('template_url').'/skins/blue.css');
	elseif($color_scheme == 'Brown') 
		wp_enqueue_style('brown',get_bloginfo('template_url').'/skins/brown.css');
	elseif($color_scheme == 'Red')
		wp_enqueue_style('red',get_bloginfo('template_url').'/skins/red.css');
	elseif($color_scheme == 'Green')
		wp_enqueue_style('green',get_bloginfo('template_url').'/skins/green.css');
	elseif($color_scheme == 'Yellow')
		wp_enqueue_style('blue',get_bloginfo('template_url').'/skins/blue.css');
	elseif($color_scheme == 'Aqua')
		wp_enqueue_style('aqua',get_bloginfo('template_url').'/skins/aqua.css');
	elseif($color_scheme == 'Gray')
		wp_enqueue_style('gray',get_bloginfo('template_url').'/skins/gray.css');
	elseif($color_scheme == 'Pink')
		wp_enqueue_style('pink',get_bloginfo('template_url').'/skins/pink.css');
	elseif($color_scheme == 'Purple')
		wp_enqueue_style('purple',get_bloginfo('template_url').'/skins/purple.css');
	elseif($color_scheme == 'Orange')
		wp_enqueue_style('orange',get_bloginfo('template_url').'/skins/orange.css');
 }	
function custom_scripts() {
	wp_deregister_script( 'jquery' );
		
	wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', false, '1.4.2');
	// wp_enqueue_script('jquery', get_bloginfo('template_url').'/js/jquery-1.4.2.min.js', false, '1.4.2');
	//wp_enqueue_script('masonry-jquery', 'http://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js', true, '4.1');
	wp_enqueue_script('masonry-jquery', get_bloginfo('template_url').'/js/masonry.pkgd.min.js', true, '4.1');
	wp_enqueue_script('cufon', get_bloginfo('template_url').'/js/cufon-yui.js', true, '1.0');
	wp_enqueue_script('jquery-global', get_bloginfo('template_url').'/js/global.js', true, '1.0');

	if ( is_singular() && get_option('thread_comments') ) wp_enqueue_script( 'comment-reply' );
}

// Pagenavi
function tj_pagenavi($range = 9) {
	global $paged, $wp_query;
	if ( !$max_page ) { $max_page = $wp_query->max_num_pages;}
	if($max_page > 1){
		echo '<div class="pagenav clear">';
		if(!$paged){$paged = 1;}
		echo '<span>Page '.$paged.' / '.$max_page.'</span>';
		previous_posts_link('&laquo; Previous');
		if($max_page > $range){
			if($paged < $range){
				for($i = 1; $i <= ($range + 1); $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";

				}
			} elseif($paged >= ($max_page - ceil(($range/2)))){
				for($i = $max_page - $range; $i <= $max_page; $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";
				}
			} elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
				for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";
				}
			}
		} else {
			for($i = 1; $i <= $max_page; $i++){
				echo "<a href='" . get_pagenum_link($i) ."'";
				if($i==$paged) echo " class='current'";
				echo ">$i</a>";
			}
		}
		next_posts_link('Next &raquo;');
		echo '</div>';
	}
}

// Breadcrumb
function tj_breadcrumb() {
	 $delimiter = '/';
  $name = 'Home'; //text for the 'Home' link
  $currentBefore = '<span class="current">';
  $currentAfter = '</span>';
	// echo '<small>You are here:</small>';
 
    global $post;
    $home = get_bloginfo('url');
   
	if(is_home() && get_query_var('paged') == 0) 
		echo '<span class="home">' . $name . '</span>';
	else
		echo '<a class="home" href="' . $home . '">' . $name . '</a> '. $delimiter . ' ';
		
	if(is_category()) {
		if(tj_is_folio_cat() &&  get_theme_mod('display_folio_page_in_breadcrumb') == 'Yes') {
		$pid = get_page(get_theme_mod('folio_page'));
		echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
	  } else {
		if( get_theme_mod('display_blog_page_in_breadcrumb') == 'Yes') {
			$pid = get_page(get_theme_mod('blog_page'));
			echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
		}
	  }
	 }
	
	if(is_single()) {
	if(tj_in_folio_cat() &&  get_theme_mod('display_folio_page_in_breadcrumb') == 'Yes') {
		$pid = get_page(get_theme_mod('folio_page'));
		echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
	  } else {
		if( get_theme_mod('display_blog_page_in_breadcrumb') == 'Yes') {
			$pid = get_page(get_theme_mod('blog_page'));
			echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
		}
	  }
	}
	
    if ( is_category() ) {
	 
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore;
      single_cat_title();
      echo $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single() ) {
		if(!is_attachment()) {
      $cat = get_the_category(); 
	  $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
	  }
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search for ' . get_search_query() . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore;
      single_tag_title();
      echo $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore. $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo $delimiter. $currentBefore . __('Page') . ' ' . get_query_var('paged') . $currentAfter;
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 

}

// Get image attachment (sizes: thumbnail, medium, full)
function get_thumbnail($postid=0, $size='full') {
	if ($postid<1) 
	$postid = get_the_ID();
	$thumb_key = get_theme_mod('thumb_key');
	if($thumb_key)
		$thumb_key = $thumb_key;
	else
		$thumb_key = 'thumb';
	$thumb = get_post_meta($postid, $thumb_key, TRUE); // Declare the custom field for the image
	if ($thumb != null or $thumb != '') {
		return $thumb; 
	} elseif ($images = get_children(array(
		'post_parent' => $postid,
		'post_type' => 'attachment',
		'numberposts' => '1',
		'post_mime_type' => 'image', ))) {
		foreach($images as $image) {
			$thumbnail=wp_get_attachment_image_src($image->ID, $size);
			return $thumbnail[0]; 
		}
	} else {
		return get_bloginfo ( 'stylesheet_directory' ).'/images/default_thumb.gif';
	}
	
}

// Automatically display/resize thumbnail
function tj_thumbnail($width, $height) {
	echo '<a href="'.get_permalink($post->ID).'" rel="bookmark"><img src="'.get_bloginfo('template_url').'/timthumb.php?src='.get_thumbnail($post->ID, 'full').'&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1" alt="'.get_the_title().'" /></a>';
}

// Get limit excerpt
function tj_content_limit($max_char, $more_link_text = '', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);

   if (strlen($_GET['p']) > 0) {
      echo "";
      echo $content;
      echo "...";
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo "";
        echo $content;
        echo "...";
   }
   else {
      echo "";
      echo $content;
   }
}

// Return number of posts in a Archive Page
function tj_current_postnum() {
	global $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if(empty($paged) || $paged == 0) $paged = 1;
	if (!is_404()) 
		$begin_postnum = (($paged-1)*$posts_per_page)+1; 
	else 
		$begin_postnum = '0';
	if ($paged*$posts_per_page < $numposts) 
		$end_postnum = $paged*$posts_per_page; 
	else 
		$end_postnum = $numposts;
	$current_page_postnum = $end_postnum-$begin_postnum+1;
	return $current_page_postnum;
}

// begin customization
// used by Downloads and Download Collateral pages to send an email identifying the specific collateral requested
function add_query_vars_filter($vars) {
  $vars[] = 'assetid';	//ID of asset (case study, etc) to download
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

//Kinjal menu customization
function prefix_nav_description( $item_output, $item, $depth, $args ) {
if ( !empty( $item->description ) ) {
$item_output = str_replace( $args->link_after . '</a>', '<p class="menu-item-description">' . $item->description . '</p>' . $args->link_after . '</a>', $item_output );
}
return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );
// end customization



?>
