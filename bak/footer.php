	</div><!-- end #main -->
	</div><!-- end #container .inner -->
	</div><!-- end #container -->
  
	<div id="fwrap">
	<div class="inner">
		<div class="footer-nav-row">
			<div class="inner-content">
				<div id="footbar">
					<?php if(get_theme_mod('footer_contact_status') == 'Yes') {?>
						<div class="fwidget" id="footer-widget-contact">
							<h3 class="fwidget-title"><?php echo get_theme_mod('footer_contact_title'); ?></h3>
							
							<?php
								$address = get_theme_mod('address');
								$phone = get_theme_mod('phone');
								$fax = get_theme_mod('fax');
								$email = get_theme_mod('email');
								
							?>
							<div class="contact-list">
								<div class="phone-num"><?php echo $phone; ?></div>	
								<div class="timings"><?php echo $fax; ?></div>
								<div class="mail-to"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
							</div>
							<!--<dl class="contact-list">
								<?php if($address) { ?><dt>Address:</dt><dd><?php echo wpautop($address); ?></dd><?php } ?> 
								<?php if($phone) { ?><dt>Phone:</dt><dd><?php echo $phone; ?></dd><?php } ?>
								 <?php if($fax) { ?><dt>Fax:</dt><dd><?php echo $fax; ?></dd><?php } ?> 
								<?php if($email) { ?><dt>Email:</dt><dd><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></dd><?php } ?>
							</dl>
							<div class="akeneo-silver-logo">
								<img class="" src="/wp-content/uploads/2016/09/akeneo_sliver_partner_h.png" alt="akeneo silver parter" />
							</div>
							 -->
						</div><!-- end #footer-widget-contact -->
						<?php } ?>

					<div class="footer-nav-links">
						<div class="fwidget-area" id="fwidget-area-4">
							<?php if ( is_active_sidebar( 'footer-widget-area-4' ) ) :  dynamic_sidebar( 'footer-widget-area-4'); endif; ?>
						</div>

						<div class="fwidget-area" id="fwidget-area-1">
							<?php if ( is_active_sidebar( 'footer-widget-area-1' ) ) :  dynamic_sidebar( 'footer-widget-area-1'); endif; ?>
						</div>
				
						<div class="fwidget-area" id="fwidget-area-2">
							<?php if ( is_active_sidebar( 'footer-widget-area-2' ) ) :  dynamic_sidebar( 'footer-widget-area-2'); endif; ?>
						</div>
						
						<div class="fwidget-area" id="fwidget-area-3">
							<?php if ( is_active_sidebar( 'footer-widget-area-3' ) ) :  dynamic_sidebar( 'footer-widget-area-3'); endif; ?>
						</div>
					</div><!-- end .footer-nav-links-->
				</div><!--end #footbar -->
	  		</div><!--end .inner-content -->
		</div><!-- end .footer-nav-row -->
		
		<div class="footer-social-row">
			<div class="inner-content">
				<div class="fwidget-area" id="fwidget-area-5">
		            <!-- Begin MailChimp Signup Form -->
		            <link rel="stylesheet" type="text/css" href="http://www.striketru.com/wp-content/themes/cubelight/classic-081711.css" />
		            <div id="mc_embed_signup">
		            <form action="//striketru.us12.list-manage.com/subscribe/post?u=9fed41bdf95025e316d39574f&amp;id=41191afea4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		                <div id="mc_embed_signup_scroll">
		                <h3>Sign Up for PIM News &amp; Insights</h3>
		            <div class="mc-field-group">
		                <input type="email" value="" name="EMAIL" class="required email" placeholder="Email Address" id="mce-EMAIL">
		            </div>
		                <div id="mce-responses" class="clear">
		                    <div class="response" id="mce-error-response" style="display:none"></div>
		                    <div class="response" id="mce-success-response" style="display:none"></div>
		                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9fed41bdf95025e316d39574f_41191afea4" tabindex="-1" value=""></div>
		                <div class="clear"><input type="submit" value="JOIN" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
		                </div>
		            </form>
		            </div>
		            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
		            <!--End mc_embed_signup-->
				</div>
				<div class="social-block">
					<?php 
						$twitter_url = get_theme_mod('twitter_url');
						$facebook_url = get_theme_mod('facebook_url');
						$linkedin_url = get_theme_mod('linkedin_url');
						$flickr_url = get_theme_mod('flickr_url');
						$rss_url = get_theme_mod('rss_url');
					?>
								
					<ul class="social-list">
						<?php if($rss_url) { ?><li class="foot-rss"><a target=_blank href="<?php echo $rss_url; ?>">RSS Feed</a></li><?php } ?>
						<?php if($flickr_url) { ?><li class="foot-flickr"><a target=_blank href="<?php echo $flickr_url; ?>">Flickr</a></li><?php } ?>
						<?php if($linkedin_url) { ?><li class="foot-linkedin"><a target=_blank href="<?php echo $linkedin_url; ?>">Linkedin</a></li><?php } ?>
						<?php if($facebook_url) { ?><li class="foot-facebook"><a target=_blank href="<?php echo $facebook_url; ?>">Facebook</a></li><?php } ?>
						<?php if($twitter_url) { ?><li class="foot-twitter"><a target=_blank href="<?php echo $twitter_url; ?>">Twitter</a></li><?php } ?>
					</ul>
				</div>
			</div><!--end .footer-social-row .inner-content -->
		</div><!--end .footer-social-row -->
		<div class="footer-rights-row">
			<div class="inner-content">
				<p>
					All content &copy; <a href="<?php bloginfo('url'); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"  style="color: rgb(0,255,0)"><font color="FF8000"><?php bloginfo( 'name' ); ?>, LLC</font></a>, 2013-2019. 
				</p>
				<span class="footer-terms-nav">
					<!-- <a class="term-link" href="">Sitemap</a><span>|</span> 
					<a class="term-link" href="">Terms of Use</a><span>|</span>
					<a class="term-link" href="">Privacy Policy</a> -->
				</span>
			</div><!--end .footer-rights-row .inner-content -->
		</div><!--end .footer-rights-row -->
	
	</div><!--end #frwap .inner -->
	</div><!--end #frwap -->
	
</div><!--end #wrapper -->

<!--begin of body code-->	
<?php if(get_theme_mod('body_code_status') == "Yes") echo stripslashes(get_theme_mod('body_code')); ?>
<!--end of body code-->

<?php wp_footer(); ?>

</body>
</html>
