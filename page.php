<?php 
	if(is_page(get_theme_mod('blog_page'))) {
		include(TEMPLATEPATH. '/blog.php');
	} elseif(is_page(get_theme_mod('folio_page'))) {
		include(TEMPLATEPATH. '/portfolio.php');
	} elseif(is_page(get_theme_mod('contact_page'))) {
		include(TEMPLATEPATH. '/contact.php');
} else { ?>

<?php get_header(); ?>
		
	<div id="content">
		<div class="inner-content">
			<?php the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<!--<h1 class="page-title"><?php the_title(); ?></h1>-->

				<div class="entry entry-content">
					<?php the_content(); ?>

					<div class="conactus-form">
					<?php 
					if (is_page('request-demo')) {
					    echo tj_demo_form();
					} elseif (is_page('akeneo-trial')) {
					    echo tj_trial_form();
					} elseif (is_page('free-consultation')) {
					    echo tj_consultation_form();
					} elseif (is_page('download-collateral')) {
					    echo tj_download_collateral_form();
					}
					?>
					</div>

					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'themejunkie' ), 'after' => '</div>' ) ); ?>
				</div> <!--end .entry-->
				
			</div> <!--end #post-->
		</div><!--end .inner-content-->
	</div><!--end #content-->
	

<?php get_sidebar(); ?> 
<?php get_footer(); ?>

<?php } ?>
