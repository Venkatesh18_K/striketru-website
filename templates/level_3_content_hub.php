<?php
/* Template Name: 3 Level Content Hub Template */

get_header();
session_start();
session_unset();
$_SESSION['page_name_lavel_3'] = get_the_title(get_the_ID());
$_SESSION['page_name_lavel_3_id'] = get_the_ID();
?>

<div class='page_content_main product_data_cha_main entry'>
	<div class="container_custom">
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} 
} 
?>
</div>
</div>
<?php get_footer(); ?>