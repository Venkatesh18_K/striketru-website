<?php
	/* Template Name: Blog Listing*/
get_header();
 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>


<div class="post_filter_main">
    <div class="container_custom">
    <div class="row">
    <div class="col_12 top_column">
        <div class="search_main search_form_quality">
            <input type="hidden" name="surrent_page" id="current_page" value="<?php echo get_the_permalink(65);?>">
            <input type="text" placeholder="Search" name="s" id="keyword" class="search_input">
            <button type="button" class="search_btn" id="keyword_search">Submit</button>
        </div>
        <div class="shorting blog_short_cat">
                <?php $terms = get_terms(
                    array(
                        'taxonomy'   => 'blog-category',
                        'hide_empty' => false,
                    )
                ); ?>
                <label class="main_title">Sort by Category</label>
                <select id='testSelect1' class="cat_select_main" multiple>
                    <?php foreach ( $terms as $term ) { ?>
                <option value='<?php echo $term->term_id; ?>'><span class='checkmark'></span><?php echo $term->name; ?></option>
                <?php } ?>
                </select>
        </div>
    </div>
    </div>
    </div>
</div>

<div class="col-md-12 content blog_middle_sec">
    <div class = "inner-box content no-right-margin darkviolet">
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            document.multiselect('#testSelect1').deselectAll();
            $(".multiselect-input").attr("placeholder", "Select");
            $(".multiselect-input").attr('readonly',true);
            $('#testSelect1_itemList li .multiselect-text').prepend('<span class="checkmark"></span>')
            $("#testSelect1 > option").attr("selected",false);
            // This is required for AJAX to work on our page
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

            function cvf_load_all_posts(page){
                $(".cvf_pag_loading").fadeIn().css('background','#fff');
                var data = {
                  // keyword: jQuery('#keyword').val(),
                    page: page,
                    action: "demo-pagination-load-posts"
                };

                $.post(ajaxurl, data, function(response) {
                    $(".cvf_universal_container").empty().append(response);
                        var tot_record=$('#total_pages').val();
                        if(page == 1){
                            $('.pre_arrow').css('display','none');
                        }

                        if(page == tot_record){
                            $('.next_arrow').css('display','none');
                        }
                    $(".cvf_pag_loading").css({'background':'none', 'transition':'all 1s ease-out'});
                    $(window).scrollTop(0); 
                });
            }
              

            cvf_load_all_posts('<?php echo $paged; ?>');
            $('.cvf_universal_container .cvf-universal-pagination li.active').live('click',function(){
                var page = $(this).attr('p');
                cvf_load_all_posts(page);
                var up_url=$('#current_page').val();
                if(page!=1){
                var up_url=up_url+'page/'+page;
                
                }
                window.history.pushState("object or string", "Title", up_url);

            });

            $('#keyword').keyup( function() {
                document.multiselect('#testSelect1').deselectAll();
                    $(".multiselect-input").val("Select");
                    $(".multiselect-checkbox").attr("checked",false);
                    $("#testSelect1 > option").attr("selected",false);

                     $(".cvf_pag_loading").fadeIn().css('background','#fff');

                     var data = {
                    keyword: $('#keyword').val(),
                    page: 1,
                    action: "demo-pagination-load-posts"
                };

                // Send the data
                $.post(ajaxurl, data, function(response) {
                    $(".cvf_universal_container").empty().append(response);
                    $(".cvf_pag_loading").css({'background':'none', 'transition':'all 1s ease-out'});
                     var up_url=$('#current_page').val();
                        var up_url=$('#current_page').val();
                        window.history.pushState("object or string", "Title", up_url);
                });

            });
            
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

var blog = getUrlParameter('cat');

                    $("input[data-val="+blog+"]:checkbox").attr('checked',true);    

                    $('#testSelect1_itemList li').change(function(){
                 $('#keyword').val('');
                $(".cvf_pag_loading").fadeIn().css('background','#fff');
                      var cat_list=$('#testSelect1').val();
                     
                      var list_cat='';
                     if(cat_list){
                        list_cat=cat_list;
                    }else{
                        list_cat=0;
                    }
                    var data = {
                    term_slug:  list_cat,
                     page: 1,
                     action: "demo-pagination-load-posts"
                };
                $.post(ajaxurl, data, function(response) {
                    $(".cvf_universal_container").empty().append(response);
                  $(".cvf_pag_loading").css({'background':'none', 'transition':'all 1s ease-out'});
                  var up_url=$('#current_page').val();
                        var up_url=$('#current_page').val();
                        var up_url=up_url;
                        window.history.pushState("object or string", "Title", up_url);

              });

              
            });

            //setInterval(function(){ jQuery(".multiselect-input").val("Select"); }, 10);
    

            

        }); 
        </script>
        

    <div class="cvf_pag_loading">
        <div class="cvf_universal_container">
            <div class="cvf-universal-content"></div>
        </div>
    </div>


    </div>      
</div>



<?php get_footer(); ?>