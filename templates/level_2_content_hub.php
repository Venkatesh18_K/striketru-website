<?php
/* Template Name: 2 Level Content Hub Template */

get_header();

session_start();
session_unset();
$_SESSION['page_name_lavel_2'] = get_the_title(get_the_ID());
$url = $_SERVER['HTTP_REFERER'];
$post_id= url_to_postid( $url );
$page_title = get_the_title($post_id);
$_SESSION['page_name_lavel_2_url'] = get_permalink(get_the_ID());
?>

<div class='page_content_main product_data_cha_main entry'>
	<div class="container_custom">
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} 
} 
?>
</div>
</div>
<?php get_footer(); ?>