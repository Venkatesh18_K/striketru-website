<?php
/*
 * Template Name:news-blog-template
 */
get_header();
session_unset();
?>

<div class="post_filter_main">
    <div class="container_custom">
        <div class="row">
            <div class="col_12 ">
                <input type="hidden" name="current_url" id="currenturl" value="<?php echo get_the_permalink(65); ?>">
                <div name="bloglist" id="bloglist" class="top_column">
                    <div class="search_main search_form_quality">

                        <input type="text" placeholder="Search" name="search_key" id="keyword" class="search_input" value="<?php echo $_GET['search_key']; ?>">
                        <input type="submit" class="search_btn" id="keyword_search">
                    </div>
                    <div class="shorting blog_short_cat">
                        <?php
                        $terms = get_terms(
                                array(
                                    'taxonomy' => 'blog-category',
                                    'hide_empty' => true,
                                )
                        );
                        ?>
                        <label class="main_title">Sort by Category</label>
					
						<select class="cat_selection" id="cat_selection" multiple="multiple">
								<?php
                            if ($_GET['cat_list']) {
                                $cat_list = $_GET['cat_list'];
                                $cat_list = explode(",", $cat_list);
                            }
                            foreach ($terms as $term) {
                                ?>
								<option value='<?php echo $term->term_id; ?>' <?php   if ($_GET['cat_list']) { if(in_array($term->term_id,$cat_list)){echo 'selected';} } ?> ><?php echo $term->name; ?></option>
					<?php } ?>
       
						</select>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product_cat_main blog_cat_main">
    <div class="container_custom">
        <div class="row">
            <?php
			$all_term=array();
    $terams=get_terms('blog-category');
    foreach($terams as $term){
        $all_term[]=$term->term_id;
    }
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            if ($cat_list) {
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => get_option('posts_per_page'),
                    's' => $_GET['search_key'],
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'blog-category',
                            'field' => 'term_id',
                            'terms' => $cat_list
                        )
                    ),
                );
            } else {
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 6,
                    's' => $_GET['search_key'],
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
					'tax_query' => array(
                        array(
                            'taxonomy' => 'blog-category',
                            'field' => 'term_id',
                            'terms' => $all_term
                        )
                    ),
                );
            }
            $the_query = new WP_Query($args);
            if ($the_query->have_posts()){
				while ($the_query->have_posts()) 
				{
				$the_query->the_post();
                    global $post;
                    $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
                    $image = wp_get_attachment_image_src($post_thumbnail_id, 'post-feature');
                    if (!$post_thumbnail_id) {
                        $image[0] = get_template_directory_uri() . '/images/default_image-353x178.png';
                    }
                    ?>

                    <div class="col_4">
                        <div class="blog_category_list">
                            <div class="cat_img">
                               <a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>"></a>
                            </div>
                            <div class="cat_content">
                                <a href="<?php the_permalink(); ?>"><h4 class="font16"><?php the_title(); ?></h4></a>
                                
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <?php
				}
            }
			else{
				echo "No post found.";
			}
            ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
    <div class="container_custom"> 

        <div class="cvf-universal-pagination">
            <?php
            $total_pages = $the_query->max_num_pages;
            if ($total_pages > 1) {
                $current_page = max(1, get_query_var('paged'));
                $pagenum_link = trailingslashit($url_parts[0]) . '%_%';
                $si = get_site_url() . '/blog';
                echo paginate_links(array(
                    'base' => $si . $pagenum_link,
                    'format' => 'page/%#%',
                    'current' => $current_page,
                    'total' => $total_pages,
                    'type' => 'list',
                    'prev_text' => __('<svg xmlns="http://www.w3.org/2000/svg" width="6" height="13" viewBox="0 0 6 13"><g><g><path fill="#1a1a1a" d="M.093 6.768l5.37 6.14c.127.144.33.144.457 0a.405.405 0 0 0 0-.521L.778 6.508 5.92.63a.405.405 0 0 0 0-.521A.305.305 0 0 0 5.693 0a.296.296 0 0 0-.227.109l-5.37 6.14a.404.404 0 0 0-.003.519z"/></g></g></svg>'),
                    'next_text' => __('<svg xmlns="http://www.w3.org/2000/svg" width="6" height="13" viewBox="0 0 6 13"><g><g><path fill="#1a1a1a" d="M5.907 6.232L.537.092a.294.294 0 0 0-.457 0 .405.405 0 0 0 0 .521l5.142 5.879L.08 12.37a.405.405 0 0 0 0 .52c.062.072.146.11.227.11a.296.296 0 0 0 .227-.11l5.37-6.14a.404.404 0 0 0 .003-.518z"/></g></g></svg>'),
                ));
            }
            ?>  
        </div>
    </div>
</div>
<script type="text/javascript">

    $(function() {
        window.fs_test = $('.cat_selection').fSelect();
    });

// Enter Press Search JS
$(document).keypress(function(event){
    var get_val=$('.search_input').val();
    if(get_val != ''){
    var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('#keyword_search').click();   
        }
    }
});



    $(document).ready(function ($) {

              $('#cat_selection').change(function () {
            
            var cat_list = $('#cat_selection').val();
            var cat_list = cat_list.join();
                var keyword = $('#keyword').val();
                var url = window.location.href;
                var params = { 'cat_list':cat_list,'search_key':keyword};
                var url = $('#currenturl').val()+'?' + jQuery.param(params) ;
          
            window.location = url;
        });


    });
    $('#keyword_search').click(function () {
        
          var cat_list = $('#cat_selection').val();
            var cat_list = cat_list.join();
                var keyword = $('#keyword').val();
                var url = window.location.href;
                var params = { 'cat_list':cat_list,'search_key':keyword};
                var url = $('#currenturl').val()+'?' + jQuery.param(params) ;
          
            window.location.href = url;
    });

</script>

<?php get_footer(); ?>