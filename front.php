<?php get_header(); ?>
<style type="text/css">
	@media screen(max-width: 737px){
		#main #content { margin-top: 0 !important;}
	}
</style>
	<div id="content" style="margin-top:23px;">

<!-- Add About Image 19-03-20 -->

<?php $banner_img = get_template_directory_uri() . '/images/Artwork.png'; ?>

<div id="home-about" class="home_about_img_sec home-slider-main">
        <div class="inner-content-slider">
             <div class="_slider style-slider">
			    <a href="#" class="_slider_next">&#10095;</a>
			    <a href="#" class="_slider_prev">&#10094;</a>
			    <ul>
			    	<li class="slide_1_home">
			        	<img src="/wp-content/uploads/home/main-banner-bg.png"/>
					<div class="banner-content">
						<div class="banner-text">
							<h2>
								Affordable PIM 
								for Ecommerce
							</h2>
							<p>
								We help businesses realize the potential of 
								product data & get on the fastest path 
								to digital commerce success.
							</p>
							<a href="/request-demo/">request demo</a>
						</div>
						<div class="banner-image">
							<img src="/wp-content/uploads/home/striketru-banner-image.png"/>
						</div>
					</div>
			        </li>
			      
			    </ul>  

			</div>
           
            
            </div>
    </div>
<!-- New Section  06-28-2021 (Manage Product Data)-->
	<div class="manage-proddata">	
		<div class="manage-process">
			<div class="process-step">
				<div>
					<img src="/wp-content/uploads/home/collect1.png" />
					<h3 class="process-title">Collect</h3>
					<h3 class="process-desc">
						Collect product data required to market and sell products to customers. 
						Onboard cleansed and normalized data into internal enterprise systems.
					</h3>
				</div>
				<h2 class="process-btn"><a href="/product-data-collection/">Product Data Collection</a></h2>
			</div>
			<div class="process-step">
				<div>
					<img src="/wp-content/uploads/home/manage1.png" />
					<h3 class="process-title">Manage</h3>
					<h3 class="process-desc">
						Repair and enrich product data, create high-quality and consistent 
						content to publish on multiple sales and marketing channels. Update data based on changes initiated by suppliers.
					</h3>
				</div>
				<h2 class="process-btn"><a href="/product-information-management/">Product Information Management</a></h2>
			</div>
			<div class="process-step">
				<div>
					<img src="/wp-content/uploads/home/syndicate1.png" />
					<h3 class="process-title">Syndicate	</h3>
					<h3 class="process-desc">
						Publish rich product data to eCommerce and other sales channels and 
						marketplaces based on the desired product mix and data formats to create a unified digital experience online.
					</h3>
				</div>
				<h2 class="process-btn"><a href="/product-data-syndication/">Product Data Syndication</a></h2>
			</div>
		
		</div>
		<div class="below-process">
			<div>
				<p>Empower your business with <b>powerful, flexible & cost-effective capabilities</b><br>
					to outthink product data problems and continuously drive up sales.</p>
				<a href="/work/">Read our client stories</a>
			</div>
		</div>	
	</div>
	
	
	<div class="client-tabs">
		<div class="tabs-title">
			<div id="client-1" class="client-logo active">
				<img src="/wp-content/uploads/home/client-11.png" /> 

			</div>
			<div  id="client-2" class="client-logo">
				<img src="/wp-content/uploads/home/client-22.png" />
			</div>
			<div  id="client-3" class="client-logo">
				<img src="/wp-content/uploads/home/client-31.png" />
			</div>
			<div  id="client-4" class="client-logo">
				<img src="/wp-content/uploads/home/client-41.png" />
			</div>
			<div  id="client-5" class="client-logo">
				<img src="/wp-content/uploads/home/client-51.png" />
			</div>
		</div>
		<div id="tab-client-1" class="tab-client"  style="display:block">
			<div class="client-tab-content">
				<div class="content-left">
					<div class="content-text">	
						<div class="quote-left">
							<img src="/wp-content/uploads/home/quot-left.png" />
						</div>					
						<p> 
							We have been very impressed with StrikeTru’s 
							expert level of understanding data-structure, ecommerce & the online world. 
							Their comprehensive understanding of Akeneo PIM in impressive. They were able to overcome 
							our challenges at every turn & help us better understand our future possibilities with their implementation.<br><br> 
							<b><i>Levi W. Hill IV President, Richmond Supply Company</i></b>
						</p>
						<div class="quote-right">

							<img src="/wp-content/uploads/home/quot-right.png">
						</div>
					
					</div>
					<a href="/work/">Learn More</a>
					 

				</div>
				<div class="content-right"> 
					<img src="/wp-content/uploads/home/pim-industiral.png" /> 
				</div> 
			</div>
		</div>
		<div id="tab-client-2" class="tab-client">
			<div class="client-tab-content">
				<div class="content-left">
					<div class="content-text">	
						<div class="quote-left">
							<img src="/wp-content/uploads/home/quot-left.png" />
						</div>					
						<p> 
							StrikeTru did a great job helping us modernize our IT and content management operations by implementing and customizing a 
							PIM solution to City Furniture’s unique business needs. <br>  <br>  
							<i><b>Juan Lopez, Sr. Manager of Digital Technology, City Furniture</b></i>
						</p>
						<div class="quote-right">

							<img src="/wp-content/uploads/home/quot-right.png">
						</div>
					
					</div>
					<a href="/work/">Learn More</a>
					 

				</div>
				<div class="content-right"> 
					<img src="/wp-content/uploads/home/pim-furniture.png" /> 
				</div> 
			</div>
		</div>
		<div id="tab-client-3" class="tab-client">
			<div class="client-tab-content">
				<div class="content-left">
					<div class="content-text">	
						<div class="quote-left">
							<img src="/wp-content/uploads/home/quot-left.png" />
						</div>					
						<p> 
							Leveraging StrikeTru's Akeneo2GlobalLink translation connector solution will drive significant value for our 
							content and commerce teams, and for our customers. <br><br>
							<i><b>Johan Viktorsson, Head of Program Management Office, Pierce AB</b></i>
				
						</p>
						<div class="quote-right">

							<img src="/wp-content/uploads/home/quot-right.png">
						</div>
					
					</div>
					<a href="/work/">Learn More</a>
					 

				</div>
				<div class="content-right"> 
					<img src="/wp-content/uploads/home/pim-automotive.png" /> 
				</div> 
			</div>
		</div>
		<div id="tab-client-4" class="tab-client">
			<div class="client-tab-content">
				<div class="content-left">
					<div class="content-text">	
						<div class="quote-left">
							<img src="/wp-content/uploads/home/quot-left.png" />
						</div>					
						<p> 
							We were captivated by StrikeTru's results from diving into granular detail on our product data upfront. 
							They were able to articulate a clear vision of what our data landscape would look like post implementation.<br><br>
							<i><b>Alixandra Beverley, Ecommerce Manager at AuthenTEAK</b></i>
						</p>
						<div class="quote-right">

							<img src="/wp-content/uploads/home/quot-right.png">
						</div>
					
					</div>
					<a href="/work/">Learn More</a>
					 

				</div>
				<div class="content-right"> 
					<img src="/wp-content/uploads/home/pim-furniture.png" /> 
				</div> 
			</div>
		</div>
		
		<div id="tab-client-5" class="tab-client">
			<div class="client-tab-content">
				<div class="content-left">
					<div class="content-text">	
						<div class="quote-left">
							<img src="/wp-content/uploads/home/quot-left.png" />
						</div>					
						<p> 
							Luxury outdoor furniture multi-brand manufacturer and retailer Brown Jordan selected StrikeTru 
							to deploy Akeneo PIM to optimize enterprise product data for their entire portfolio, 
							and help them re-platform their ecommerce site from NetSuite's SuiteCommerce to Magento M2.<br><br>
							<i><b></b></i>
						</p>
						<div class="quote-right">

							<img src="/wp-content/uploads/home/quot-right.png">
						</div>
					
					</div>
					<a href="/work/">Learn More</a>
					 

				</div>
				<div class="content-right"> 
					<img src="/wp-content/uploads/home/cleint-sample-store2.png" /> 
				</div> 
			</div>
		</div>
		

	
	</div>

	
	
		<div class="below-process">
			<div>
				<p>Read Akeneo's customer success story featuring City Furniture, powered by StrikeTru, to <b>find out how they accelerated <br>
					time-to-market by 50% while increasing eCommerce sales by 60% </b>with our custom-built PIM for Furniture solution.</p>
				<a href="/resources/#casestudies">Read Case Study</a>
			</div>
		</div>	
	
	
	<div class="platforms">
		<div class="platform-section akeneo-riversand-side">
			<div class="testimony akeneo animation-element slide-right delay-500 in-view _refresher">
				<h2>First Silver Solution Partner for Akeneo PIM in the US</h2>
				<div>
				<img src="/wp-content/uploads/home/akeneo-logo.png"/>
				<p>
					“StrikeTru has consistently leveraged its strong industry 
					experience and Akeneo PIM software capabilities to help 
					accelerate the growing adoption of Akeneo PIM in the US 
					as companies face increasing pressure to provide rich and high quality product content across all channels.”<br>
					<b style="font-size:15px;line-height:27px;"><i>Frederick de Gombert, CEO and Co-Founder of Akeneo</i></b>
				</p>
				<a href="/product-infromation-management/akeneo-pim-solutions/">Learn More</a>
				</div>
			</div>
			<div class="testimony riversand animation-element slide-left delay-500 in-view _refresher">
				<h2>20+ years of Riversand Implementation Expertise</h2>
				<div>
					<img src="/wp-content/uploads/home/riversand1.png"/>
					<p>
						Our deep understanding of the Riversand platform ensures that all our projects incorporate robust enterprise information and data architecture, data governance,
						integrations, automation, syndication, and best practices that shorten implementation times, drive user productivity and deliver outstanding business results.
					</p>
				<a href="/product-infromation-management/riversand-pim-solutions/">Learn More</a>
				
				</div>
			</div>
			
			<a class="solution-expert">Consult our solutions expert</a>

		</div>
	</div>
	
	<div class="home-smallpims">
		<div class="home-smallpim">
			<div class="smallpim-content">
				<h2>
					Discover the smallest yet immensely
					<b>powerful PIM</b> solution designed for
					growing businesses to easily manage their
					product databases, reach new customers
					and sell <b>more products online</b>
				</h2>
				<p>End to End  •  Low cost + Full Feature  •  Akeneo powered  •  Quick launch	</p>
			</div>
			<div class="smallpim-logo">
				<img src="/wp-content/uploads/home/smallpim-logo.png" />
			</div>
			<div class="smallpim-btns">
				<a class="smallpim-explore"><i style="color:#fca222;margin-right:8px;" class="fas fa-arrow-right"></i> Explore smallPIM</a>
				<a class="smallpim-need">Need PIM basics first?</a>
			</div>
		</div>
	</div>
	
	
	<div class="our-features">
		<div class="our-feature">
			<div class="our-feature-img">
				<img src="/wp-content/uploads/home/our-features-img.png" />
			</div>
			<div class="our-feature-content">
				<div>
					<h2>Our Features</h2>
					<p>
						We are a best-of-breed commerce
						transformation partner that helps
						businesses of all sizes translate data
						management efficiencies into growth
						opportunities
					</p>
					<a style="font-weight:700;">Grow with us <i style="color:#fff;margin-right:8px;" class="fas fa-arrow-right"></i> </a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- end #content -->
		
	<div id="sidebar" style="display:none;">	

			<!-- begin #skufactors -->
			<div id="home-quote">
			
				<a class="more" href="http://www.skufactors.com/">More »</a>				<h3 class="section-title">SkuFactors</h3>
				
				<div id="home-quote-content">
					<br><p>Hosted-based Product Information Management (PIM) system that is flexible, auto-upgradeable, and user friendly - a true Hosted alternative to the traditional PIM systems.</p>
				</div>

			</div>
			<!-- end #skufactors -->
			
			<!-- begin #home-quote -->
			<div id="home-quote">
			
				<?php if(get_theme_mod('home_quote_more_url')) echo '<a class="more" href="'.get_theme_mod('home_quote_more_url').'">More &raquo;</a>'; ?>
				<h3 class="section-title"><?php echo get_theme_mod('home_quote_title'); ?></h3>
				
				<div id="home-quote-top"></div>
				
				<div id="home-quote-content">
					<?php echo stripslashes(wpautop(get_theme_mod('home_quote_content'))); ?>
				</div>

				<div id="home-quote-bottom">
					<span class="home-quote-author">
						<?php 
							if(get_theme_mod('home_quote_author_url')) echo '<a href="'.get_theme_mod('home_quote_author_url').'">';
							if(get_theme_mod('home_quote_author_name')) echo get_theme_mod('home_quote_author_name'); 
							if(get_theme_mod('home_quote_author_url')) echo '</a>';
						?>
						<?php if(get_theme_mod('home_quote_author_opp')) echo ' - <span class="home-quote-opp">'.get_theme_mod('home_quote_author_opp').'</span>'; ?>
					</span>
				</div>
			</div>
			<!-- end #home-quote -->

			<div id="home-news">
				<h3 class="section-title"><?php echo get_theme_mod('home_news_title'); ?></h3>
				<ul>
				<?php 
					query_posts(array(
						'showposts' => get_theme_mod('home_news_num'),
						'category__not_in' => array(get_theme_mod('folio_cats'))
					));
					if(have_posts()):
					while(have_posts()): the_post(); ?>
						<li>
							<h4><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
							
							<p class="meta">
								<span class="date"><?php the_time(get_option('date_format')); ?></span>
								<span class="author">by <?php the_author(); ?></span> 
								<span class="comments">- <?php comments_popup_link('0 Comments','1 Comments','% Comments'); ?></span>
							</p>
							
							<p class="desc"><?php tj_content_limit(120); ?></p>
						</li><!-- end .post -->
					<?php endwhile;
					endif;
				?>
				</ul>
				<p class="more"><a href="<?php echo get_permalink(get_theme_mod('blog_page')); ?>"><?php _e('More news from our blog &raquo;','themejunkie'); ?></a></p>
			</div><!-- end #home-news -->
	</div><!-- end #sidebar -->
<script>

jQuery(document).ready(function (jQuery) {

  var interval;
 // interval = setInterval(function () {
//    moveRight();
//  }, 5000);

  jQuery('._slider').mouseover(function(){
    clearInterval(interval);
  });
  
  jQuery('._slider').mouseleave(function(){
    interval = setInterval(function () {
      moveRight();
      }, 5000);
  });
  
	var slideCount = jQuery('._slider ul li').length;
	var slideWidth = jQuery('._slider ul li').width();
	var slideHeight = jQuery('._slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	
	
    jQuery('._slider ul li:last-child').prependTo('._slider ul');

    function moveLeft() {
        jQuery('._slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            jQuery('._slider ul li:last-child').prependTo('._slider ul');
            jQuery('._slider ul').css('left', '');
        });
    };

    function moveRight() {
        jQuery('._slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            jQuery('._slider ul li:first-child').appendTo('._slider ul');
            jQuery('._slider ul').css('left', '');
        });
    };

    jQuery('._slider_prev').click(function () {
        moveLeft();
        return false;
    });

    jQuery('._slider_next').click(function () {
        moveRight();
        return false;
    });

});    
</script>
<script>
$(document).ready(function(){
  $(".a-smooth").on('click', function(event) {

    if (this.hash !== "") {

      event.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        window.location.hash = hash;
      });
    } 
  });
  
  var n = $('._slider.style-slider ul li').length;
	if(n < 2){
		$('._slider_next').hide();
		$('._slider_prev').hide();
	}
	});


</script>

<!--TAB SCRIPT-->
<script>	
$(document).ready(function(){
  
  $("#client-1").on('click', function(event) {
	  $( ".client-logo").each(function() {
		  $(this).removeClass("active");
	  });
	  $(".tab-client").each(function() {
		  $(this).hide();
	  });
	  $("#tab-client-1").show();
	  $(this).addClass("active");
  });
  $("#client-2").on('click', function(event) {
	   $( ".client-logo").each(function() {
		  $(this).removeClass("active");
	  });
	  $(".tab-client").each(function() {
		  $(this).hide();
	  });
	  $("#tab-client-2").show();
	  $(this).addClass("active");
  });
  $("#client-3").on('click', function(event) {
	   $( ".client-logo").each(function() {
		  $(this).removeClass("active");
	  });
	  $(".tab-client").each(function() {
		  $(this).hide();
	  });
	  $("#tab-client-3").show();
	  $(this).addClass("active");
  });
  $("#client-4").on('click', function(event) {
	   $( ".client-logo").each(function() {
		  $(this).removeClass("active");
	  });
	  $(".tab-client").each(function() {
		  $(this).hide();
	  });
	  $("#tab-client-4").show();
	  $(this).addClass("active");
  });
  $("#client-5").on('click', function(event) {
	  $( ".client-logo").each(function() {
		  $(this).removeClass("active");
	  });
	  $(".tab-client").each(function() {
		  $(this).hide();
	  });
	  $("#tab-client-5").show();
	  $(this).addClass("active");
  });
  
 
  
});
</script>
<script type="text/javascript" >

		var $animation_elements = $('.animation-element');
		var $window = $(window);
		var refresherIndex;
		
		function check_if_in_view() {
		  var window_height = $window.height();
		  var window_top_position = $window.scrollTop();
		  var window_bottom_position = (window_top_position + window_height);
	
		  $.each($animation_elements, function() {
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);
		 
			//check to see if this current container is within viewport
			if ((element_bottom_position >= window_top_position) &&
				((element_top_position+60) <= window_bottom_position)) {
				$element.addClass('in-view');
				$element.addClass('_refresher'); 
				var refresherIndex = setInterval(function(){ _view_refresher($element); }, 300000);

			} else {
				$element.removeClass('in-view');
				$element.removeClass('_refresher'); 
				
				clearInterval(refresherIndex);
			}
		  });
		}
	
		function _view_refresher($element){
			if($element.hasClass( "_refresher" )){
				$element.removeClass('in-view');
				$refresher = setTimeout(function() {
					$element.addClass('in-view');
				}, 250);
				
			}	
		}

		$window.on('scroll resize', 
			function(){
				if($window.width() > 800){
					check_if_in_view();
				}
			}
		);
		$window.trigger('scroll');
		</script>
</div>
<?php get_footer(); ?>
