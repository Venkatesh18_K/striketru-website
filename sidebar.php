<div id="sidebar">
	<?php 
		if(is_page()) {
			global $post;
			if($post->ID == get_theme_mod('folio_page')) {
				if ( is_active_sidebar( 'folio-sidebar' ) ) : dynamic_sidebar( 'folio-sidebar'); endif; 
			} elseif($post->ID == get_theme_mod('blog_page')) { 
				if ( is_active_sidebar( 'blog-sidebar' ) ) : dynamic_sidebar( 'blog-sidebar'); endif;
			} else {
				if ( is_active_sidebar( 'page-sidebar' ) ) : dynamic_sidebar( 'page-sidebar'); endif; 
			}	
		} elseif(is_single() && tj_in_folio_cat() ) {
			if ( is_active_sidebar( 'folio-sidebar' ) ) : dynamic_sidebar( 'folio-sidebar'); endif; 
		} elseif(is_category() && tj_is_folio_cat()) {
			if ( is_active_sidebar( 'folio-sidebar' ) ) : dynamic_sidebar( 'folio-sidebar'); endif; 
		} else {
			if ( is_active_sidebar( 'blog-sidebar' ) ) : dynamic_sidebar( 'blog-sidebar'); endif;
		}
	?>
</div><!-- end #sidebar -->
