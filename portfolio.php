<?php get_header(); ?>

	<div class="inner-content">
	<div class="folio portfolio">
	<?php 
		$paged = get_query_var('paged') ? get_query_var('paged'):1;
		
		if(is_page(get_theme_mod('folio_page'))) {
			$folio_cat_ids = get_theme_mod('folio_cats');
			$folio_cat_arr = explode(',',$folio_cat_ids);
			query_posts(array(
				'posts_per_page' => get_theme_mod('folio_num'),
				'paged' => $paged,
				'category__in' => $folio_cat_arr
			));
		} else {
			$cat = get_query_var('cat');
			query_posts('posts_per_page='.get_theme_mod('folio_num').'&paged='.$paged.'&cat='.$cat);
		}
		
		if (have_posts()) {
			echo '<div class="gridrow clear">';
			
			while (have_posts()) : the_post();
				global $post; 
				$q = $wp_query->current_post;  
				$maxq = tj_current_postnum(); 
				if(is_int(($q+1)/2)) $postclass = 'fpost last'; else $postclass = 'fpost'; ?>
				
				<div class="<?php echo $postclass; ?>">
					<div class="fpost-wrap">
						<div class="thumb">
							<?php tj_thumbnail(416,get_theme_mod('folio_thumb_height')); ?>
						</div>
						
						<div class="text-col">
							<h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							
							<p class="excerpt">
								<?php tj_content_limit(140); ?>
							</p>
							<a class="button learn_more" href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>">LEARN MORE</a>
						</div>
					</div>
				</div> <!-- end .item -->
			
				
				<?php if($q < $maxq-1 && is_int(($q+1)/3)) echo '</div><div class="gridrow clear">';
			endwhile;
			
			echo '</div> <!--end .gridrow-->';
				
			if ( $wp_query->max_num_pages > 1 ) tj_pagenavi();
		} else { 
			include(TEMPLATEPATH. '/includes/not-found.php'); 
		}	
	?>
	</div><!-- end .folio -->
	</div><!-- end .inner-content -->

<?php get_footer(); ?>