<?php
session_start();
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'themejunkie', TEMPLATEPATH . '/languages' );	

// Load functions and Widgets.
require_once(TEMPLATEPATH . '/functions/meta-box.php');
require_once(TEMPLATEPATH . '/functions/comments.php');
require_once(TEMPLATEPATH . '/functions/contact-form.php');
require_once(TEMPLATEPATH . '/functions/theme-options.php');
require_once(TEMPLATEPATH . '/functions/flickr-widget.php');
require_once(TEMPLATEPATH . '/functions/category-widget.php');

if (function_exists('create_initial_post_types')) create_initial_post_types(); //fix for wp 3.0

if (function_exists('add_post_type_support')) add_post_type_support( 'page', 'excerpt' );

// Add menus support
add_action( 'init', 'tj_register_my_menu' );
function tj_register_my_menu() {
   register_nav_menus(
      array(
         'header-pages' => __( 'Header Pages', 'themejunkie' ),
      )
   );
}
//Contact Form 7
add_filter('wpcf7_form_action_url', 'remove_unit_tag');

function remove_unit_tag($url){
    $remove_unit_tag = explode('#',$url);
    $new_url = $remove_unit_tag[0];
    return $new_url;
}

// Custom Conditional Tags
function tj_is_folio_cat($cid = false) {
	if($cid) 
		$cat = $cid;
	else
		$cat = get_query_var('cat');
	
	$folio_cat_ids = get_theme_mod('folio_cats');
	$folio_cat_arr = explode(',',$folio_cat_ids);
	
	if(in_array($cat,$folio_cat_arr)) {
		return true;
	} else {
	
		foreach($folio_cat_arr as $folio_cat) {
			if(cat_is_ancestor_of($folio_cat,$cat))
			return true;
			break;
		}
	}
	
}

function tj_in_folio_cat($pid = '') {
	global $post;		
	$cats = get_the_category($pid);
	
	foreach($cats as $cat) {
		$folio_cat_ids = get_theme_mod('folio_cats');
		$folio_cat_arr = explode(',',$folio_cat_ids);
					
		if(in_array($cat->cat_ID,$folio_cat_arr)) {
			return true;
			break;
		}
	}
}


// Filter to new excerpt length
function tj_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'tj_excerpt_length' );

// Filter to new excerpt more text
function tj_excerpt_more($post) {
	return '... <a class="meta-more" href="'. get_permalink($post->ID) . '">'.__('Read more <span class="meta-nav">&raquo;</span>','themejunkie').'</a>';
}
add_filter('excerpt_more', 'tj_excerpt_more');

// Filter to fix first page redirect
add_filter('redirect_canonical', 'fixpageone');

function fixpageone($redirect_url) {
	if(get_query_var('paged') == 1)
		$redirect_url = '';
	return $redirect_url;
}

// Tests if any of a post's assigned categories are descendants of target categories
function post_is_in_descendant_category( $cats, $_post = null ) {
	foreach ( (array) $cats as $cat ) {
		// get_term_children() accepts integer ID only
		$descendants = get_term_children( (int) $cat, 'category');
		if ( $descendants && in_category( $descendants, $_post ) )
			return true;
		}
	return false;
}

// Register Widgets
function tj_widgets_init() {

	// Blog Sidebar
	register_sidebar( array (
		'name' => __( 'Blog Sidebar', 'themejunkie' ),
		'id' => 'blog-sidebar',
		'description' => __( 'The blog widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Portfolio Sidebar
	register_sidebar( array (
		'name' => __( 'Portfolio Sidebar', 'themejunkie' ),
		'id' => 'folio-sidebar',
		'description' => __( 'The portfolio widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Page Sidebar
	register_sidebar( array (
		'name' => __( 'Page Sidebar', 'themejunkie' ),
		'id' => 'page-sidebar',
		'description' => __( 'The page widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 1
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 1', 'themejunkie' ),
		'id' => 'footer-widget-area-1',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 2
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 2', 'themejunkie' ),
		'id' => 'footer-widget-area-2',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 3
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 3', 'themejunkie' ),
		'id' => 'footer-widget-area-3',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
	
	// Footer Widget Area 3
	register_sidebar( array (
		'name' => __( 'Footer Widget Area 4', 'themejunkie' ),
		'id' => 'footer-widget-area-4',
		'description' => __( 'The bottom widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="fwidget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="fwidget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'init', 'tj_widgets_init' );

function myFilter($query) {
	$paged = get_query_var('paged') ? get_query_var('paged'):1;
	
	$folio_showposts = get_theme_mod('folio_num');
	$folio_cat_ids = get_theme_mod('folio_cats');
	$folio_cat_arr = explode(',',$folio_cat_ids);
	
    if (is_search()) {
        $query->set('category__not_in',$folio_cat_arr);
    }

	if (is_date()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
	if (is_tag()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
	if (is_feed()) {
        $query->set('category__not_in',$folio_cat_arr);
    }
	
return $query;
}
add_filter('pre_get_posts','myFilter');

// Custom styles and scripts
if(!is_admin()) {
	add_action( 'wp_print_styles', 'custom_styles', 100 );
	add_action( 'wp_print_scripts', 'custom_scripts', 100 );
}
function custom_styles() {
	$color_scheme = get_theme_mod('color_scheme');
	if($color_scheme == 'Blue') 
		wp_enqueue_style('blue',get_bloginfo('template_url').'/skins/blue.css');
	elseif($color_scheme == 'Brown') 
		wp_enqueue_style('brown',get_bloginfo('template_url').'/skins/brown.css');
	elseif($color_scheme == 'Red')
		wp_enqueue_style('red',get_bloginfo('template_url').'/skins/red.css');
	elseif($color_scheme == 'Green')
		wp_enqueue_style('green',get_bloginfo('template_url').'/skins/green.css');
	elseif($color_scheme == 'Yellow')
		wp_enqueue_style('blue',get_bloginfo('template_url').'/skins/blue.css');
	elseif($color_scheme == 'Aqua')
		wp_enqueue_style('aqua',get_bloginfo('template_url').'/skins/aqua.css');
	elseif($color_scheme == 'Gray')
		wp_enqueue_style('gray',get_bloginfo('template_url').'/skins/gray.css');
	elseif($color_scheme == 'Pink')
		wp_enqueue_style('pink',get_bloginfo('template_url').'/skins/pink.css');
	elseif($color_scheme == 'Purple')
		wp_enqueue_style('purple',get_bloginfo('template_url').'/skins/purple.css');
	elseif($color_scheme == 'Orange')
		wp_enqueue_style('orange',get_bloginfo('template_url').'/skins/orange.css');
 }	
function custom_scripts() {
	wp_deregister_script( 'jquery' );

	wp_enqueue_script('jquery', get_bloginfo('template_url').'/js/jquery-1.4.2.min.js', false, '1.4.2',true);

// Start Enque Script For Mail Chimp 
	if ( is_tax('blog-category') || is_single() ) {
		wp_enqueue_script('jquery-mail', get_bloginfo('template_url').'/js/mail-champ-.js', true, '1.0',true);
	}
// End Enque Script For Mail Chimp 
	
    wp_enqueue_style('style_multi', get_stylesheet_directory_uri().'/jquery.multiselect.css', false, '');
	 wp_enqueue_style('fselect', get_stylesheet_directory_uri().'/fSelect.css', false, '');
	//wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', false, '1.4.2');
	
	//wp_enqueue_script('jquery_custom', get_stylesheet_directory_uri().'/custom.js', true, '');
	wp_enqueue_script('jquery_multi', get_template_directory_uri().'/js/jquery.multiselect.js','', '', true);
	
        wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js' );
	//wp_enqueue_script('masonry-jquery', 'http://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js', true, '4.1');
	wp_enqueue_script('masonry-jquery', get_bloginfo('template_url').'/js/masonry.pkgd.min.js', true, '4.1');
	wp_enqueue_script('cufon', get_bloginfo('template_url').'/js/cufon-yui.js', true, '1.0');
	wp_enqueue_script('jquery-global', get_bloginfo('template_url').'/js/global.js', true, '1.0');

    // wp_enqueue_script('jquery111', 's3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', '', '',true);

	//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js


	wp_localize_script( 'custom-js', 'js_config', array(
        'ajax_url'  => admin_url( 'admin-ajax.php' ),
        'ajax_nonce'    => wp_create_nonce( 'ajax-nonce' ),
    ));

	if ( is_singular() && get_option('thread_comments') ) wp_enqueue_script( 'comment-reply' );
}



function misha_my_load_more_scripts() {
	global $wp_query; 
        
	// In most cases it is already included on the page and this line can be removed
	wp_enqueue_script('jquery');
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore.js', array('jquery') );

	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );



// Pagenavi
function tj_pagenavi($range = 9) {
	global $paged, $wp_query;
	if ( !$max_page ) { $max_page = $wp_query->max_num_pages;}
	if($max_page > 1){
		echo '<div class="pagenav clear">';
		if(!$paged){$paged = 1;}
		echo '<span>Page '.$paged.' / '.$max_page.'</span>';
		previous_posts_link('&laquo; Previous');
		if($max_page > $range){
			if($paged < $range){
				for($i = 1; $i <= ($range + 1); $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";

				}
			} elseif($paged >= ($max_page - ceil(($range/2)))){
				for($i = $max_page - $range; $i <= $max_page; $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";
				}
			} elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
				for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
					echo "<a href='" . get_pagenum_link($i) ."'";
					if($i==$paged) echo " class='current'";
					echo ">$i</a>";
				}
			}
		} else {
			for($i = 1; $i <= $max_page; $i++){
				echo "<a href='" . get_pagenum_link($i) ."'";
				if($i==$paged) echo " class='current'";
				echo ">$i</a>";
			}
		}
		next_posts_link('Next &raquo;');
		echo '</div>';
	}
}

// Breadcrumb
function tj_breadcrumb() {
	 $delimiter = '/';
  $name = 'Home'; //text for the 'Home' link
  $currentBefore = '<span class="current">';
  $currentAfter = '</span>';
	// echo '<small>You are here:</small>';
 
    global $post;
    $home = get_bloginfo('url');
   
	if(is_home() && get_query_var('paged') == 0) 
		echo '<span class="home">' . $name . '</span>';
	else
		echo '<a class="home" href="' . $home . '">' . $name . '</a> '. $delimiter . ' ';
		
	if(is_category()) {
		if(tj_is_folio_cat() &&  get_theme_mod('display_folio_page_in_breadcrumb') == 'Yes') {
		$pid = get_page(get_theme_mod('folio_page'));
		echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
	  } else {
		if( get_theme_mod('display_blog_page_in_breadcrumb') == 'Yes') {
			$pid = get_page(get_theme_mod('blog_page'));
			echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
		}
	  }
	 }
	
	if(is_single()) {
           
	if(tj_in_folio_cat() &&  get_theme_mod('display_folio_page_in_breadcrumb') == 'Yes') {
		$pid = get_page(get_theme_mod('folio_page'));
		echo '<a href="'.get_permalink($pid).'" >'.$pid->post_title.'</a>'.$delimiter;
                
	  } else {
		if( get_theme_mod('display_blog_page_in_breadcrumb') == 'Yes') {
			$pid = get_page(get_theme_mod('blog_page'));
                        
			echo '<a href="'.get_permalink($pid).'">'.$pid->post_title.'</a> '.$delimiter;
		}
	  }
	}
	
    if ( is_category()) {
	 
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore;
      single_cat_title();
      echo $currentAfter;
 
    }

// Start Content Hub Category Page Breadcurm (21-04-20)

     else if ( is_tax('content-hub-category') ) {
	 
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore;

					$page = get_page_by_title( $_SESSION['page_name_lavel_3'] );					

					$url = $_SERVER['HTTP_REFERER'];
                      
               		$post_id= url_to_postid( $url );

                    $page_title = get_the_title($post_id);

                    if(get_post_type( $post_id ) == 'post'){
                    	echo '<a href="'.get_permalink($_SESSION['page_name_lavel_3_id']).'">'.$_SESSION['page_name_lavel_3'].'</a>'.' ' . $delimiter . ' ';
                    }else{
					echo '<a href="'.get_permalink($post_id).'">'. $page_title.'</a>'.' ' . $delimiter . ' ';
					}

 					  single_cat_title();	
				      echo $currentAfter;
} 

// End Content Hub Category Page breadcrumb (21-04-20)


    else if ( is_archive('blog-category') ) {
	 
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore;
      echo '<a href="'.get_permalink(65).'">Blog</a>'.' ' . $delimiter . ' ';
       single_cat_title();
      echo $currentAfter;
 
    } 


    elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single() ) {
		if(!is_attachment()) {
      $cat = get_the_category(); 
	  $cat = $cat[0];


// Start Change Content Hub Lavel 2 and Lavel 3 breadcrumb (21-04-20)

	  				$url = $_SERVER['HTTP_REFERER'];
					  
					$post_id= url_to_postid( $url );

					$page_title = get_the_title($post_id);
	  		        if($_SESSION['page_name_lavel_2'] == $page_title){
	  		        	$_SESSION['page_name_lavel_3']='';
	  		        }


					if($_SESSION['page_name_lavel_3'] != ''){
						$page = get_page_by_title( $_SESSION['page_name_lavel_3'] );

						//echo $url = $_SERVER['HTTP_REFERER'];

					    	
					    echo '<a class="single_cat" href="'.get_permalink($_SESSION['page_name_lavel_3_id']).'">'.$_SESSION['page_name_lavel_3'].'</a>'.' ' . $delimiter . ' ';

					    echo '<a class="single_cat" href="'.$_SESSION['page_name_lavel_3_term_link'].'">'.$_SESSION['page_name_lavel_3_term_name'].'</a>'.' ' . $delimiter . ' ';


					  
					}else if($_SESSION['page_name_lavel_2'] != ''){
							$page = get_page_by_title( $_SESSION['page_name_lavel_2'] );
						    echo '<a class="single_cat" href="'.$_SESSION['page_name_lavel_2_url'].'">'.$_SESSION['page_name_lavel_2'].'</a>'.' ' . $delimiter . ' ';
					}
					else{
						session_unset();
						echo '<a class="single_cat" href="'.get_permalink(65).'">Blog</a>'.' ' . $delimiter . ' ';
					}
	

// End Change Content Hub Lavel 2 and Lavel 3 Breadcurms (21-04-20)


    //  echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      // (21-04-20) echo '<a href="'.get_permalink(65).'">Blog</a>'.' ' . $delimiter . ' ';
	  }
        
      echo $currentBefore;
      the_title();
      echo $currentAfter;
    
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search for ' . get_search_query() . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore;
      single_tag_title();
      echo $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore. $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
       if(!is_page('65')){
      echo $delimiter. $currentBefore . __('Page') . ' ' . get_query_var('paged') . $currentAfter;
       }
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 

}

// Get image attachment (sizes: thumbnail, medium, full)
function get_thumbnail($postid=0, $size='full') {
	if ($postid<1) 
	$postid = get_the_ID();
	$thumb_key = get_theme_mod('thumb_key');
	if($thumb_key)
		$thumb_key = $thumb_key;
	else
		$thumb_key = 'thumb';
	$thumb = get_post_meta($postid, $thumb_key, TRUE); // Declare the custom field for the image
	if ($thumb != null or $thumb != '') {
		return $thumb; 
	} elseif ($images = get_children(array(
		'post_parent' => $postid,
		'post_type' => 'attachment',
		'numberposts' => '1',
		'post_mime_type' => 'image', ))) {
		foreach($images as $image) {
			$thumbnail=wp_get_attachment_image_src($image->ID, $size);
			return $thumbnail[0]; 
		}
	} else {
		return get_bloginfo ( 'stylesheet_directory' ).'/images/default_thumb.gif';
	}
	
}

// Automatically display/resize thumbnail
function tj_thumbnail($width, $height) {
	echo '<a href="'.get_permalink($post->ID).'" rel="bookmark"><img src="'.get_bloginfo('template_url').'/timthumb.php?src='.get_thumbnail($post->ID, 'full').'&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1" alt="'.get_the_title().'" /></a>';
}

// Get limit excerpt
function tj_content_limit($max_char, $more_link_text = '', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);

   if (strlen($_GET['p']) > 0) {
      echo "";
      echo $content;
      echo "...";
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo "";
        echo $content;
        echo "...";
   }
   else {
      echo "";
      echo $content;
   }
}

// Return number of posts in a Archive Page
function tj_current_postnum() {
	global $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if(empty($paged) || $paged == 0) $paged = 1;
	if (!is_404()) 
		$begin_postnum = (($paged-1)*$posts_per_page)+1; 
	else 
		$begin_postnum = '0';
	if ($paged*$posts_per_page < $numposts) 
		$end_postnum = $paged*$posts_per_page; 
	else 
		$end_postnum = $numposts;
	$current_page_postnum = $end_postnum-$begin_postnum+1;
	return $current_page_postnum;
}

// begin customization
// used by Downloads and Download Collateral pages to send an email identifying the specific collateral requested
function add_query_vars_filter($vars) {
  $vars[] = 'assetid';	//ID of asset (case study, etc) to download
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

//Kinjal menu customization
function prefix_nav_description( $item_output, $item, $depth, $args ) {
if ( !empty( $item->description ) ) {
$item_output = str_replace( $args->link_after . '</a>', '<p class="menu-item-description">' . $item->description . '</p>' . $args->link_after . '</a>', $item_output );
}
return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
   .hidden-field {
    display: block;
}
  </style>';
}

// end customization


//** Changes 19-03-20  **//

add_theme_support( 'post-thumbnails' );
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


function themename_custom_taxonomies() {
	// Register Texonomy

$coach_method = array(
		'name' => _x( 'Blog Category', 'taxonomy general name' ),
		'singular_name' => _x( 'Blog Category', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search in Blog Category' ),
		'all_items' => __( 'All Blog Category' ),
		'most_used_items' => null,
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Blog Category' ), 
		'update_item' => __( 'Update Blog Category' ),
		'add_new_item' => __( 'Add new Blog Category' ),
		'new_item_name' => __( 'New Blog Category' ),
		'menu_name' => __( 'Blog Category' ),
	);



	$args = array(
		'hierarchical' => true,
		'labels' => $coach_method,
		'show_ui' => true,
		'query_var' => true,
		'show_in_rest' => true,	
		'rewrite' => array( 'slug' => 'blog-category' )
	);
    register_taxonomy( 'blog-category', array( 'post' ), $args );
}
add_action( 'init', 'themename_custom_taxonomies', 0 );

add_action( 'save_post', 'my_save_post_function', 10, 3 );

function my_save_post_function( $post_ID, $post, $update ) {
  	
$args = array(
'hide_empty' => false,

'taxonomy'  => 'blog-category',
);
$terms = get_terms( $args );

foreach ($terms as $key => $value) {
	
	$posts_array = get_posts(
    array(
        'posts_per_page' => 1,
        'orderby' => 'modified',
        'tax_query' => array(
            array(
                'taxonomy' => 'blog-category',
                'field' => 'term_id',
                'terms' => $value->term_id,
            )
        )
    )
);
	$newDate = date("d/m/Y", strtotime($posts_array[0]->post_modified)); 

	 update_term_meta($value->term_id,'last_updated_post_date',$newDate);

 }
}
 add_image_size('post-feature', 353, 178, true);
 add_image_size('blog-listing', 353, 178, true);
 add_image_size('blog-product', 353, 228, true);
 add_image_size('blog-detail', 731, 307, true);


// Short Code For Updated Category

function updated_post(){
global $wp_query;
$current_page_id=$wp_query->post->ID;

$args = array('post_type' => 'post',
	'orderby' => 'modified',
	'posts_per_page'=> -1
 );                                              
$the_query = new WP_Query( $args );
$term_ids=array();
if ($the_query -> have_posts() ) {
	ob_start();
	while ($the_query -> have_posts() ) {
		$the_query -> the_post(); 
		
		$term_obj_list = get_the_terms( get_the_ID(), 'blog-category' );

		$chk_show=get_field('enable_disable','term_'.$term_obj_list[0]->term_id);
		
		    $chk_show_page=get_field('select_page_to_show_blog_category','term_'.$term_obj_list[0]->term_id);
		    
		    
		        foreach ((array) $chk_show_page as $values) {
		                 
		                 	if($values->ID == $current_page_id){
                			 $term_ids[]=$term_obj_list[0]->term_id;
                		    }
		             
                }

	} // end while
	wp_reset_query();
}

$ids=array_unique($term_ids);

$term_ids_show=array_filter($ids);
	
$output='';
$output.='<div class="product_cat_main">

<div class="container_custom">
<div class="row">';


foreach ($term_ids_show as  $term_id) {
	
	$value=get_term($term_id,'blog-category');

	$chk_show=get_field('enable_disable',$value->term_id);

	$image=get_field("add_images","term_".$value->term_id);


	$image_attributes = wp_get_attachment_image_src( $attachment_id = $image ,'blog-product');
	

	if (!$image_attributes[0]) {
                    $image = get_template_directory_uri() . '/images/default_image-353x228.png';
                }else{
                	$image=$image_attributes[0];
                }
	
	$output.='<div class="col_4">
	<div class="blog_category_list">


	<div class="cat_img"><a href="'.get_term_link($value->slug, "blog-category").'" ><img src="'.$image.'"></a></div>
	 <div class="cat_content">';
	
	$output.= '<h4 class="font16"><a href="'.get_term_link($value->slug, "blog-category").'" >'.$value->name.'</a></h4>';
	//$output.= "<p class='gray_txt'>Last Updated On: ".get_field('last_updated_post_date','term_'.$value->term_id)."</p>"; 
	$output.= '<p>'.$value->description.'</p>';
	$output.= "<a href=".get_term_link($value->slug, 'blog-category')." class='button'>Read More</a>";
	
	$output.='</div>
	</div>
	</div>';

}


$output.='</div>
</div>
</div>';
ob_end_clean();
wp_reset_postdata();
 
 return $output;
    
}
add_shortcode('content_hub','updated_post');


// Disable Field to Edit in Backend Blog-Category Section

add_filter('acf/load_field/key=field_5e6b1e04b8bca', 'acf_read_only');
function acf_read_only($field) {
	$field['readonly'] = 1;
	return $field;
}


// Blog Page With Search pagination and Sort By Category

add_action( 'wp_ajax_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' );
add_action( 'wp_ajax_nopriv_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' ); 
function cvf_demo_pagination_load_posts() {
    global $wpdb;
    // Set default variables
    $msg = '';
    if(isset($_POST['page']) ){
       $msg = '';
    $page = sanitize_text_field($_POST['page']);
    $cur_page = $page;
    $page -= 1;
    $per_page = get_option('posts_per_page');
    $previous_btn = true;
    $next_btn = true;
    $first_btn = true;
    $last_btn = true;
    $start = $page * $per_page;
    $search = $_POST['keyword'];
    $selected_tax = $_POST['selecetd_taxonomy'];
    if ($selected_tax == "all") {
        $selected_tax = "";
    }
    global $post;
    $all_term=array();
    $terams=get_terms('blog-category');
    foreach($terams as $term){
        $all_term[]=$term->term_id;
    }
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => $per_page,
        'order' => 'DESC',
        'offset' => $start,
        's' => $search,
              'tax_query' => array(
                        array(
                            'taxonomy' => 'blog-category',
                            'field' => 'term_id',
                            'terms' => $all_term
                        )
                    ),
    );
    if($_POST['term_slug']){
           $args = array(
         'post_type' => 'post',
        'posts_per_page' => $per_page,
        'order' => 'DESC',
        'offset' => $start,
        's' => $search,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'blog-category',
                            'field' => 'term_id',
                            'terms' => $_POST['term_slug']
                        )
                    ),
                );
        
    }
    $count_posts = new WP_Query($args);


 $msg.='<div class="product_cat_main">

<div class="container_custom">
<div class="row">';
if($count_posts  -> have_posts()) {
 while ($count_posts  -> have_posts()) {
 	$count_posts  -> the_post();
 	  	 global $post;
                $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
                $image = wp_get_attachment_image_src($post_thumbnail_id, 'post-feature');
                if (!$post_thumbnail_id) {
                    $image[0] = get_template_directory_uri() . '/images/default_image-353x178.png';
                }
                $cnc = get_the_title();
                $msg.='<div class="col_4">
	<div class="blog_category_list">';

                $msg.='<div class="cat_img"><a href="' . get_the_permalink() . '"><img src="' . $image[0] . '"></a></div>';

                $msg.='<div class="cat_content">';

                $msg.= '<h4 class="font16"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h4>';
                $msg.= '<p>' . $cnc . '</p>';
                $msg.= "<a href=" . get_the_permalink() . " class='button'>Read More</a>";

                $msg.='</div></div>
	</div>';
            }
}else{
	$msg.='<div class="col_12 error">Sorry no Blogs matched your criteria. </div>';
}
$msg.='</div>
	</div>
	</div>';


        // Optional, wrap the output into a container

        $msg = "<div class='cvf-universal-content'>" . $msg . "</div>";
        // This is where the magic happens
        $count = $count_posts->found_posts;
        $no_of_paginations = ceil($count / $per_page);
        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }

        // Pagination Buttons logic     

        $pag_container .= "<div class='container_custom'>
<div class='row'>
<div class='col_12'>
        <div class='cvf-universal-pagination'>
            <ul>";

            $link=get_template_directory_uri().'/images/pagination_arrow.png';

        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active pre_arrow'><img src=".$link."></li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive pre_arrow'><img src=".$link."></li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
            else
                $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            
            $pag_container .= "<li p='$nex' class='active next_arrow'><img src=".$link."></li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive next_arrow'><img src=".$link."></li>";
        }


        $pag_container = $pag_container . "
            </ul>
        </div></div></div></div><input type='hidden' id='total_pages' value=".$no_of_paginations.">";

        // We echo the final output
        echo 
        '<div class = "cvf-pagination-content">' . $msg . '</div>' ; 
        if($no_of_paginations >1){
        echo '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';
    	}

    }
    // Always exit to avoid further execution
    exit();}

// Blog Texonomy Page

    function misha_loadmore_ajax_handler() {
    $tax = $_POST['tax'];
    $paged = $_POST['page'] + 1;
    $args['post_status'] = 'publish';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'orderby' => 'ASC',
        's' => $_POST['search'],
        'paged' => $paged,
        'tax_query' => array(
            array(
                'taxonomy' => 'blog-category',
                'field' => 'term_id',
                'terms' => $tax,
            )
        ),
    );
    $query = new WP_Query($args);
    $data = "";
    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
            global $post;
            $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'post-feature');
            $img_default = get_template_directory_uri() . '/images/default_image.png';
            $post_date = get_the_date('j/m/Y');
            $all_tags = array();
            $cnc = get_the_excerpt();
            $author = get_the_author();
            $title = get_the_title();
            $link = get_the_permalink();
            $data.='<div class="blog_detail_content col_4">';
            $data.='<div  class="blog_category_list">';
            $data.='<div class="cat_img">';
            if ($large_image_url[0]) {
                $data.='<a href="' . $link . '"><img src="' . $large_image_url[0] . '"></a>';
            } else {
                $data.='<a href="' . $link . '"><img src="' . $img_default . '"></a>';
            }
            $data.= '</div>';
            $data.='<div class="cat_content">';

            $data.='<div class="entry-metadata-info">';

            $data.='<div class="entry-tags-blog">';

            // if (is_array(get_the_tags())) {
            //     $tags = get_the_tags();
            //     foreach ($tags as $tag) {
            //         $data.= '<li>' . "$tag->name" . '</li>';
            //     }
            // }

            $data.='</div>';
           // $all_tags = get_the_tags();
            //$data.='<p class="gray_txt"><span class="meta-date">' . $post_date . '</span> | <span class="meta-author">By ' . $author . '</span> </p>';
            $data.='<h4 class="font16"><a href="' . $link . '"> ' . $title . ' </a></h4>';
            $data.='<p>' . $cnc . '</p>';

            $data.= '</div>';

            $data.= '<div class="entry">';
            $data.= '<a href="' . $link . '" class="button">READ MORE</a>';
            $data.= '</div>';

            $data.= '</div>';

            $data.= '</div>';
            $data.='</div>';
        endwhile;
    endif;
    wp_reset_query();

    echo $data;
    die(0); // here we exit the script and even no wp_reset_query() required!
}

add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


// Register Custom Sidebar
register_sidebar( array (
		'name' => __( 'Blog Detail Page Sidebar', 'themejunkie' ),
		'id' => 'blog-detail-sidebar',
		'description' => __( 'The blog detail page widget area', 'themejunkie' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

// NewsLetter Function 
function jnz_tnp_ajax_subscribe_footer() {
    check_ajax_referer( 'ajax-nonce', 'nonce' );
    $data = urldecode( $_POST['data'] );
    if ( !empty( $data ) ) :
        $data_array = explode( "&", $data );
        $fields = [];
        foreach ( $data_array as $array ) :
            $array = explode( "=", $array );
            $fields[ $array[0] ] = $array[1];
        endforeach;
    endif;
    if ( !empty( $fields ) ) :
         if ( filter_var( $fields['ne'], FILTER_VALIDATE_EMAIL ) ) :
            global $wpdb;
             //check if the email is already in the database
             $exists = $wpdb->get_var(
                 $wpdb->prepare(
                     "SELECT email FROM " . $wpdb->prefix . "newsletter
                    WHERE email = %s",
                     $fields['ne']
                 )
             );
             if ( ! $exists ) {
                 NewsletterSubscription::instance()->subscribe();
                 $output = array(
                     'status'    => 'success',
                     'msg'       => __( '<div style="color:green;">Thank you for your Email. Please check your inbox to confirm your subscription.</div>', 'theme_text_domain' )
                 );
             } else {
                 //email is already in the database
                 $output = array(
                     'status'    => 'error',
                     'msg'       => __( 'Your Email is already in our system. Please check your inbox, to confirm your subscription.', 'theme_text_domain' )
                 );
             }
         else :
             $output = array(
                 'status'    => 'error',
                 'msg'       => __( 'Please enter your valid email.', 'theme_text_domain' )
             );
         endif;
    else :
        $output = array(
            'status'    => 'error',
            'msg'       => __( 'An Error occurred. Please try again later.', 'theme_text_domain' )
        );
    endif;
    wp_send_json( $output );
}
add_action( 'wp_ajax_nopriv_ajax_subscribe_footer', 'jnz_tnp_ajax_subscribe_footer' );
add_action( 'wp_ajax_ajax_subscribe_footer', 'jnz_tnp_ajax_subscribe_footer' );


// Start List Category Name In Blog Single page Sidebar
function blog_category_post_id(){

$output='';

if(is_single()){
	global $post;

	//$terms = wp_get_object_terms($post->ID, 'blog-category', array('orderby' => 'term_id', 'order' => 'ASC') );
		   		
	$terms = get_terms( array(
    'taxonomy' => 'blog-category',
    'hide_empty' => true
	) );

	$output.='<div class="widget widget_categories"><h3 class="widget-title">Categories</h3>		<ul>';

	 if ( !empty( $terms ) ) {
		    foreach ( $terms as $term ) {
		    	$term_link = get_term_link( $term );
	     		$output.='<li class="cat-item cat-item-1"><a href="'.get_site_url().'/blog/?cat_list='.$term->term_id.'">'.$term->name.'</a></li>';
		}
	}
		
		$output.='</ul>
				</div>';

				echo $output;

	}

}
add_shortcode('blog_category_list','blog_category_post_id');

// End List Category Name In Blog Single page Sidebar




//All Content Hub Changes(21-04-20)





// Start Register Texonomy Content Hub Category (21-04-20)

function themename_custom_taxonomies_content_hub() {
	

$coach_method = array(
		'name' => _x( 'Content Hub Category', 'taxonomy general name' ),
		'singular_name' => _x( 'Content Hub Category', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search in Content Hub Category' ),
		'all_items' => __( 'All Content Hub Category' ),
		'most_used_items' => null,
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Content Hub Category' ), 
		'update_item' => __( 'Update Content Hub Category' ),
		'add_new_item' => __( 'Add new Content Hub Categoryy' ),
		'new_item_name' => __( 'New Content Hub Category' ),
		'menu_name' => __( 'Content Hub Category' ),
	);



	$args = array(
		'hierarchical' => true,
		'labels' => $coach_method,
		'show_ui' => true,
		'query_var' => true,
		'show_in_rest' => true,	
		'rewrite' => array( 'slug' => 'content-hub','with_front' => false, )

	);
    register_taxonomy( 'content-hub-category', array( 'post' ), $args );
}
add_action( 'init', 'themename_custom_taxonomies_content_hub', 0 );

// End Register Texonomy Content Hub Category (21-04-20)





// Start Content Hub Lavel 2 Listing Post Shortcode (21-04-20)

function  content_hub_level_2_post_list(){

    $output='';
	$post_ids=get_field('select_post_content_hub',get_the_ID());
		 
	$output.='<div class="product_cat_main">
	<div class="container_custom">
	<div class="row">';

	if ( !empty($post_ids) ) {
		    foreach ($post_ids as $post_id ) {

                  if (has_post_thumbnail( $post_id ) ){ 
 					  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'post-feature' );
 				   }else{
 				      $image[0] = get_template_directory_uri() . '/images/default_image-353x178.png';
 				   }
 				   $content = apply_filters('the_content', get_post_field('post_content', $post_id));
 				   $content = mb_strimwidth(wp_strip_all_tags($content), 0, 250, '');
                $output.='<div class="col_4">
				<div class="blog_category_list">';

		    	$output.='<div class="cat_img"><a href="'.get_permalink($post_id).'"><img src="'.$image[0].'"></a></div><div class="cat_content">';
		    	
		    		$output.='<h4 class="font16"><a href="'.get_permalink($post_id).'">'.get_the_title($post_id).'</a></h4>';	

	     		$output.='<p>'.$content.'</p>';
	     		$output.= '<a href="'.get_permalink($post_id).'" class="button">Read More</a>';
	     		$output.='</div>
				</div>
				</div>';
		}
	}
	else{

	}
	$output.='</div>
	</div>
	</div>';
	return $output;


}
add_shortcode('content_hub_level_2_post_list','content_hub_level_2_post_list');

// End Content Hub Lavel 2 Listing Post Shortcode (21-04-20)




// Start Content Hub Lavel 3 Listing Category Shortcode (21-04-20)
function content_hub_category_list_lavel_3(){

	$output='';

	$term_list=get_field('select_category_content_hub',get_the_ID());
	if($term_list){
	$terms = get_terms( array(
    'taxonomy' => 'content-hub-category',
    'include' => $term_list,	
    'orderby'  => 'include',
    'hide_empty' => false,
	) );

	$output.='<div class="product_cat_main">
	<div class="container_custom">
	<div class="row">';

	if ( !empty( $terms ) ) {
		    foreach ( $terms as $term ) {
		    	$term_link = get_term_link( $term );

		    	$term_thumbnail_id = get_field('add_images_hub','term_'.$term->term_id);
                    $image = wp_get_attachment_image_src($term_thumbnail_id, 'post-feature');
                    if (!$term_thumbnail_id) {
                        $image[0] = get_template_directory_uri() . '/images/default_image-353x178.png';
                    }

                $output.='<div class="col_4">
				<div class="blog_category_list">';

		    	$output.='<div class="cat_img"><a href="'.esc_url( $term_link ).'"><img src="'.$image[0].'"></a></div><div class="cat_content">';
	     		$output.='<h4 class="font16"><a href="'.esc_url( $term_link ).'">'.$term->name.'</a></h4>';

	     		$output.='<p>'.wp_trim_words( $term->description, 40, '' ).'</p>';
	     		$output.= "<a href=".esc_url( $term_link )." class='button'>Read More</a>";
	     		//$output.= "<a href='#' class='button'>Read More</a>";
	     		$output.='</div>
				</div>
				</div>';
		}
	}
	$output.='</div>
	</div>
	</div>';
	return $output;
}

}
add_shortcode("content_hub_category_list_lavel_3","content_hub_category_list_lavel_3");

// End Content Hub Lavel 3 Listing Category Shortcode (21-04-20)

// Start Set Content Hub Category Image size in Category Page(21-04-20) 
 add_image_size('category_img', 1111, 322, true);
// End Set Content Hub Category Image size in Category Page(21-04-20) 


// VG 062421 Turn off RSS feed
function turn_off_feed() {
wp_die( __('Our feed is currently off, please visit our homepage!') );
//wp_die( __('Our Feed is currently off,please visit our <a href=”'. get_bloginfo('insert_url_here') .'”>homepage</a>!’) );
}
add_action('do_feed', 'turn_off_feed', 1);
add_action('do_feed_rdf', 'turn_off_feed', 1);
add_action('do_feed_rss', 'turn_off_feed', 1);
add_action('do_feed_rss2', 'turn_off_feed', 1);
add_action('do_feed_atom', 'turn_off_feed', 1);
add_action('do_feed_rss2_comments', 'turn_off_feed', 1);
add_action('do_feed_atom_comments', 'turn_off_feed', 1);

