	</div><!-- end #main -->
	</div><!-- end #container .inner -->
	</div><!-- end #container -->
 
<!-- Start Edit Newsletter Form (14-04-20) -->
	<div id="mc_embed_signup">
		<form action="https://striketru.us20.list-manage.com/subscribe/post?u=fbc67bec9b446f88b4048fa32&amp;id=92fa2120e8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

			<div id="mc_embed_signup_scroll">
				<h3>
					<b>Sign up for <b>StrikeTru’s personalized</b> content, <span style="color:#f29e21">Be a part of our Journey! </span></b>
					</h3>
				<div class="mc-field-group">
					<input type="email" value="" name="EMAIL" class="required email" placeholder="Email Address" id="mce-EMAIL">
				</div>
		          
				<div style="position: absolute; left: -5000px;" aria-hidden="true">
					<input type="text" name="b_9fed41bdf95025e316d39574f_41191afea4" tabindex="-1" value="">
				</div>
				<div class="clear">
		                	<?php if(is_tax('blog-category') || is_single()){ ?>
		                	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
		                	<?php }else{ ?>
		                	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button all_popup">
		                <?php } ?>
				</div>
			</div>
		</form>
	</div>
	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

		  <!-- End Edit Newsletter Form (14-04-20) --> 
	<div id="fwrap">
	<div class="inner">
		<div class="footer-nav-row">
			<div class="inner-content">
				<div id="footbar">
					<?php /* if(get_theme_mod('footer_contact_status') == 'Yes') {?>
						<div class="fwidget" id="footer-widget-contact">
							<h3 class="fwidget-title"><?php echo get_theme_mod('footer_contact_title'); ?></h3>
							
							<?php
								$address = get_theme_mod('address');
								$phone = get_theme_mod('phone');
								$fax = get_theme_mod('fax');
								$email = get_theme_mod('email');
								
							?>
							<div class="contact-list">
								<div class="phone-num"><?php echo $phone; ?></div>	
								<div class="timings"><?php echo $fax; ?></div>
								<div class="mail-to"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
							</div>
							<!--<dl class="contact-list">
								<?php if($address) { ?><dt>Address:</dt><dd><?php echo wpautop($address); ?></dd><?php } ?> 
								<?php if($phone) { ?><dt>Phone:</dt><dd><?php echo $phone; ?></dd><?php } ?>
								 <?php if($fax) { ?><dt>Fax:</dt><dd><?php echo $fax; ?></dd><?php } ?> 
								<?php if($email) { ?><dt>Email:</dt><dd><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></dd><?php } ?>
							</dl>
							<div class="akeneo-silver-logo">
								<img class="" src="/wp-content/uploads/2016/09/akeneo_sliver_partner_h.png" alt="akeneo silver parter" />
							</div>
							 -->
						</div><!-- end #footer-widget-contact -->
						<?php } */?>

					<div class="footer-nav-links">

						<div class="fwidget-area" id="fwidget-area-1">
							<?php if ( is_active_sidebar( 'footer-widget-area-1' ) ) :  dynamic_sidebar( 'footer-widget-area-1'); endif; ?>
						</div>
				
						<div class="fwidget-area" id="fwidget-area-2">
							<?php if ( is_active_sidebar( 'footer-widget-area-2' ) ) :  dynamic_sidebar( 'footer-widget-area-2'); endif; ?>
						</div>
					</div><!-- end .footer-nav-links-->
				</div><!--end #footbar -->
	  		</div><!--end .inner-content -->
		</div><!-- end .footer-nav-row -->
		
		<div class="footer-rights-row">
			<div class="inner-content">
				<p>
					All content &copy; <a href="<?php bloginfo('url'); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"  style="color: rgb(0,255,0)"><font color="FF8000"><?php bloginfo( 'name' ); ?>, LLC</font></a>, 2013-2020. 
				</p>
				<div class="social-block">
					<?php 
						$twitter_url = get_theme_mod('twitter_url');
						$facebook_url = get_theme_mod('facebook_url');
						$linkedin_url = get_theme_mod('linkedin_url');
						$flickr_url = get_theme_mod('flickr_url');
						$rss_url = get_theme_mod('rss_url');
					?>
								
					<ul class="social-list">
						<?php if($rss_url) { ?><li class="foot-rss"><a target=_blank href="<?php echo $rss_url; ?>">RSS Feed</a></li><?php } ?>
						<?php if($flickr_url) { ?><li class="foot-flickr"><a target=_blank href="<?php echo $flickr_url; ?>">Flickr</a></li><?php } ?>
						<?php if($linkedin_url) { ?><li class="foot-linkedin"><a target=_blank href="<?php echo $linkedin_url; ?>">Linkedin</a></li><?php } ?>
						<?php if($facebook_url) { ?><li class="foot-facebook"><a target=_blank href="<?php echo $facebook_url; ?>">Facebook</a></li><?php } ?>
						<?php if($twitter_url) { ?><li class="foot-twitter"><a target=_blank href="<?php echo $twitter_url; ?>">Twitter</a></li><?php } ?>
					</ul>
				</div>
				<span class="footer-terms-nav">
					<!-- <a class="term-link" href="">Sitemap</a><span>|</span> -->
					<a class="term-link" href="/terms-conditions">Terms of Use</a><span>|</span>
					<a class="term-link" href="/privacy-policy">Privacy Policy</a> 
				</span>
				
			</div><!--end .footer-rights-row .inner-content -->
		</div><!--end .footer-rights-row -->

<!-- Start Newsletter Popup For Message (04-14-20) -->
		<div id="myModal" class="modal">
						  <div class="modal-content">
						    <div id="mce-responses" class="clear">
			                    <div class="response" id="mce-error-response" style="display:none"></div>
			                    <div class="response" id="mce-success-response" style="display:none"></div>
			                </div>
						  </div>
						</div>



		            <link rel="stylesheet" type="text/css" href="/wp-content/themes/cubelight/classic-081711.css" />
<script type="text/javascript">
	$('.all_popup').click(function(){
		debugger;
		var btn_click=jQuery('.mc-field-group .email').val();
		if(btn_click != ''){
    var chk_txt='';
    var myVar = setInterval(function(){ chk_txt=jQuery('#myModal #mce-responses #mce-success-response').text(); }, 1000);

    if(chk_txt == ''){
    	debugger;
        clearInterval(myVar);

         $('#myModal').show();
         setTimeout(function(){ $('#myModal').hide(); location.reload(); }, 3000);
    	     
    }
}
});
​
</script>
<script>
		$(document).ready(function(){
					$('.fwidget').on('click', function(event) {
						if( $(window).width() < 668){
									$(this).find('.textwidget').slideToggle(500);
									$(this).find('h3').toggleClass('active');
						}
					});
			  $('#promo').css('padding-top',$('#header').height());
			  
			  
			$('.wprmenu_parent_item_li').click(function() {
						
			})
					
		});
		$(window).resize(function(){
			if($(window).width() > 667){
				$('.textwidget').show();
			}
			
			  $('#promo').css('padding-top',$('#header').height());
		});
</script>


<!-- End Newsletter Popup For Message (04-14-20) -->
		
	
	</div><!--end #frwap .inner -->
	</div><!--end #frwap -->
	
</div><!--end #wrapper -->

<!--begin of body code-->	
<?php if(get_theme_mod('body_code_status') == "Yes") echo stripslashes(get_theme_mod('body_code')); ?>
<!--end of body code-->

<?php wp_footer(); ?>


<script>
//begin customization
//display top menu in 1 column if no 'description' populated
//	jQuery( ".sub-menu" ).each(function( i ) {
//var _thisul = jQuery(this);
//jQuery( _thisul.children('li.menu-item') ).each(function( i ) {
//
//var _thisele = jQuery(this);
//if(_thisele.find('p').length <= 0)
//{
//	_thisele.css('width','100%');
//}
//});
//});
//end customization


//begin customization
//display top menu in 1 column if less than 6 items
// jQuery( ".sub-menu" ).each(function( i ) {
// var _thisul = jQuery(this);
// var _thisli = jQuery(_thisul.children('li.menu-item')).length;
// if(_thisli <= 6)
// {
// 	//100% causes menu like 'Case Studies' to be split into 2 lines !!
// 	_thisul.css('width','100%');
// 	//300% causes menu to split into two columns !!
// 	//_thisul.css('width','300%');
// 	//_thisul.css('width','200%');
// }
// });
//end customization
</script>
<!--Start of Tawk.to Script-
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f36a2044c7806354da668ef/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
-End of Tawk.to Script-->
<script src="//code.tidio.co/s5zalbm3dglia1wk0mqyr1on5vrf4nyz.js" async></script>

</body>
</html>
