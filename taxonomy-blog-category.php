<?php
get_header();
$tax = get_query_var('term');
$term = get_term_by('slug', $tax, 'blog-category');
$args = array(
    'post_type' => 'post',
    'fields' => 'ids',
    'tax_query' => array(
        array(
            'taxonomy' => 'blog-category',
            'field' => 'term_id',
            'terms' => $term->term_id,
        )
    )
);
$latest_post = get_posts($args);

$all_tags_in = array();
for ($i = 0; $i <= count($latest_post); $i++) {

    $post_tags = get_the_tags($latest_post[$i]);
    if (!empty($post_tags)) {
        foreach ($post_tags as $tag) {
         
            $all_tags_in[] = $tag->term_id;
        }
    }
}

$all_tags_in = array_unique($all_tags_in);

?>

<div id="content">
    <!-- <div class="inner-content blogs-main-wrapper"> -->
    <div class=" dynamic_option_get blog_cat_sec">
        <input type="hidden" name="tax" id="tax" value="<?php echo $term->term_id;?>">
        <div class="container_custom">
            <div class="row">
                <div class="col_12 cat_top_content">
                    <form action="<?php echo get_term_link($tax, 'blog-category') ?>" method="post" class="search_form_quality">
                        <input type="text" name="search_from_tax" id="search_from_tax" placeholder="Search" class="form_control search_input" value="<?php echo $_POST['search_from_tax'];?>">
                        <input type="submit" name="submit" id="submit" value="submit" class="search_btn">
                    </form>
                    <input type="hidden" name="tax-name" id="tax-name" value="<?php echo $tax; ?>">
                    <input type="hidden" name="ajax-url" id="ajax-url" value="<?php echo admin_url('admin-ajax.php'); ?>">
                    <p class="top_content_blog_cat"><?php echo $term->description; ?></p>
                </div>
            </div>  
            <div class="product_cat_main row " id="post_content">
                <?php 
                
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$query_args = array (
   'post_type' => 'post',
   'posts_per_page' => 3,
   'orderby' => 'ASC',
   's'=>$_POST['search_from_tax'],
   'paged' => $paged,
    'tax_query' => array(
        array(
            'taxonomy'  => 'blog-category',
            'field'     => 'term_id',
            'terms'     => $term->term_id,
        )
    ),
);

$query = new WP_Query($query_args);
                
                if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="blog_detail_content col_4">
                            <?php
                            global $post;
                            $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'post-feature');
                            $img_default = get_template_directory_uri() . '/images/default_image.png';
                            $post_date = get_the_date('j/m/Y');
                            $all_tags = array();
                            ?>
                            <div id="post-<?php the_ID(); ?>" class="blog_category_list">
                                <div class="cat_img">
                                    <?php if ($large_image_url[0]) { ?>
                                       <a href="<?php the_permalink(); ?>"> <img src="<?php echo $large_image_url[0]; ?>"></a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $img_default; ?>"></a>
                                    <?php } ?>
                                </div>
                                <div class="cat_content">
                                    <div class="entry-metadata-info">
                                   
                                        <h4 class="font16"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'themejunkie'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                                        <p><?php the_excerpt(); ?></p>
                                    </div>
                                    <div class="entry">
                                        <a href="<?php the_permalink(); ?>" class="button">READ MORE</a>
                                    </div>
                                </div>									
                            </div><!-- end .post --> 
                        </div>
                        <?php
                    endwhile;
                endif;
                ?><?php wp_reset_query(); ?>
            </div>
            <!-- end .blog-posts -->
            <?php if ($all_tags_in) { ?>
                <div class="all-tags">
                    <h4 class="font16">Tags:</h4>
                    <?php
                    foreach ($all_tags_in as $taging) {
                        $tag = get_tag($taging);
                        //echo '<a href="' . get_tag_link($taging) . '">' . $tag->name  . '</a>';
                        echo '<a href="javascript:void(0);">' . $tag->name . '</a>';
                    }
                    ?>
                </div>
            <?php } ?>
        </div>
    </div><!-- end .inner-content -->
    <div class="like_our_content_sec">
        <div class="container_custom">	
            <div class="row">
                <div class="col_12">	
                    <div id="mc_embed_signup_scroll">
                        <h1>Like Our Content?</h1>
                        <div class="row">
                            <div class="col_8 col_left">
            <!-- Start Edit Newsletter Form (14-04-20) -->
        <div  id="mc_embed_signup">
                <form action="https://striketru.us20.list-manage.com/subscribe/post?u=fbc67bec9b446f88b4048fa32&amp;id=92fa2120e8" method="post" name="mc-embedded-subscribe-form" class="validate footer_newsletter" target="_blank" novalidate>

                                <div id="mc_embed_signup_scroll" class="footer_newsletter_scroll">
                       
                    <div class="mc-field-group1">
                        <input type="email" value="" name="EMAIL" class="required email search_input" placeholder="Email Address" id="mce-EMAIL">
                    </div>
                                <div class="field-button"><input type="submit" value="SUBSCRIBE" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                       
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9fed41bdf95025e316d39574f_41191afea4" tabindex="-1" value=""></div>
                        
                        </div>
                    </form>
                    </div>
                     <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
    <!-- End Edit Newsletter Form (14-04-20) -->
                                <?php // echo do_shortcode('[newsletter_form form="1"]'); ?>
                            </div>
                            <div class="col_4">
                                <span class="or_txt">OR</span>
                                <a href="/contact-us/" class="button">Contact Us</a>
                                <span class="more_detail">For More Details</span>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".entry-tags-blog a").attr('href', 'javascript:void(0)');
    });
</script>