<?php
$theme = get_current_theme();
$themename = "CubeLight";
$modsname = 'theme_mods_'.$theme;
$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );

$cat_array = get_categories('parent=0&hide_empty=0');
$page_array = get_pages('parent=0&hide_empty=0');
$pages_number = count($page_array);

$site_pages = array();
$site_cats = array();

foreach ($page_array as $pagg) {
	$site_pages[$pagg->ID] = htmlspecialchars($pagg->post_title);
	$page_ids[] = $pagg->ID;
}

foreach ($cat_array as $categs) {
	$site_cats[$categs->cat_ID] = $categs->cat_name;
	$cat_ids[] = $categs->cat_ID;
}


$options = array (
// Begin Primary Metabox Holder
	array( 	"type" => "box-container-open"),

	// General Settings
	array(  "name" => __( 'General Settings', 'themejunkie' ),
			"type" => "box-open"),
			
	array(  "name" => __( 'Color Scheme', 'themejunkie' ),
            "id" => "color_scheme",
            "type" => "select",
            "std" => "Blue",
            "options" => array("Blue", "Brown", "Green", "Red", "Orange", "Pink", "Purple", "Aqua", "Gray")),
			
	array(  "name" => __( 'Logo Type', 'themejunkie' ),
            "id" => "logo",
            "type" => "select",
            "std" => "Image Logo",
            "options" => array("Image Logo", "Text Logo")),
	array(  "name" => __( 'Image Logo URL', 'themejunkie' ),
            "id" => "logo_url",
			"desc" => "fixed width: 160x48px",
            "type" => "text"),
	
	array( 	"type" => "box-close"),
	
	// Tagline Settings
	array(  "name" => __( 'Tagline Settings', 'themejunkie' ),
			"type" => "box-open"),
	
	array(  "name" => __( 'Home Tagline', 'themejunkie' ),
            "id" => "home_tagline",
            "type" => "textarea",
			"std" => "<h2>This is Tagline for Home Page</h2> \nEnter some text instead of this  in Theme Options &raquo; Tagline Settings"),
	
	array(  "name" => __( 'Search Tagline', 'themejunkie' ),
            "id" => "search_tagline",
            "type" => "textarea",
			"std" => "This is Tagline for Search Page. \nEnter some text instead of this  in Theme Options &raquo; Tagline Settings"),
	
	array(  "name" => __( 'Archive Tagline', 'themejunkie' ),
            "id" => "archive_tagline",
            "type" => "textarea",
			"std" => "This is Tagline for Archive Page. \nEnter some text instead of this  in Theme Options &raquo; Tagline Settings"),
			
	array(  "name" => __( '404 Tagline', 'themejunkie' ),
            "id" => "404_tagline",
            "type" => "textarea",
			"std" => "This is Tagline for 404 Page. \nEnter some text instead of this  in Theme Options &raquo; Tagline Settings"),
			
	array( 	"type" => "box-close"),
	
	// Blog Settings
	array(  "name" => __( 'Blog Settings', 'themejunkie' ),
			"type" => "box-open"),
	
	array(  "name" => __( 'Blog Page', 'themejunkie'),
            "id" => "blog_page",
            "type" => "droppage"),	
	array(  "name" => __( 'Display Blog Page in Breadcrumb', 'themejunkie'),
            "id" => "display_blog_page_in_breadcrumb",
			"std" => "Yes",
            "type" => "select",
			"options" => array("Yes","No")),
			
	array( 	"type" => "box-close"),
	
	// Portfolio Settings
	array(  "name" => __( 'Portfolio Settings', 'themejunkie' ),
			"type" => "box-open"),
	array(  "name" => __( 'Portfolio Page', 'themejunkie'),
            "id" => "folio_page",
            "type" => "droppage"),	
	array(  "name" => __( 'Display Portfolio Page in Breadcrumb', 'themejunkie'),
            "id" => "display_folio_page_in_breadcrumb",
			"std" => "Yes",
            "type" => "select",
			"options" => array("Yes","No")),
	array(  "name" => __( 'Portfolio Categories', 'themejunkie'),
            "id" => "folio_cats",
            "type" => "checkbox",
			"wptype" => "cat",
            "std" => "",
            "options" => $cat_ids),
	array(  "name" => __( '	Display Portfolio Dropdown categories in Portfolio Page and Per Portfolio Category', 'themejunkie'),
            "id" => "display_portfolio_dropcats",
			"std" => 'Yes',
			"type" => "select",
			"options" => array('Yes','No')),
	array(  "name" => __( '	Number of <b>Portfolio Post</b> in per page', 'themejunkie'),
            "id" => "folio_num",
			"std" => 9,
			"type" => "select",
			"options" => array(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21)),
	array(  "name" => __( 'Thumbnail Key', 'themejunkie' ),
            "id" => "thumb_key",
            "type" => "text",
            "std" => "thumb"),
	array(  "name" => __( 'Portfolio Post Thumbnail Height', 'themejunkie' ),
            "id" => "folio_thumb_height",
			"class" => "small-text",
			"desc" => "fixed width: 288px",
			"type" => "text",
            "std" => 162),
	array(  "name" => __( 'Display Single Portfolio Post Thumbnail', 'themejunkie'),
            "id" => "display_single_folio_thumbnail",
			"std" => "Yes",
            "type" => "select",
			"options" => array("Yes","No")),

	array(  "name" => __( 'Single Portfolio Post Thumbnail Height', 'themejunkie' ),
            "id" => "single_folio_thumb_height",
			"class" => "small-text",
			"desc" => "fixed width: 608px",
			"type" => "text",
            "std" => 342),
	array(  "name" => __( 'Display Comments in Single Poftfolio Post', 'themejunkie'),
            "id" => "display_single_folio_comments",
			"std" => "No",
            "type" => "select",
			"options" => array("Yes","No")),

	
	array( 	"type" => "box-close"),
	
	// Contact Settings
	array(  "name" => __('Contact Settings', 'themejunkie'),
			"type" => "box-open"),
			
	array(  "name" => __( 'Contact Page', 'themejunkie'),
            "id" => "contact_page",
            "type" => "droppage"),	
	
	array(  "name" => "Contact Info",
            "type" => "sub-title"),
	array(  "name" => "Contact Info Title",
            "id" => "contact_info_title",
            "std" => "Contact Info",
            "type" => "text"),
	array(  "name" => "Address",
            "id" => "address",
            "type" => "textarea"),
	array(  "name" => "Phone",
            "id" => "phone",
            "type" => "text"),
	array(  "name" => "Fax",
            "id" => "fax",
			"type" => "text"),
	array(  "name" => "Email",
            "id" => "email",
            "std" => get_option('admin_email'),
            "type" => "text"),
	array(  "name" => "After Contact Info",
			"desc" => "Maybe you want to enter some text for more contact infomation, for example, to insert a google map.",
            "id" => "after_contact_info",
            "type" => "textarea"),
	
	array(  "name" => "Social Links",
            "type" => "sub-title"),
	array(  "name" => "Social Links Title",
            "id" => "social_links_title",
            "std" => "Find us elsewhere",
            "type" => "text"),
	array(  "name" => "RSS Feed URL",
            "id" => "rss_url",
            "std" => get_bloginfo('rss2_url'),
            "type" => "text"),
	array(  "name" => "Twitter URL",
            "id" => "twitter_url",
			"type" => "text"),
	array(  "name" => "Facebook URL",
            "id" => "facebook_url",
            "type" => "text"),
	array(  "name" => "Linkedin URL",
            "id" => "linkedin_url",
            "type" => "text"),
	array(  "name" => "Flickr URL",
            "id" => "flickr_url",
            "type" => "text"),
			
	array(  "name" => "Conatct Form",
            "type" => "sub-title"),
	array(  "name" => "Contact Form Title",
            "id" => "contact_form_title",
            "std" => "Contact Form",
            "type" => "text"),
	array(  "name" => "Contact Form Email",
			"desc" => "Your email for receive submissions from contact form",
            "id" => "contact_form_email",
            "std" => get_option('admin_email'),
            "type" => "text"),
	array(  "name" => "Before Contact Form",
			"desc" => "Maybe you want to enter some text as tips for contact form",
            "id" => "before_contact_form",
            "type" => "textarea"),
	array(	"name" => "Footer Contat",
			"type" => "sub-title"),
	array(  "name" => __( 'Display Contact Info and Social Links in Footer', 'themejunkie'),
            "id" => "footer_contact_status",
			"std" => "Yes",
            "type" => "select",
			"options" => array("Yes","No")),
	array(  "name" => __( 'Footer Contact Title', 'themejunkie'),
            "id" => "footer_contact_title",
			"std" => "Get in Touch",
            "type" => "text"),
			
	array( 	"type" => "box-close"),
	


	// Integration Settings
	array(  "name" => __( 'Code Integration', 'themejunkie'),
			"type" => "box-open"),
			
	array(  "name" => __( 'Enable head code', 'themejunkie'),
            "id" => "head_code_status",
			"std" => "No",
            "type" => "select",
			"options" => array("Yes","No")),
	array(  "name" => __( '<span class="description">Add code to the < head > of your site</span>', 'themejunkie'),
            "id" => "head_code",
            "type" => "textarea",
            "std" => ""),

	array(  "name" => __( 'Enable body code', 'themejunkie'),
            "id" => "body_code_status",
			"std" => "No",
            "type" => "select",
			"options" => array("Yes","No")),            	
	array(  "name" => __( '<span class="description">Add code to the < body > (good tracking codes such as google analytics)</span>', 'themejunkie'),
            "id" => "body_code",
            "type" => "textarea",
            "std" => ""),
	array( 	"type" => "box-close"),
	

	array( 	"type" => "box-container-close"), 
// End Primary Metabox Holder

// Begin Secondary Metabox Holder
	array( 	"type" => "box-container-open"),
	
	// Home Settings - Featured Services
	array(  "name" => __( 'Home Settings - Featured Services', 'themejunkie'),
			"type" => "box-open"),
			
	array(  "name" => __( 'Enable this area', 'themejunkie'),
            "id" => "home_services_status",
            "type" => "select",
			"options" => array("Yes","No"),
			"std" => "Yes"),		
			
	array(  "name" => __( 'Featured Services 1', 'themejunkie'),
			"type" => "sub-title"),
	array(  "name" => __( 'Icon url (32x32px)', 'themejunkie'),
            "id" => "fs_icon_1",
			"type" => "text",
			"std" => get_bloginfo('template_url').'/test/fs1.png'),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "fs_title_1",
            "type" => "text",
			"std" => "Featured Services 1"),
	array(  "name" => __( 'Description', 'themejunkie'),
            "id" => "fs_desc_1",
            "type" => "textarea",
			"std" => "Enter some text as service description."),
	array(  "name" => __( 'Link for "Read more"', 'themejunkie'),
            "id" => "fs_more_link_1",
            "type" => "text",
			"std" => "#"),
			
	array(  "name" => __( 'Featured Services 2', 'themejunkie'),
			"type" => "sub-title"),
	array(  "name" => __( 'Icon url (32x32px)', 'themejunkie'),
            "id" => "fs_icon_2",
			"type" => "text",
			"std" => get_bloginfo('template_url').'/test/fs2.png'),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "fs_title_2",
            "type" => "text",
			"std" => "Featured Services 2"),
	array(  "name" => __( 'Description', 'themejunkie'),
            "id" => "fs_desc_2",
            "type" => "textarea",
			"std" => "Enter some text as service description."),
	array(  "name" => __( 'Link for "Read more"', 'themejunkie'),
            "id" => "fs_more_link_2",
            "type" => "text",
			"std" => "#"),
			
	array(  "name" => __( 'Featured Services 3', 'themejunkie'),
			"type" => "sub-title"),
	array(  "name" => __( 'Icon url (32x32px)', 'themejunkie'),
            "id" => "fs_icon_3",
			"type" => "text",
			"std" => get_bloginfo('template_url').'/test/fs3.png'),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "fs_title_3",
            "type" => "text",
			"std" => "Featured Services 3"),
	array(  "name" => __( 'Description', 'themejunkie'),
            "id" => "fs_desc_3",
            "type" => "textarea",
			"std" => "Enter some text as service description."),
	array(  "name" => __( 'Link for "Read more"', 'themejunkie'),
            "id" => "fs_more_link_3",
            "type" => "text",
			"std" => "#"),
	
	array(  "type" => "box-close"),
	
	// Home Settings - Home About
	array(  "name" => __( 'Home Settings - Home About', 'themejunkie'),
			"type" => "box-open"),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "home_about_title",
            "type" => "text",
			"std" => "Who are we?"),
	array(  "name" => __( 'About infomation', 'themejunkie'),
            "id" => "home_about_info",
            "type" => "textarea",
			"std" => "Enter some text as about infomation."),
	array(  "type" => "box-close"),
	
	// Home Settings - Home Portfolio
	array(  "name" => __( 'Home Settings - Home Portfolio', 'themejunkie'),
			"type" => "box-open"),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "home_folio_title",
            "type" => "text",
			"std" => "Featured Work"),
	array(  "name" => __( '	Number of <b>Portfolio Posts</b>', 'themejunkie'),
            "id" => "home_folio_num",
			"std" => 4,
			"type" => "select",
			"options" => array(2,3,4,5,6,7,8,9,10)),
	array(  "type" => "box-close"),
	
	// Home Settings - Home Quote
	array(  "name" => __( 'Home Settings - Home Quote', 'themejunkie'),
			"type" => "box-open"),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "home_quote_title",
            "type" => "text",
			"std" => "Testimonials"),
	array(  "name" => __( 'More link', 'themejunkie'),
            "id" => "home_quote_more_url",
            "type" => "text",
			"std" => ""),
	array(  "name" => __( 'Quote content', 'themejunkie'),
            "id" => "home_quote_content",
            "type" => "textarea"),
	array(  "name" => __( 'Quote author\'s name', 'themejunkie'),
            "id" => "home_quote_author_name",
            "type" => "text",
			"std" => "Mystery Man"),
	array(  "name" => __( 'Quote author\'s url', 'themejunkie'),
            "id" => "home_quote_author_url",
            "type" => "text",
			"std" => ""),
	array(  "name" => __( 'Quote author\'s Occupation ', 'themejunkie'),
            "id" => "home_quote_author_opp",
            "type" => "text",
			"std" => ""),
	array(  "type" => "box-close"),
	
	// Home Settings - Home News
	array(  "name" => __( 'Home Settings - Home News', 'themejunkie'),
			"type" => "box-open"),
	array(  "name" => __( 'Title', 'themejunkie'),
            "id" => "home_news_title",
            "type" => "text",
			"std" => "Latest News"),
	array(  "name" => __( '	Number of <b>Blog Posts</b>', 'themejunkie'),
            "id" => "home_news_num",
			"std" => 3,
			"type" => "select",
			"options" => array(2,3,4,5,6,7,8,9,10)),
	array(  "type" => "box-close"),
	
	
	
	array( 	"type" => "box-container-close"),
// End Secondary Metabox Holder

/**
 * Default Theme Options, include common form elements.
	array( 	"type" => "box-container-open"),
	array(  "name" => "Default Theme Options",
			"type" => "box-open"),
    array(  "name" => "Radio Selection Set",
			"desc" => "This is a descriptions",
            "id" => "radio",
            "type" => "radio",
            "std" => "3",
            "options" => array("3", "2", "1")),
    array(  "name" => "Text Box",
			"desc" => "This is a descriptions",
            "id" => "text",
            "std" => "Some Default Text",
            "type" => "text"),
    array(  "name" => "Bigger Text Box",
			"desc" => "This is a descriptions",
            "id" => "textarea",
            "std" => "Default Text",
            "type" => "textarea"),
    array(  "name" => "Dropdown Selection Menu",
			"desc" => "This is a descriptions",
            "id" => "select",
            "type" => "select",
            "std" => "Default",
            "options" => array("Default", "Option 1", "Option 2")),
    array(  "name" => "Checkbox selection set",
			"desc" => "This is a descriptions",
            "id" => "checkbox",
            "type" => "checkbox",
            "std" => "Default",
            "options" => array("Default", "Option 1", "Option 2")),
    array(  "name" => "Multiple selection box",
			"desc" => "This is a descriptions",
            "id" => "multiselect",
            "type" => "multiselect",
            "std" => "Default",
            "options" => array("Defaults", "Option 1s", "Option 2s")),
	array( 	"type" => "box-close"),
	array( 	"type" => "box-container-close")
*/
);

function hello_options() {
	global $themename, $modsname, $options;
	foreach ($options as $value) {
		$key = $value['id'];
		$val = $value['std'];
		$new_options[$key] = $val; 
	}
	add_option($modsname, $new_options );
}
add_action('wp_head', 'hello_options');
add_action('admin_head', 'hello_options');

function mytheme_add_admin() {
    global $themename, $modsname, $options;
	$settings = get_option($modsname);
    if ( $_GET['page'] == basename(__FILE__) ) {
        if ( 'save' == $_REQUEST['action'] ) {
			foreach ($options as $value) {
				if(($value['type'] === "checkbox" or $value['type'] === "multiselect" ) and is_array($_REQUEST[ $value['id'] ]))
					{ $_REQUEST[ $value['id'] ]=implode(',',$_REQUEST[ $value['id'] ]);
					}
				$key = $value['id']; 
				$val = $_REQUEST[$key];
				$settings[$key] = $val;
			}
			update_option($modsname, $settings);                   
			header("Location: themes.php?page=theme-options.php&saved=true");
            die;
        } else if( 'reset' == $_REQUEST['action'] ) {
			foreach ($options as $value) {
				$key = $value['id'];
				$std = $value['std'];
				$new_options[$key] = $std;
			}
			update_option($modsname, $new_options );
            header("Location: themes.php?page=theme-options.php&reset=true");
            die;
        }
    }
    add_theme_page($themename." Theme Options", $themename. " Theme Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');
}

function mytheme_admin() {
    
	global $themename, $modsname, $options;
	$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	
    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' '.__('settings saved.', 'themejunkie').'</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' '.__('settings reset.', 'themejunkie').'</strong></p></div>'; ?>
	
	<div class="wrap">
		<div class="icon32" id="icon-themes"><br></div>
		<h2><?php echo $themename; ?> <?php _e('Theme Options','themejunkie'); ?></h2>
		<div id="poststuff">
			

			<form method="post">
			<div class="metabox-holder">
				<?php 
					$settings = get_option($modsname);
					foreach ($options as $value) { 
						$id = $value['id'];
						$std = $value['std'];
						if (($value['type'] == "text") || ($value['type'] == "textarea") || ($value['type'] == "select") || ($value['type'] == "multiselect") || ($value['type'] == "checkbox") || ($value['type'] == "radio") || ($value['type'] == "dropcat") || ($value['type'] == "droppage")) { ?>
							<tr>
								<th><label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?>:</label></th>
								<td>
						<?php } ?>
						
						<?php if ($value['type'] == "box-container-open") { ?>
							<div class="postbox-container" style="width:49%;">
								<div id="normal-sortables" class="meta-box-sortables ui-sortable">
						<?php } elseif ($value['type'] == "box-container-close") { ?>
								</div><!-- end .meta-box-sortables -->
							</div><!-- end .post-box-container -->
							
							
						<?php } elseif ($value['type'] == "box-open") { ?>
							<div class="postbox ">
								<div class="handlediv" title="<?php _e('Show/Hide','themejunkie'); ?>"><br></div>
								<h3 class="hndle"><span><?php echo $value['name']; ?></span></h3>
								<div class="inside">
								<table class="form-table">
						<?php } elseif ($value['type'] == "box-close") { ?>
								</table><!-- end .form-table -->
								</div><!-- end .inside -->
							</div><!-- end .postbox -->
						
						<?php } elseif ($value['type'] == "sub-title") { ?>
							<tr><td colspan ="2" style="margin:0 0 10px 0;border-bottom:1px solid #DDD;font-size:13px;"><strong><?php echo $value['name']; ?></strong></td></tr>
							
						<?php } elseif ($value['type'] == "about") { ?>
							<tr>
								<th><?php _e( 'Theme:', $domain ); ?></th>
								<td><a href="<?php echo $theme_data['URI']; ?>" title="<?php echo $theme_data['Title']; ?>"><?php echo $theme_data['Title']; ?> <?php echo $theme_data['Version']; ?></a></td>
							</tr>
							<tr>
								<th><?php _e( 'Author:', $domain ); ?></th>
								<td><?php echo $theme_data['Author']; ?></td>
							</tr>
							<tr>
								<th><?php _e( 'Description:', $domain ); ?></th>
								<td><?php echo $theme_data['Description']; ?></td>
							</tr>
						<?php } elseif ($value['type'] == "text") { ?>
							<input <?php if($value['class'] == 'small-text') echo 'class="small-text"'; else echo 'class="regular-text"';?>name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php echo $settings[$id]; ?>" size="40" />
							
						<?php } elseif ($value['type'] == "textarea") { ?>
							<textarea name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" cols="40" rows="5"/><?php echo stripslashes($settings[$id]); ?></textarea>
						<?php } elseif ($value['type'] == "select") { ?>
							<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
								<?php foreach ($value['options'] as $option) { ?>
								<option<?php if ( $settings[$id] == $option) { echo ' selected="selected"'; }?>><?php echo $option; ?></option>
								<?php } ?>
							</select>
						<?php } elseif ($value['type'] == "multiselect") { ?>
							<select  multiple="multiple" size="3" name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>" style="height:100px;">
								<?php $ch_values=explode(',',$settings[$id] ); foreach ($value['options'] as $option) { ?>
								<option<?php if ( in_array($option,$ch_values)) { echo ' selected="selected"'; }?> value="<?php echo $option; ?>"><?php echo $option; ?></option>
								<?php } ?>
							</select>
						<?php } elseif ($value['type'] == "radio") { ?>
							<?php foreach ($value['options'] as $option) { ?>
									<?php echo $option; ?><input name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php echo $option; ?>" 									<?php if ( $settings[$id] == $option) { echo 'checked'; } ?>/>
							<?php } ?>
						<?php } elseif ($value['type'] == "checkbox") { ?>
							<?php 
								$ch_values=explode(',',$settings[$id]);
								foreach ($value['options'] as $option) { ?>
									<input name="<?php echo $value['id']; ?>[]" type="<?php echo $value['type']; ?>" value="<?php echo $option; ?>" <?php if ( in_array($option,$ch_values)) { echo ' checked="checked"'; } ?> />
									<?php
									if($value['wptype'] == "cat") {
										echo get_cat_name($option); 
									} elseif($value['wptype'] == "page") {
										$page_data = get_page($option); 
										echo $page_data->post_title;
									} else {
										echo $option; 
									} ?>
									<br/>
									
<?php 		} ?>
						<?php } elseif ($value['type'] == "dropcat") { ?>
							<?php wp_dropdown_categories(array('selected' => get_theme_mod($value['id']), 'name' => $value['id'], 'orderby' => 'Name' , 'hierarchical' => 1, 'hide_empty' => '0' )); ?>
						<?php } elseif ($value['type'] == "droppage") { ?>
							<?php wp_dropdown_pages(array('selected' => get_theme_mod($value['id']), 'name' => $value['id'], 'depth' => 1)); ?>
						<?php } ?>
						
						<?php if(isset($value['desc'])){ ?><br/><span class="description"><?php echo $value['desc']?></span><?php } ?>
						
						<?php if (($value['type'] == "text") || ($value['type'] == "textarea") || ($value['type'] == "select") || ($value['type'] == "multiselect") || ($value['type'] == "checkbox") || ($value['type'] == "radio") || ($value['type'] == "dropcat") || ($value['type'] == "droppage")) { ?>
							</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</div><!-- end .metabox-holder -->
				<p id="submit-saved" class="submit">
					<input id="submit-saved" class="button-primary" name="save" type="submit" value="<?php _e('Save Changes','themejunkie') ;?>" />    
					<input type="hidden" name="action" value="save" />
				</p>
			</form>
			
			<form method="post">
				<p id="submit-reset" class="submit">
					<input id="submit-reset" name="reset" type="submit" value="<?php _e('Reset to Defaults','themejunkie') ;?>" />
					<input type="hidden" name="action" value="reset" />
				</p>
			</form>
	</div><!-- end #wrap -->
<?php } ?>
<?php add_action('admin_menu', 'mytheme_add_admin');

function tj_theme_options_css_js() {
	if ( $_GET['page'] == basename(__FILE__) ) {
	// wp_enqueue_script( 'common' );
	// wp_enqueue_script( 'wp-lists' );
	wp_enqueue_script( 'postbox' ); ?>
	<script type="text/javascript">
		jQuery(document).ready(function(){ // Toglle .postbox
			jQuery(".postbox").addClass("closed");
				jQuery(".handlediv").click(function(){
				jQuery(this).parent(".postbox").toggleClass("closed");
			});
		});
	</script>
	<style type="text/css">
		#submit-saved{clear:left;}
		.submit{float:left;margin:0 10px 0 0;}
	</style>
	
	<?php
	
}}
add_action('admin_head','tj_theme_options_css_js');
 ?>