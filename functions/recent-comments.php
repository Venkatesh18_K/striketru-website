<?php 

function tj_recent_comments_options() {
	global $defaults;
	$defaults = array( 
			'title' => __('Recent Comments', 'theme-junkie'), 
			'limit' => '5', 
			'prefix' => 'on', 
			'suffix' => 'said:', 
			'show_date' => 'Relative', 
			'date_format' => 'F j, Y',
			'show_count' => false,
			'excerpt' => false,
			'show_post_title' => false ,
			'author' => false,
			'gravatar' => false,
			'gravatar_size' => '40',
			'gravatar_spacing' => '10',
			'comment_length' => '80',
			'exclude_admin_email' => get_option('admin_email')
	);
}

class TJ_Recent_Comments_Widget extends WP_Widget {

	function TJ_Recent_Comments_Widget() {
		$widget_ops = array( 'classname' => 'widget_recent_comments tj_widget_recent_comments', 'description' => __('The advanced most recent comments.', 'theme-junkie') );
		$control_ops = array( 'width' => 400, 'height' => 850, 'id_base' => 'theme-junkie-recent-comments' );
		$this->WP_Widget( 'tj-recent-comments', __('TJ Recent Comments', 'theme-junkie'), $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		$title = apply_filters('widget_title', $instance['title'] );
		$parameters = array(
			'title' => $title,
			'limit' => (int)$instance['limit'],
			'suffix' => $instance['suffix'],
			'prefix' => $instance['prefix'],
			'show_date' => $instance['show_date'],
			'date_format' => $instance['date_format'],
			'comment_length' => (int)$instance['comment_length'],
			'gravatar_size' => (int)$instance['gravatar_size'],
			'gravatar_spacing' => (int)$instance['gravatar_spacing'],
			'exclude_admin_email' => $instance['exclude_admin_email'],
			'show_post_title' => isset( $instance['show_post_title'] ) ? $instance['show_post_title'] : false,
			'author' => isset( $instance['author'] ) ? $instance['author'] : false,
			'gravatar' => isset( $instance['gravatar'] ) ? $instance['gravatar'] : false,
			'show_count' => isset( $instance['show_count'] ) ? $instance['show_count'] : false,
			'excerpt' => isset( $instance['excerpt'] ) ? $instance['excerpt'] : false
		);
		
		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		tj_recent_comments($parameters);
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['limit'] 	= $new_instance['limit'];
		$instance['prefix'] = $new_instance['prefix'];
		$instance['suffix'] = $new_instance['suffix'];
		$instance['show_date'] = $new_instance['show_date'];
		$instance['date_format'] = $new_instance['date_format'];
		$instance['comment_length'] = $new_instance['comment_length'];
		$instance['show_count'] = $new_instance['show_count'];
		$instance['show_post_title'] = $new_instance['show_post_title'];
		$instance['author']	= $new_instance['author'];
		$instance['excerpt'] = $new_instance['excerpt'];
		$instance['gravatar'] = $new_instance['gravatar'];
		$instance['gravatar_size'] = $new_instance['gravatar_size'];
		$instance['gravatar_spacing'] = $new_instance['gravatar_spacing'];
		$instance['exclude_admin_email'] = $new_instance['exclude_admin_email'];
		
		return $instance;
	}
	
	function form( $instance ) {
		$admin_email = get_option('admin_email');
		tj_recent_comments_options();
		global $defaults;
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme-junkie'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e('Show Date:', 'theme-junkie'); ?></label> 
			<select id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" class="widefat">
				<option <?php if ( 'Disable' == $instance['show_date'] ) echo 'selected="selected"'; ?>>Disable</option>
				<option <?php if ( 'Relative' == $instance['show_date'] ) echo 'selected="selected"'; ?>>Relative</option>
				<option <?php if ( 'Custom' == $instance['show_date'] ) echo 'selected="selected"'; ?>>Custom</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'date_format' ); ?>"><?php _e('Date Format:', 'theme-junkie'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id( 'date_format' ); ?>" name="<?php echo $this->get_field_name( 'date_format' ); ?>" value="<?php echo $instance['date_format']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'prefix' ); ?>"><?php _e('Prefix:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'prefix' ); ?>" name="<?php echo $this->get_field_name( 'prefix' ); ?>" value="<?php echo $instance['prefix']; ?>" />
			<label for="<?php echo $this->get_field_id( 'suffix' ); ?>"><?php _e('Suffix:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'suffix' ); ?>" name="<?php echo $this->get_field_name( 'suffix' ); ?>" value="<?php echo $instance['suffix']; ?>" />
			
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e('Limit:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" value="<?php echo $instance['limit']; ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['excerpt'], true ); ?> id="<?php echo $this->get_field_id( 'excerpt' ); ?>" name="<?php echo $this->get_field_name( 'excerpt' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'excerpt' ); ?>"><?php _e('Show Comment Text', 'theme-junkie'); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'comment_length' ); ?>"><?php _e('Comment Length:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'comment_length' ); ?>" name="<?php echo $this->get_field_name( 'comment_length' ); ?>" value="<?php echo $instance['comment_length']; ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['show_post_title'], true ); ?> id="<?php echo $this->get_field_id( 'show_post_title' ); ?>" name="<?php echo $this->get_field_name( 'show_post_title' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_post_title' ); ?>"><?php _e('Show Post Title', 'theme-junkie'); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['author'], true ); ?> id="<?php echo $this->get_field_id( 'author' ); ?>" name="<?php echo $this->get_field_name( 'author' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'author' ); ?>"><?php _e('Show Author', 'theme-junkie'); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_admin_email' ); ?>"><?php _e('Exclude Admin Email: <br/>(Leave blank if you won\'t exclude admin\'s comments)', 'theme-junkie'); ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id( 'exclude_admin_email' ); ?>" name="<?php echo $this->get_field_name( 'exclude_admin_email' ); ?>" value="<?php echo $instance['exclude_admin_email']; ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['show_count'], true ); ?> id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" /> 
			<label for="<?php echo $this->get_field_id( 'show_count' ); ?>"><?php _e('Show Count', 'theme-junkie'); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['gravatar'], true ); ?> id="<?php echo $this->get_field_id( 'gravatar' ); ?>" name="<?php echo $this->get_field_name( 'gravatar' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'gravatar' ); ?>"><?php _e('Show Gravatar (Ignore Count)', 'theme-junkie'); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'gravatar_size' ); ?>"><?php _e('Gravatar Size:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'gravatar_size' ); ?>" name="<?php echo $this->get_field_name( 'gravatar_size' ); ?>" value="<?php echo $instance['gravatar_size']; ?>" />
			<label for="<?php echo $this->get_field_id( 'gravatar_spacing' ); ?>"><?php _e('Gravatar Spacing:', 'theme-junkie'); ?></label>
			<input size="4" type="text" id="<?php echo $this->get_field_id( 'gravatar_spacing' ); ?>" name="<?php echo $this->get_field_name( 'gravatar_spacing' ); ?>" value="<?php echo $instance['gravatar_spacing']; ?>" />
		</p>
	<?php
	}
}

function tj_recent_comments($args = '') {
		tj_recent_comments_options();
		global $defaults;
		$args = wp_parse_args( $args, $defaults );
	extract($args);

	if($gravatar) {
			echo '<ul class="withgravatar">';
		} else {
		if($show_count) {
			echo '<ul class="asol">';
		} else {
			echo '<ul>';
		}
		}
		global $wpdb;
		$sql = "SELECT * FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND comment_author_email != '$exclude_admin_email' AND post_password = '' ORDER BY comment_date DESC LIMIT $limit";
		$comments = $wpdb->get_results($sql);
		$count = 0;
		foreach ($comments as $comment) {
			if($gravatar) {
				if($gravatar_size) {
					$gravatar_padding = $gravatar_size+$gravatar_spacing;
					echo '<li style="min-height:'.$gravatar_size.'px;height:auto !important;height:'.$gravatar_size.'px;padding-left:'.$gravatar_padding.'px;">';
				} else {
					echo '<li style="padding-left:50px;">';
				}
			} else {
				echo "<li>";
			}
			if($show_count && !$gravatar) {
				$count++;
				echo '<span class="count">'.$count.'</span>';
			}
			if($gravatar) {
				if($gravatar_size) {
					echo '<span class="gravatar">'.get_avatar($comment->comment_author_email,$gravatar_size).'</span>';
				} else {
					echo '<span class="gravatar">'.get_avatar($comment->comment_author_email,40).'</span>';
				}
			}
			if ($show_date == 'Relative') {
				$human_time_diff = human_time_diff(mysql2date('U',$comment->comment_date),mysql2date('U',current_time('timestamp')));
				echo '<span class="date">'.$human_time_diff.' ago</span>';
			} elseif ($show_date == 'Custom') {
				echo '<span class="date">'.mysql2date($date_format,$comment->comment_date).'</span>';
			}
			if($author) {
				echo strip_tags($comment->comment_author);
			}
			if($show_post_title) {
				echo ' '.$prefix.' <a rel="nofollow" href="';
				get_permalink($comment->ID);
				echo "#comment-" . $comment->comment_ID;
				echo "\" title=\"on ";
				echo $comment->post_title;
				echo "\">";
				echo $comment->post_title;
				echo "</a> ";
			}
			if($author) {
				echo ' '.$suffix.' ';
			}
			if($excerpt) {
				if($comment_length) {
					$comment_excerpt = mb_strimwidth(strip_tags(apply_filters('comment_content', $comment->comment_content)), 0, $comment_length, "...");
				} else {
					$comment_excerpt = mb_strimwidth(strip_tags(apply_filters('comment_content', $comment->comment_content)), 0, 80, "...");
				}
				if ($comment_excerpt != '') {
					echo '<span class="excerpt">'.$comment_excerpt.'</span>';
				}
			}
			echo '</li>';
}
		echo '</ul>';
}

?>