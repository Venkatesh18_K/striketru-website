jQuery(document).ready(function ($) {

//Start Change Recent Breadcurm in Single Page Sidebar (21-04-20)

// $('.recent-posts-extended ul li a').click(function(){
//     debugger;
//        $.ajax({
//                       url: js_config.ajax_url,
//                       type: "POST",
//                       data:{
//                         action:'remove_session_in_all'
//                       },
//                       success: function (response) {

//                         $('.single_cat').attr('href','https://www.striketru.com/blog/');
//                          $('.single_cat').text('Blog');
//                 }
//             });
// });

//End Change Recent Breadcurm in Single Page Sidebar (21-04-20)


//get js from footer
$( ".sub-menu" ).each(function( i ) {
    var _thisul = $(this);
    var _thisli = $(_thisul.children('li.menu-item')).length;
    if(_thisli <= 6)
    {
        //100% causes menu like 'Case Studies' to be split into 2 lines !!
        _thisul.css('width','100%');
        //300% causes menu to split into two columns !!
        //_thisul.css('width','300%');
        //_thisul.css('width','200%');
    }
});

//Load More js ==Start
 var canBeLoaded = true;
    var btm_height1 = $('.all-tags').outerHeight();
    var btm_height2 = $('.like_our_content_sec').outerHeight();
    var btm_height3 = $('#fwrap').outerHeight();
    var btm_height4 = $('.product_cat_main .blog_category_list').outerHeight();
    var bottomOffset = btm_height1 + btm_height2 + btm_height3 + btm_height4;

    $(window).scroll(function () {
        var data = {
            'action': 'loadmore',
            'tax': $('#tax').val(),
            'search': $('#search_from_tax').val(),
            'page': misha_loadmore_params.current_page
        };
        if ($(document).scrollTop() > ($(document).height() - bottomOffset) && canBeLoaded == true) {
           
            $.ajax({
                url: misha_loadmore_params.ajaxurl,
                data: data,
                type: 'POST',
                beforeSend: function (xhr) {
                    // you can also add your own preloader here
                    // you see, the AJAX call is in process, we shouldn't run it again until complete
                    canBeLoaded = false;
                },
                success: function (data) {
                    if (data) {
                        $('#post_content').append(data); // where to insert posts
                        canBeLoaded = true; // the ajax is completed, now we can run it again
                        misha_loadmore_params.current_page++;
                    }
                }
            });
        }
    });
//Load More js ==End

    //$('.cat-item-1 a').attr('href','https://www.striketru.com/blog');
    $( "#footer_newsletter" ).submit(function( event ) {
// debugger;
        var
                $form = $(this),
                $inputs = $form.find("input, select, button, textarea"),
                serializedData = $form.serialize();

      $.ajax({
            url: js_config.ajax_url,
            type: 'post',
            data: {
                action: 'ajax_subscribe_footer',
                nonce: js_config.ajax_nonce,
                ne: $form.find('.newsletter-email').val(), //THIS IS IMPORTANT TO SUBMIT!! ITS REQUIRED BY THE subscribe() METHOD
                data: serializedData
            },

            success: function (response) {
//we have an answer. it will be placed right after our form
                var text = $('<p class="newsletter-' + response.status + '">' + response.msg + '</p>').hide();
                $('.footer_sucess_msg').html(response.msg).slideDown();

                if (response.status == 'success') {
                    setTimeout(function () {
                        $('#footer_newsletter').trigger("reset");
                        $('.footer_sucess_msg').html('');
                    }, 4000);
                }
                //$form.after( text ).next().slideDown();
            },
        });
       return false;
    });
 });

