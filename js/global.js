jQuery.fn.ImageAutoSize = function (width) {
    $("img", this).each(function () {
        var image = $(this);
        if (image.width() > width) {
            image.width(width);
            image.height((width / image.width()) * image.height());
        }
    });
};
jQuery.noConflict();

		$(window).resize(function(){
			if( $(window).width() > 1000){
			$(".parent-menu:first-child").each(function () {
				if ($(this).children("ul").length > 0) {
					$(this).addClass("withul");
					$(this).children("a").addClass("withula");
					$(this).hover(
						function () {
							$(this).addClass("withul-hover");
							$(this).children("ul").css('display','flex');
							$(this).children("ul").show();
							$(this).css('background','#f29d22');
							$(this).find(".withula").css('color','#fff');
						},
						function () {
							$(this).removeClass("withul-hover");
							$(this).children("ul").hide();
							$(this).css('background','none');
							$(this).find(".withula").css('color','#000');
						}
					);
				}
			});
		$(".parent-menu:not(:first-child)").each(function () {
			if ($(this).children("ul").length > 0) {
				$(this).addClass("withul");
				$(this).children("a").addClass("withula");
				$(this).hover(
					function () {
						$(this).addClass("withul-hover");
						$(this).children("ul").css('width','fit-content');
						$(this).children("ul").show();
						$(this).css('background','#f29d22');
						$(this).find(".withula").css('color','#fff');
					},
					function () {
						$(this).removeClass("withul-hover");
						$(this).children("ul").hide();
						$(this).css('background','none');
						$(this).find(".withula").css('color','#000');
					}
				);
			}
		}); 
	
		}
		});
$(document).ready(function ($) {
		if( $(window).width() > 1000){
			$(".parent-menu:first-child").each(function () {
				if ($(this).children("ul").length > 0) {
					$(this).addClass("withul");
					$(this).children("a").addClass("withula");
					$(this).hover(
						function () {
							$(this).addClass("withul-hover");
							$(this).children("ul").css('display','flex');
							$(this).children("ul").show();
							$(this).css('background','#f29d22');
							$(this).find(".withula").css('color','#fff');
						},
						function () {
							$(this).removeClass("withul-hover");
							$(this).children("ul").hide();
							$(this).css('background','none');
							$(this).find(".withula").css('color','#000');
						}
					);
				}
			});
		$(".parent-menu:not(:first-child)").each(function () {
			if ($(this).children("ul").length > 0) {
				$(this).addClass("withul");
				$(this).children("a").addClass("withula");
				$(this).hover(
					function () {
						$(this).addClass("withul-hover");
						$(this).children("ul").css('width','fit-content');
						$(this).children("ul").show();
						$(this).css('background','#f29d22');
						$(this).find(".withula").css('color','#fff');
					},
					function () {
						$(this).removeClass("withul-hover");
						$(this).children("ul").hide();
						$(this).css('background','none');
						$(this).find(".withula").css('color','#000');
					}
				);
			}
		}); 
	
		}
    $("#category-select").each(function () {
        $(this).hover(
            function () {
                $("#category-dropdown:hidden").slideDown(200);
            },
            function () {
                $("#category-dropdown:visible").slideUp(100);
            }
        );
    });
    $("#category-select-toggle").click(function () {
        return false;
    });
    $("#category-select ul li a").wrapInner("<span>");
    $(".widget ul ul li:last-child").css({ "border-bottom": 0, "padding-bottom": 0 });
    $(".plan-picker").click(function (t) {
        $(".plan-picker").removeClass("active");
        $(t.target).addClass("active");
        $(".price-container .three-years").toggleClass("hidden");
        $(".price-container .one-year").toggleClass("hidden");
        $(".discount-ribbon").toggleClass("hidden");
        $(".striketru-price").toggleClass("hidden");
        $(".lmtd-offer").toggleClass("hidden");
    });
    var acc = $(".accordion");
    for (var i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            $(this).toggleClass("active");
            $(this).children(".expand-icon").toggleClass("collapse-icon");
            $(this).next().toggleClass("show");
        };
    }
});
