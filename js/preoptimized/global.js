jQuery.fn.ImageAutoSize = function(width) {
    $("img",this).each(function()
    {
        var image = $(this);
        if(image.width()>width)
        {
            image.width(width);
            image.height(width/image.width()*image.height());
        }
    });
}
jQuery.noConflict();
jQuery(document).ready(function($){
	/* Topnav Dropdown menu.	*/
	$('#topnav ul li').each(function(){
		if($(this).children('ul').length > 0) {
			$(this).addClass('withul');
			$(this).children('a').addClass('withula');
		
			$(this).hover(function(){
				$(this).addClass('withul-hover');
				$(this).children('ul').slideDown(200);
			},function(){
				$(this).removeClass('withul-hover');
				$(this).children('ul').slideUp(100);	
			});
		}
	});
	
	/* Slide Category Select */
	$('#category-select').each(function(){
		$(this).hover(function(){
			$('#category-dropdown:hidden').slideDown(200);
		},function(){
			$('#category-dropdown:visible').slideUp(100);	
		});
	});

	$('#category-select-toggle').click(function(){
		return false;
	});
	
	$('#category-select ul li a').wrapInner('<span>');
	
	$('.widget ul ul li:last-child').css({'border-bottom':0,'padding-bottom':0});

	$(".plan-picker").click(function(t) {
		$(".plan-picker").removeClass("active");
		$(t.target).addClass("active");
	 	$(".price-container .three-years").toggleClass("hidden"); 
	 	$(".price-container .one-year").toggleClass("hidden"); 
	 	$(".discount-ribbon").toggleClass("hidden");
	 	$(".striketru-price").toggleClass("hidden");
	 	$(".lmtd-offer").toggleClass("hidden");
	});

	var acc = $(".accordion");

	for (var i = 0; i < acc.length; i++) {
	    acc[i].onclick = function(){
	        $(this).toggleClass("active");
	        $(this).children(".expand-icon").toggleClass("collapse-icon");
	        $(this).next().toggleClass("show");
	    }
	}
	
	$(".testimonials-inner").masonry({
	  itemSelector: ".testimonal"//,
	  //columnWidth: 353,
	  //rowHeight: "auto"
	});

});