<?php get_header(); ?>		
<div id="content">	
	<div class="inner-content">			
		<?php the_post(); ?>		
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>						
		<?php if(get_theme_mod('display_single_folio_comments') == 'Yes') { ?>				
		<h1 class="entry-title"><?php the_title(); ?></h1>				
		<span class="meta-comments"><?php comments_popup_link('0','1','%'); ?></span>			
		<?php } else { ?>				
		<h1 class="entry-title full-entry-title"><?php the_title(); ?></h1>	
		<?php } ?>						
		<div class="entry-meta">				
		<span class="meta-author"><?php _e('Posted by','themejunkie'); ?> <?php the_author_posts_link(); ?></span>				
		<span class="meta-date"><?php _e( 'on ', 'themejunkie' ); ?><?php the_time(get_option('date_format')); ?></span>				
		<span class="meta-cat"><?php _e( 'in ', 'themejunkie' ); ?><?php the_category( ', ' ); ?></span>				
		<?php edit_post_link( __( 'Edit', 'themejunkie' ), '<span class="meta-edit">(', '</span>)' ); ?>			
		</div> <!--end .entry-meta-->			
		<div class="entry entry-content">				
		<?php global $post;					if(get_theme_mod('display_single_folio_thumbnail') == 'Yes' && get_post_meta($post->ID,get_theme_mod('thumb_key'),true )) { ?>					
		<?php tj_thumbnail(608,get_theme_mod('single_folio_thumb_height')); ?>				
		<?php } ?>				
		<?php the_content(); ?>				
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'themejunkie' ), 'after' => '</div>' ) ); ?>			
		</div> <!--end .entry-->

		<!-- begin customizaton -->
		<!-- display form to download case study -->
		<?php 
			if (get_the_ID() == 413 || get_the_ID() == 690 || get_the_ID() == 2045) {
				echo '<br>';
				echo '<h1 class="entry-title">Download full case study</h1>';
				echo tj_download_casestudy_form(); 
			}
		?>
		<!-- end customizaton -->
	</div> <!--end .inner-content-->		
</div>				
<?php if(get_theme_mod('display_single_folio_comments') == 'Yes') { comments_template( '', true ); } ?>			
</div><!--end #content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
