<?php 
// Start Set Breadcrumb Recent Posts (21-04-20)
session_start();
 $url = $_SERVER['HTTP_REFERER'];
                    $post_id= url_to_postid( $url );
                   if($post_id != 0){
                    if(get_post_type( $post_id ) == 'post'){           
                        session_unset();
                    }
                }
// End Set Breadcrumb Recent Posts (21-04-20)
get_header();
 ?>		
<div id="content" class="single_blog_main">	
    <div class="top_row">	
    <div class="container_custom">	
        <div class="row">
            <?php
            the_post();
            $post_date = get_the_date('j/m/Y');
            $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-detail');
            ?>
            <div class="col_8">
                <div class="col_main_content">
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="top_meta_main">
                            <div class="col_left">
                                <span class="meta-date"><?php echo $post_date; ?></span> |
                                <span class="meta-author">by <?php the_author(); ?></span> 
                            </div>
                            <div class="social_icon_style"><?php echo do_shortcode('[TheChamp-Sharing]'); ?></div>
                        </div>
                        <div class="large_img">
                            <img src="<?php echo $large_image_url[0]; ?>" alt="">
                        </div>
                        <div class="entry-tags-blog">
                        <?php //printf(the_tags('<div class="entry-tags-blog">', ' ', '</div>')); 

                        if (is_array(get_the_tags())) {
                           $tags = get_the_tags();
                           foreach($tags as $tag) {
                              echo '<li>'.$tag->name.'</li>';
                           } 
                        } 
                        ?></div>
                        <div class="all_content">
                            <?php the_content(); ?>
                        </div>
                    </div>	
                </div>	
            </div>	
            <div class="col_4">
                <div class="sidebar_main">
                    <div class="blog_detail_sidebar">
                        <?php dynamic_sidebar('blog-detail-sidebar'); ?>
                    </div>
                    <?php comments_template('', true); ?>	
                </div>
            </div>
        </div>
    </div> 
    </div> 
    <!--end .inner-content -->	

        <div class="like_our_content_sec">
        <div class="container_custom">	
            <div class="row">
                <div class="col_12">	
                    <div id="mc_embed_signup_scroll">
                        <h1>Like Our Content?</h1>
                        <div class="row">
                            <div class="col_8 col_left">

<!-- Start Edit Newsletter Form (14-04-20)  -->

             <div  id="mc_embed_signup">
                <form action="https://striketru.us20.list-manage.com/subscribe/post?u=fbc67bec9b446f88b4048fa32&amp;id=92fa2120e8" method="post" name="mc-embedded-subscribe-form" class="validate footer_newsletter" target="_blank" novalidate>

                                <div id="mc_embed_signup_scroll" class="footer_newsletter_scroll">
                    <div class="mc-field-group1">
                        <input type="email" value="" name="EMAIL" class="required email search_input" placeholder="Email Address" id="mce-EMAIL">
                    </div>
                                <div class="field-button"><input type="submit" value="SUBSCRIBE" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9fed41bdf95025e316d39574f_41191afea4" tabindex="-1" value=""></div>
                        
                        </div>
                    </form>
                    </div>
                      <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
        <!-- End Edit Newsletter Form (14-04-20)  -->
                                <?php // echo do_shortcode('[newsletter_form form="1"]'); ?>
                            </div>
                            <div class="col_4">
                                <span class="or_txt">OR</span>
                                <a href="/contact-us/" class="button">Contact Us</a>
                                <span class="more_detail">For More Details</span>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div><!--end #content -->
<!--?php get_sidebar(); ?-->



<?php 
get_footer(); ?>