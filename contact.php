<?php get_header(); ?>
	
	<div class="inner-content">
		<div class="onecol clear" id="contact-page">
			<?php the_post(); ?>
			
			<div class="contact-right">
				<div class="contact-form">
					<h3 class="contact-section-title"><?php echo get_theme_mod('contact_form_title'); ?></h3>
					
					<div class="contact-section-inner">
					
					<?php if(get_theme_mod('before_contact_form')) { ?>
					<div class="contact-form-before">
						<?php echo stripslashes(wpautop(get_theme_mod('before_contact_form'))); ?>
					</div><!-- end .contact-form-before -->
					<?php } ?>
							
					<?php echo tj_contact_form(); ?>
			
					</div>
				
				</div><!-- end .contact-form -->
			</div>

			<div class="contact-address-wrapper">
				<h3 class="contact-section-title"><?php echo get_theme_mod('contact_info_title'); ?></h3>
				<div class="contact-container">
					<?php
						$address = get_theme_mod('address');
						$phone = get_theme_mod('phone');
						$fax = get_theme_mod('fax');
						$email = get_theme_mod('email');
					?>
					<div class="contact-hustn">
						<div class="address-block">
							<h3 class="section-title">StrikeTru Texas</h3>
							<?php if($address) { ?><div><?php echo wpautop($address); ?></div><?php } ?>
							<?php if($fax) { ?><div>Fax:</div><div><?php echo $fax; ?></div><?php } ?>
							
							<br>
							<h3 class="section-title">StrikeTru New York</h3>
							<div>31-00 47th Avenue, Suite# 3100<br>Long Island City, NY 11101</div> 
						</div>
					</div>
					<div class="contact-suprt">
						<div class="address-block">
							<?php if($email) { ?><div>Support:</div><div><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div><?php } ?>
							<br/><?php if($phone) { ?><div>Phone:</div><div><?php echo $phone; ?></div><?php } ?>
							<!-- <br/><div>StrikeTru Hyderabad:</div><div>+91-9000376345</div> -->
						</div>
					</div>
					<div class="contact-hyd">
						<div class="address-block">
							<h3 class="section-title">StrikeTru India</h3>
							<!--
							<div>Address:</div>
							<p>3rd Floor, A-54, Journalists Colony<br>
								Jubilee Hills, Hyderabad - 500033</p>	
							-->
							<p>
							A-54, Journalists Colony<br>
							Jubilee Hills, Hyderabad<br>
							Telangana – 500033<br>
							INDIA
							</p>
						</div>
					</div>

					<?php if(get_theme_mod('after_contact_info')) { ?>
						<div class="contact-info-after">
							<?php echo stripslashes(wpautop(get_theme_mod('after_contact_info'))); ?>
						</div>
					<?php } ?>
				</div>

				<div class="contact-social">
					<h3 class="contact-section-title"><?php echo get_theme_mod('social_links_title'); ?></h3>
					<div class="contact-section-inner">
						<?php 
								$twitter_url = get_theme_mod('twitter_url');
								$facebook_url = get_theme_mod('facebook_url');
								$linkedin_url = get_theme_mod('linkedin_url');
								$flickr_url = get_theme_mod('flickr_url');
								$rss_url = get_theme_mod('rss_url');
							?>
							
							<ul class="social-list">
								<?php if($rss_url) { ?><li class="foot-rss"><a target=_blank href="<?php echo $rss_url; ?>">RSS Feed</a></li><?php } ?>
								<?php if($flickr_url) { ?><li class="foot-flickr"><a target=_blank href="<?php echo $flickr_url; ?>">Flickr</a></li><?php } ?>
								<?php if($linkedin_url) { ?><li class="foot-linkedin"><a target=_blank href="<?php echo $linkedin_url; ?>">Linkedin</a></li><?php } ?>
								<?php if($facebook_url) { ?><li class="foot-facebook"><a target=_blank href="<?php echo $facebook_url; ?>">Facebook</a></li><?php } ?>
								<?php if($twitter_url) { ?><li class="foot-twitter"><a target=_blank href="<?php echo $twitter_url; ?>">Twitter</a></li><?php } ?>
							</ul>
					</div><!-- end .contact-section-inner -->
				</div><!-- end .contact-social -->
			</div>
			<!-- <div class="contact-left">
				<div class="contact-info">
					<h3 class="contact-section-title"><?php echo get_theme_mod('contact_info_title'); ?></h3>
						
					<div class="contact-section-inner">
							<?php
								$address = get_theme_mod('address');
								$phone = get_theme_mod('phone');
								$fax = get_theme_mod('fax');
								$email = get_theme_mod('email');
							?>
							
							<dl class="contact-list">
								 begin .gundoju 
								<h3 class="section-title">StrikeTru Texas</h3>
								end .gundoju 
								<?php if($address) { ?><dt>Address:</dt><dd><?php echo wpautop($address); ?></dd><?php } ?>
								<?php if($phone) { ?><dt>Phone:</dt><dd><?php echo $phone; ?></dd><?php } ?>
								<?php if($fax) { ?><dt>Fax:</dt><dd><?php echo $fax; ?></dd><?php } ?>
								<?php if($email) { ?><dt>Email:</dt><dd><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></dd><?php } ?>
								begin .gundoju 
								<p><br></p>
								<h3 class="section-title">StrikeTru Ohio</h3>
								<dt>Address:</dt><dd><p>7106 Corporate Way<br>
											Dayton OH 45459</p></dd>									
								<dt>Phone:</dt><dd>+1 937 312 1204 Ext 201</dd>										
								end .gundoju 
								<p><br></p>
								<h3 class="section-title">StrikeTru India</h3>
								<dt>Address:</dt><dd><p>3rd Floor, RK Towers, Plot # 8, Road # 3<br>
											Banjara Hills, Hyderabad - 500034</p></dd>									
								<dt>Phone:</dt><dd>+91-9701225477</dd>										

							</dl>
							
							<?php if(get_theme_mod('after_contact_info')) { ?>
								<div class="contact-info-after">
									<?php echo stripslashes(wpautop(get_theme_mod('after_contact_info'))); ?>
								</div>
							<?php } ?>
				
					</div>
				</div> -->
			</div>
		
			<!--
			<div id="download_form">
				<h3>Download full version</h3>
				<div class="form-wrapper">
					<div class="form-row">
						<label class="form-lbl">Name</label>
						<input type="text" class="form-text" />
					</div>
					<div class="form-row">
						<label class="form-lbl">Company</label>
						<input type="text" class="form-text" />
					</div>
					<div class="form-row">
						<label class="form-lbl">Email</label>
						<input type="text" class="form-text" />
					</div>
					<div class="form-row">
						<label class="form-lbl">Phone</label>
						<input type="text" class="form-text" />
					</div>
					<input id="donwload-form-submit" class="button" type="submit" name="submit" value="Submit" />
				</div>
			</div>
			-->
				
			
		</div><!-- end .onecol -->
	</div><!-- end .inner-content-->

<?php get_footer(); ?>
