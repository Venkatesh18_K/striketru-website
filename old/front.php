<?php get_header(); ?>

	<div id="content">
		<div class="patner-logos-row"> 
			<div class="inner-content">
				<div class="partners-wrapper">
					<div class="partners-hdr">
						<h4><span>Some of our customers</span></h4>
					</div>
					<div class="partner-blocks-wrap">
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/marketamerica_shopcom.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/02/fossil-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/11/City-Furniture-Logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/stealasofa.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/02/mor-funiture-for-less-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/richmond-industrial-supply.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/02/Abatix_big_logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2019/02/nationwide-industrial-supply-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/petra-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/Five_ten_logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2019/03/pierce-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/PTS-America-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/02/sony_pictures_logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/11/astral-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2018/11/speedy-pros-logo.png" alt="" width="170" /></div>


						<!--  Deprecated -->
						<!-- 
						<div class="partner-block"><img src="/wp-content/uploads/2016/10/Cyberswim-Logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/riversand-logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/city_furniture_logo.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/triumvir.png" alt="" width="170" /></div>
						<div class="partner-block"><img src="/wp-content/uploads/2016/09/innovit.png" alt="" width="170" /></div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/marketamerica_shopcom.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2018/02/fossil-logo.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2018/02/city_furniture_logo.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/stealasofa.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/richmond-industrial-supply.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2018/02/Abatix_big_logo.png"/> 
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/rexel.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/Five_ten_logo.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/Samsung-Lettermark-Blue-300x46.png"/>
						</div>
						<div class="partner-block">
							<img width="170" src="/wp-content/uploads/2016/09/amer_sports.png"/>
							<img width="170" src="/wp-content/uploads/2018/02/mor-funiture-for-less-logo.png"/>
							<img width="170" src="/wp-content/uploads/2018/02/sony_pictures_logo.png"/>
							<img width="170" src="/wp-content/uploads/2016/09/invacare.png"/>
							<img width="170" src="/wp-content/uploads/2016/09/Auchan.png"/>
							<img width="170" src="/wp-content/uploads/2016/09/ePetworld.png"/>
							<img width="170" src="/wp-content/uploads/2016/09/lancaster.png"/>
						</div>
						-->
					</div>
				</div>
			</div>
		</div> <!-- end #container .partner-logos-row -->
		<div id="home-about">
			<!-- <h3 class="section-title"><?php echo get_theme_mod('home_about_title'); ?></h3>
			<div class="home-about-entry">
				<?php echo stripslashes(wpautop(get_theme_mod('home_about_info'))); ?>
			</div> -->
			<div class="inner-content">
				<h3 class="section-title">FEATURED SOLUTION</h3>
				<h2 class="section-subtitle">Akeneo, A Premier PIM Solution</h2>
				<!-- <h2 class="section-subtitle">Give customers easy access to great product content</h2> -->
				<div class="home-about-entry">
					<p>With Akeneo, it’s easy to deliver rich product content across your sales channels, digital marketing
					platforms, and devices. We can also help you centralize product data and asset management, improve 
					data quality, automate content production and dissemination, and boost online visibility and conversion.
					</p>
				</div>
				<div class="video-wrapper">
					<div class="video-block">
						<iframe width="560" height="349" src="https://www.youtube.com/embed/fmHAz9pSS38?list=PLjizFBk9AiOMQIIL9vfreJiRw-Zfo7NvX" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div><!-- end #home-about -->
			
		<div id="home-folio">
			<div class="inner-content">
				<div class="figure-text-block">
					<div class="feat-container">
						<figure class="figure-col">
							<img width="100%" src="/wp-content/uploads/2016/09/content-is-king.jpg" alt="Rich Content">
						</figure>
						<div class="text-col">
							<div class="markdown-field">
								<h5 class="title">RICH CONTENT</h5>
								<h2 class="link-title" href="" title="">Product content is king</h2>
								<p class="feat-desc">Consumers rely heavily on online product content to make online and store purchases. Poor content lowers brand equity, search engine rankings, and sales, and raises operating costs. Legacy content operations involve high cost and complexity. Akeneo makes it very easy to centrally create, manage and disseminate high quality product content on a consistent basis.</p>
								<!-- <p><a href="" class="button learn_more" title="">Learn more</a></p> -->
							</div>
						</div>
					</div>
				</div>
				<div class="text-figure-block">
					<div class="feat-container">
						<figure class="figure-col">
							<img width="100%" src="/wp-content/uploads/2016/09/digital-asset-management.png" alt="Digital assets">
						</figure>
						<div class="text-col">
							<div class="markdown-field">
								<h5 class="title">DIGITAL ASSETS</h5>
								<h2 class="link-title" href="" title="">Digital assets for compelling product stories</h2>
								<p class="feat-desc">Polish your products by associating relevant imagery, videos, PDFs, and other digital assets. Use Akeneo to centrally organize, maintain, protect, reuse, transform, and share your assets. Ensure they’re in place prior to launching products, and meet the ever increasing demands of your digital channels.</p>
								<!-- <p><a href="" class="button learn_more" title="">Learn more</a></p> -->
							</div>
						</div>
					</div>
				</div>
				<div class="figure-text-block">
					<div class="feat-container">
						<figure class="figure-col">
							<img width="100%" src="/wp-content/uploads/2017/01/content-automation-1.jpg" alt="content automation">
						</figure>
						<div class="text-col">
							<div class="markdown-field">
								<h5 class="title">AUTOMATION</h5>
								<h2 class="link-title" href="" title="">Automate content production and delivery</h2>
								<p class="feat-desc">Customers rely a lot on titles, descriptions, and images to make buying decisions. Automate systematic concatenation of rich data to create consistent titles and descriptions, and extraction and delivery of channel/retailer specific content feeds.</p>
								<!-- <p><a href="" class="button learn_more" title="">Learn more</a></p> -->
							</div>
						</div>
					</div>
				</div>
				<div class="text-figure-block">
					<div class="feat-container">
						<figure class="figure-col">
							<img width="100%" src="/wp-content/uploads/2017/01/ecommerce-1.jpg" alt="e-commerce">
						</figure>
						<div class="text-col">
							<div class="markdown-field">
								<h5 class="title">E-COMMERCE</h5>
								<h2 class="link-title" href="" title="">Content for commerce</h2>
								<p class="feat-desc">Rich product content, typically managed in a PIM solution like Akeneo, enables navigation and exploration features found on popular E-Commerce platforms. These features include intuitive categories and sub-categories, helpful product names, facet navigation, cross-sell compatible products, comparison, etc.</p>
								<!-- <p><a href="" class="button learn_more" title="">Learn more</a></p> -->
							</div>
						</div>
					</div>
				</div>
				<div class="figure-text-block">
					<div class="feat-container">
						<figure class="figure-col">
							<img width="100%" src="/wp-content/uploads/2017/01/digital-marketing-1.jpg" alt="digital marketing">
						</figure>
						<div class="text-col">
							<div class="markdown-field">
								<h5 class="title">DIGITAL MARKETING</h5>
								<h2 class="link-title" href="" title="">Greater online visibility and conversion</h2>
								<p class="feat-desc">Great product content on search, social media, and retailer websites will ensure customers find and purchase your products. Feed eCommerce websites and digital marketing platforms with rich content consistently to make them work aggressively for you, and drive higher traffic and conversion.</p>
								<!-- <p><a href="" class="button learn_more" title="">Learn more</a></p> -->
							</div>
						</div>
					</div>
				</div>
			</div><!-- end #home-folio .inner-content -->
		</div><!-- end #home-folio -->
		
		<div id="product-features">
			<div class="inner-content">
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Good Quality-50-rich-content.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">Rich Content</h3>
						<p class="pfeat-desc">Create and manage rich product content in a PIM to drive SEO, site search, and e-commerce conversion.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Approval-50-reliable.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">RELIABLE CONTENT</h3>
						<p class="pfeat-desc">Aggregate, enrich, maintain, and govern product content. Create a trusted source of current, complete, and accurate content.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/JPG-50-digitalassets.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">DIGITAL ASSETS</h3>
						<p class="pfeat-desc">Polish your products by linking digital assets (imagery, videos, PDFs, etc.) managed more effectively in PIM.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Accept Database-50-data-quality.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">DATA QUALITY</h3>
						<p class="pfeat-desc">Create quality content with completeness checks, data validation and enrichment rules, approval workflows, and granular access controls.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Automatic-50-automation.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">AUTOMATION</h3>
						<p class="pfeat-desc">Auto generate consistent titles, descriptions, and URLs, and automate channel and retailer specific content feeds.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Multiple Devices-50-digital-marketing.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">DIGITAL MARKETING</h3>
						<p class="pfeat-desc">Feed websites, marketplaces, and other digital marketing platforms with content tailored for individual channel needs.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Shopping Cart Loaded-64-ecommerce.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">E-COMMERCE</h3>
						<p class="pfeat-desc">Enable e-commerce site navigation and exploration features from rich, granular data best managed in a PIM.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Disconnected-64-magento-sync.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">MAGENTO SYNC</h3>
						<p class="pfeat-desc">Synchronize your catalog from Akeneo to Magento easily using PimGento, a free Magento extension.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Hub-50-integrations.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">INTEGRATIONS</h3>
						<p class="pfeat-desc">Easily integrate PIM with other systems for a single source of truth, one time data entry, and automated publishing.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Factory-50-supplier.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">SUPPLIER PORTAL</h3>
						<p class="pfeat-desc">Reduce SKU setup costs and time to market by having suppliers onboard and update product content directly in PIM.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Downloads-64-asset-portal.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">ASSET PORTAL</h3>
						<p class="pfeat-desc">Provide distributors and customers self-service access to digital assets published for marketing or support purposes.</p>
					</div>
				</div>
				<div class="pfeat-item">
					<div class="feature-icon">
						<img src="../wp-content/themes/cubelight/images/assets/whyakeneo/Open Source-50.png" alt="" width="60" height="60" />
					</div>
					<div class="pfeat-details">
						<h3 class="title">OPEN SOURCE</h3>
						<p class="pfeat-desc">Get visibility and access to source code, engage your own resources to support and enhance your PIM solution.</p>
					</div>
				</div>
			</div><!-- end #product-features .inner-content-->
		</div><!-- end #product-features -->


		<!--  Deprecated -->
		<!-- 
		<div id="home-pricing-wrapper">
			<div class="inner-content">
				<div class="title-wrap">
				 	<h2>14-day risk free trial</h2>
  					<p>3 year pricing package below </p>
				</div>
				<div class="pricing-table-wrap">
					<div class="pricing-col">
						<header class="pricing-hdr-block">
							<div class="discount-ribbon">
								<span>20% OFF</span>
							</div>
							<h3 class="tbl-header">Starter</h3>
							<div class="price-container">
								<span class="currency">$</span>
								<span class="amount three-years">1000</span><span class="amount one-year hidden">1250</span>
								<span class="per-duration">/mo</span><br/>
								<span class="per-duration"> + AWS<sup>1</sup></span>
							</div>
							<div class="striketru-price">$1250.00/MO</div>
							<p class="lmtd-offer">Limited time offer</p>
						</header>
						<div class="pricing-cell-container">
							<ul class="pricing-cell-list">
								<li class="alignCenter">
									<a href="/akeneo-trial/" class="button gray">Start Free Trial</a>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Akeneo Community Edition</p></li>
									</ul>
								</li>
								<li class="block-one">
									<span>Hosted<sup>2</sup></span><br/>
									<span>Setup & Support</span>
								</li>
								<li class="offers-list">
									<ul class="pricing-cell-inner">
										<li><p><b>1000</b> SKUs</p></li>
										<li><p><b>100</b> Attributes</p></li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Multi-Language<br>&nbsp;</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Multi-Channel<br>&nbsp;</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Multi-User Login<br>&nbsp;</p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Data Management</p></li>
										<li><p>Digital Asset Management<sup>3</sup></p></li>
										<li><p>Imports & Exports</p></li>
										<li><p>Role Based Security</p></li>
										<li><p>Teamwork Assistant</p></li>
										<li><span class="cross-icon"></span>
											<p>Product Variants</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Multiple Catalogs<br>&nbsp;</p>
										</li>

									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Digital Asset Management<br><i>advanced</i><sup>4</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Advanced Security<br><i>by category, attribute ...</i></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Data Quality Rules<sup>5</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Workflow Engine</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Versioning & Rollback</p>
										</li>
									</ul>
								</li>
								<li class="alignCenter">
									<a class="button" href="/pricing/">Learn More</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pricing-col">
						<header class="pricing-hdr-block">
							<div class="discount-ribbon">
								<span>20% OFF</span>
							</div>
							<h3 class="tbl-header">Basic</h3>
							<div class="price-container">
								<span class="currency">$</span>
								<span class="amount three-years">1500</span><span class="amount one-year hidden">1875</span>
								<span class="per-duration">/mo</span><br/>
								<span class="per-duration"> + AWS<sup>1</sup></span>
							</div>
							<div class="striketru-price">$1875.00/MO</div>
							<p class="lmtd-offer">Limited time offer</p>
						</header>
						<div class="pricing-cell-container">
							<ul class="pricing-cell-list">
								<li class="alignCenter">
									<a href="/akeneo-trial/" class="button gray">Start Free Trial</a>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Akeneo Community Edition</p></li>
									</ul>
								</li>
								<li class="block-one">
									<span>Hosted<sup>2</sup></span><br/>
									<span>Setup & Support</span>
								</li>
								<li class="offers-list">
									<ul class="pricing-cell-inner">
										<li><p><b>3000</b> SKUs</p></li>
										<li><p><b>150</b> Attributes</p></li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Multi-Language<br>&nbsp;</p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-Channel<br><i>2 channels</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-User Login<br><i>3 users</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Data Management</p></li>
										<li><p>Digital Asset Management<sup>3</sup></p></li>
										<li><p>Imports & Exports</p></li>
										<li><p>Role Based Security</p></li>
										<li><p>Teamwork Assistant</p></li>
										<li><span class="checkmark-icon"></span>
											<p>Product Variants</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Multiple Catalogs<br>&nbsp;</p>
										</li>

									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Digital Asset Management<br><i>advanced</i><sup>4</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Advanced Security<br><i>by category, attribute ...</i></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Data Quality Rules<sup>5</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Workflow Engine</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Versioning & Rollback</p>
										</li>
									</ul>
								</li>
								<li class="alignCenter">
									<a class="button" href="/pricing/">Learn More</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pricing-col">
						<header class="pricing-hdr-block">
							<div class="discount-ribbon">
								<span>20% OFF</span>
							</div>
							<h3 class="tbl-header">Professional</h3>
							<div class="price-container">
								<span class="currency">$</span>
								<span class="amount three-years">2000</span><span class="amount one-year hidden">2500</span>
								<span class="per-duration">/mo</span><br/>
								<span class="per-duration"> + AWS<sup>1</sup></span>
							</div>
							<div class="striketru-price">$2500.00/MO</div>
							<p class="lmtd-offer">Limited time offer</p>
						</header>
						<div class="pricing-cell-container">
							<ul class="pricing-cell-list">
								<li class="alignCenter">
									<a href="/akeneo-trial/" class="button gray">Start Free Trial</a>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Akeneo Community Edition</p></li>
									</ul>
								</li>
								<li class="block-one">
									<span>Hosted<sup>2</sup></span><br/>
									<span>Setup & Support</span>
								</li>
								<li class="offers-list">
									<ul class="pricing-cell-inner">
										<li><p><b>5000</b> SKUs</p></li>
										<li><p><b>200</b> Attributes</p></li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="checkmark-icon"></span>
											<p>Multi-Language<br><i>3 languages</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-Channel<br><i>3 channels</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-User Login<br><i>5 users</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Data Management</p></li>
										<li><p>Digital Asset Management<sup>3</sup></p></li>
										<li><p>Imports & Exports</p></li>
										<li><p>Role Based Security</p></li>
										<li><p>Teamwork Assistant</p></li>
										<li><span class="checkmark-icon"></span>
											<p>Product Variants</p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multiple Catalogs<br><i>3 catalogs</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Digital Asset Management<br><i>advanced</i><sup>4</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Advanced Security<br><i>by category, attribute ...</i></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Data Quality Rules<sup>5</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Workflow Engine</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Versioning & Rollback</p>
										</li>
									</ul>
								</li>
								<li class="alignCenter">
									<a class="button" href="/pricing/">Learn More</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pricing-col popular">
						<header class="pricing-hdr-block">
							<div class="discount-ribbon">
								<span>20% OFF</span>
							</div>
							<h3 class="tbl-header">Corporate</h3>
							<p class="pricebox-pretitle">Starting at</p>
							<div class="price-container">
								<span class="currency">$</span>
								<span class="amount three-years">2500</span><span class="amount one-year hidden">3125</span>
								<span class="per-duration">/mo</span><br/>
								<span class="per-duration"> + AWS<sup>1</sup></span>
							</div>
							<div class="striketru-price">$3125.00/MO</div>
							<p class="lmtd-offer">Limited time offer</p>
						</header>
						<div class="pricing-cell-container">
							<ul class="pricing-cell-list">
								<li class="alignCenter">
									<a href="/akeneo-trial/" class="button">Start Free Trial</a>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Akeneo Community Edition</p></li>
									</ul>
								</li>
								<li class="block-one">
									<span>Hosted<sup>2</sup></span><br/>
									<span>Setup & Support</span>
								</li>
								<li class="offers-list">
									<ul class="pricing-cell-inner">
										<li><p><b>10000</b> SKUs</p></li>
										<li><p><b>250</b> Attributes</p></li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="checkmark-icon"></span>
											<p>Multi-Language<br><i>3 languages</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-Channel<br><i>3 channels</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-User Login<br><i>5 users</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Data Management</p></li>
										<li><p>Digital Asset Management<sup>3</sup></p></li>
										<li><p>Imports & Exports</p></li>
										<li><p>Role Based Security</p></li>
										<li><p>Teamwork Assistant</p></li>
										<li><span class="checkmark-icon"></span>
											<p>Product Variants</p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multiple Catalogs<br><i>5 catalogs</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="cross-icon"></span>
											<p>Digital Asset Management<br><i>advanced</i><sup>4</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Advanced Security<br><i>by category, attribute ...</i></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Data Quality Rules<sup>5</sup></p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Workflow Engine</p>
										</li>
										<li><span class="cross-icon"></span>
											<p>Versioning & Rollback</p>
										</li>
									</ul>
								</li>
								<li class="alignCenter">
									<a class="button" href="/pricing/">Learn More</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="pricing-col">
						<header class="pricing-hdr-block">
							<div class="discount-ribbon">
								<span>20% OFF</span>
							</div>
							<h3 class="tbl-header">Enterprise</h3>
							<div class="price-container">
								<span class="add-on">Call for Pricing<sup>6</sup></span>
							</div>
						</header>
						<div class="pricing-cell-container">
							<ul class="pricing-cell-list">
								<li class="alignCenter">
									<a href="/contact-us" class="button gray">Contact Sales</a>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Akeneo Enterprise Edition</p></li>
									</ul>
								</li>
								<li class="block-one">
									<span>Hosted<sup>&nbsp;</sup> or On-Premise</span><br/>
									<span>Setup & Support</span>
								</li>
								<li class="offers-list">
									<ul class="pricing-cell-inner">
										<li><p>Unlimited</p></li>
										<li><p>Unlimited</p></li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="checkmark-icon"></span>
											<p>Multi-Language<br><i>unlimited</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-Channel<br><i>unlimited</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multi-User Login<br><i>unlimited</i></p>
										</li>
									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><p>Data Management</p></li>
										<li><p>Digital Asset Management<sup>3</sup></p></li>
										<li><p>Imports & Exports</p></li>
										<li><p>Role Based Security</p></li>
										<li><p>Teamwork Assistant</p></li>
										<li><span class="checkmark-icon"></span>
											<p>Product Variants</p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Multiple Catalogs<br><i>unlimited</i></p>
										</li>

									</ul>
								</li>
								<li class="included-items-list">
									<ul class="included-items-list-inner">
										<li><span class="checkmark-icon"></span>
											<p>Digital Asset Management<br><i>advanced</i><sup>4</sup></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Advanced Security<br><i>by category, attribute ...</i></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Data Quality Rules<sup>5</sup></p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Workflow Engine</p>
										</li>
										<li><span class="checkmark-icon"></span>
											<p>Versioning & Rollback</p>
										</li>
									</ul>
								</li>
								<li class="alignCenter">
									<a class="button" href="/contact-us/">Contact Sales</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				-->
				<!-- end .pricing-table-wrap -->

				<!-- 
				<div class="footnotes-section">
					<span class="foot-note">1 AWS hosting fees (est. $100-$300/mo). See <a href="https://aws.amazon.com/ec2/pricing/" rel="nofollow">official pricing page</a> for details.</span>
					<span class="foot-note">2 We can host on-premise for an additional fee.</span>
					<span class="foot-note">3 Upload, Import, Associate, Export Asset Files</span>
					<span class="foot-note">4 Asset categories, meta-data, transformations, permissions</span>
					<span class="foot-note">5 Content production & authoring workflows</span>
					<span class="foot-note">6 Enterprise pricing will be determined based on your specific requirements</span>
				</div>
				-->
				<!-- end .footnotes-section  -->
			<!-- 
			</div>
			-->
			<!-- end #home-pricing-wrapper.inner-content-->
		<!-- 
		</div>
		-->
		<!-- end #home-pricing-wrapper-->

		<div id="testimonials-wrap">
			<div class="inner-content">
				<h3>Testimonials</h3>
				<div class="testimonials-inner">
					<a id="testimonal-1" class="testimonal" href="http://www.striketru.com/industrial-distributor-deploys-akeneo/">
						"We have been very impressed with StrikeTru's expert-level understanding of data structure, e-commerce and the online world. Their comprehensive understanding of the “Akeneo” PIM is impressive. At every turn they were able to overcome our challenges and help us better understand our future possibilities with their implementation. In a world where it’s easy to spend tons of money headed down the wrong path, StrikeTru has given us a refreshing sense of confidence that our online strategy is the right one." <br/> - <i><b>Levi W. Hill IV, President of Richmond Supply Company</b></i>
					</a>
					<a id="testimonal-2" class="testimonal" href="http://www.striketru.com/pim-software-deployment-at-a-leading-global-internet-retailer/">
						"The Akeneo PIM system is central to Market America’s ongoing digital drive to deliver rich, accurate, and timely product content to our customers globally. With Akeneo PIM, we can create, collect, and syndicate rich, localized product content to our various eCommerce websites faster than before." <br/> - <i><b>Jon Vivers, VP Search and Analytics of Market America | SHOP.COM</b></i>
					</a>
					<a id="testimonal-3" class="testimonal" href="http://www.striketru.com/about-us/newsroom/press-five-ten-selects-akeneo/">
						“We like Akeneo PIM because is intuitive and user friendly. It helps centralize our product data and has a connector to our Magento eCommerce platform, which means a lot of effort saved when updating our eCommerce sites. We chose to work with StrikeTru because of their obvious mastery of the Akeneo product and their ability to give us a clear vision of how the fully implemented solution will meet our various requirements.” <br/> - <i><b>Chris Moylan, Director of IT and Logistics at Five Ten</b></i>
					</a>
					<a id="testimonal-4" class="testimonal" href="http://www.striketru.com/steal-a-sofa/">
						“Akeneo helped me escape spreadsheet hell so I can focus on dominating my market. Its open source, easy to use, and great for product enrichment and channel exports. I was able to double my assortment without adding overhead!”. <br/>  - <i><b>e-Commerce Manager, StealASofa</b></i>
					</a>
					<a id="testimonal-5" class="testimonal" href="http://www.striketru.com/industrial-distributor-deploys-akeneo/">
						"We've tried several products and processes to manage our Magento e-Commerce website content. Akeneo has been by far the easiest to use, and the most powerful. It's intuitive, fast and loaded with features to organize and utilize data. We've been especially pleased with the expertise StrikeTru has brought to the project. They have constructed the right PIM system for our needs, trained us, given great advice as we expand our offerings and made this project go smoothly." <br/>  - <i><b>Jamie Hope, PIM Analyst & Creative Manager at Richmond Supply Company</b></i>
					</a>
					<a id="testimonal-6" class="testimonal" href="http://www.striketru.com/pts-america/">
				 		“Once you realize the advantage of getting rid of the old practices and find this central place for collaboration, it’s irresistible.” <br/>  - <i><b>Charles Gallagher, Executive Vice President of PTS America</b></i>
					</a>
					<a id="testimonal-7" class="testimonal" href="http://www.striketru.com/pim-software-deployment-at-a-leading-global-internet-retailer/">
				 		“Updating product content in Akeneo PIM takes 10 minutes compared with the 30 minutes it took in the old system.” <br/>  - <i><b>Rebecca Eckenroth, Health & Nutrition Product Manager in Market America</b></i>
					</a>
				</div>
				<!-- <div class="more-testimonals">
					<a class="button " href="/testimonials">MORE</a>	
				</div> -->
			</div>
		</div><!-- end #testimonials-wrap -->

		<div id="featured_work_wrapper">
			<div class="inner-content">
				<h3 class="section-title"><?php echo get_theme_mod('home_folio_title'); ?></h3> 
				<div class="fw-posts">
				 <?php 
					query_posts('showposts='.get_theme_mod('home_folio_num').'&cat='.get_theme_mod('folio_cats'));
					if(have_posts()):
					while(have_posts()): the_post(); 
					$q = $wp_query->current_post;  
					if(is_int(($q+1)/3)) $postclass = 'fwpost last'; else $postclass = 'fwpost';
					?> 
					
				 	<div class="<?php echo $postclass; ?>">
						<div class="thumb-row">
							<?php tj_thumbnail(416,get_theme_mod('folio_thumb_height')); ?>
						</div>
						<div class="text-row">
							<h4 class="title">
								<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h4>
							
							<!-- <p class="excerpt">
								<?php tj_content_limit(140); ?>
							</p> -->

							<!-- <a class="button learn_more" href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>">LEARN MORE</a> -->
						</div>
					</div>  <!-- end .fpost -->
				
				<?php endwhile; endif; ?>
				</div>
			</div> <!-- end #featured_work_wrapper .inner-content -->
		</div>
	</div><!-- end #content -->
		
	<div id="sidebar" style="display:none;">	

			<!-- begin #skufactors -->
			<div id="home-quote">
			
				<a class="more" href="http://www.skufactors.com/">More »</a>				<h3 class="section-title">SkuFactors</h3>
				
				<div id="home-quote-content">
					<br><p>Hosted-based Product Information Management (PIM) system that is flexible, auto-upgradeable, and user friendly - a true Hosted alternative to the traditional PIM systems.</p>
				</div>

			</div>
			<!-- end #skufactors -->
			
			<!-- begin #home-quote -->
			<div id="home-quote">
			
				<?php if(get_theme_mod('home_quote_more_url')) echo '<a class="more" href="'.get_theme_mod('home_quote_more_url').'">More &raquo;</a>'; ?>
				<h3 class="section-title"><?php echo get_theme_mod('home_quote_title'); ?></h3>
				
				<div id="home-quote-top"></div>
				
				<div id="home-quote-content">
					<?php echo stripslashes(wpautop(get_theme_mod('home_quote_content'))); ?>
				</div>

				<div id="home-quote-bottom">
					<span class="home-quote-author">
						<?php 
							if(get_theme_mod('home_quote_author_url')) echo '<a href="'.get_theme_mod('home_quote_author_url').'">';
							if(get_theme_mod('home_quote_author_name')) echo get_theme_mod('home_quote_author_name'); 
							if(get_theme_mod('home_quote_author_url')) echo '</a>';
						?>
						<?php if(get_theme_mod('home_quote_author_opp')) echo ' - <span class="home-quote-opp">'.get_theme_mod('home_quote_author_opp').'</span>'; ?>
					</span>
				</div>
			</div>
			<!-- end #home-quote -->

			<div id="home-news">
				<h3 class="section-title"><?php echo get_theme_mod('home_news_title'); ?></h3>
				<ul>
				<?php 
					query_posts(array(
						'showposts' => get_theme_mod('home_news_num'),
						'category__not_in' => array(get_theme_mod('folio_cats'))
					));
					if(have_posts()):
					while(have_posts()): the_post(); ?>
						<li>
							<h4><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
							
							<p class="meta">
								<span class="date"><?php the_time(get_option('date_format')); ?></span>
								<span class="author">by <?php the_author(); ?></span> 
								<span class="comments">- <?php comments_popup_link('0 Comments','1 Comments','% Comments'); ?></span>
							</p>
							
							<p class="desc"><?php tj_content_limit(120); ?></p>
						</li><!-- end .post -->
					<?php endwhile;
					endif;
				?>
				</ul>
				<p class="more"><a href="<?php echo get_permalink(get_theme_mod('blog_page')); ?>"><?php _e('More news from our blog &raquo;','themejunkie'); ?></a></p>
			</div><!-- end #home-news -->
	</div><!-- end #sidebar -->

<?php get_footer(); ?>
