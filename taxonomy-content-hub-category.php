<?php
get_header();
session_start();

$tax = get_query_var('term');
$term = get_term_by('slug', $tax, 'content-hub-category');

//$term_name = get_term_by('name', $tax, 'content-hub-category');

$url = $_SERVER['HTTP_REFERER'];
$post_id= url_to_postid( $url );
$_SESSION['page_name_lavel_3_url'] = get_permalink($post_id);
$_SESSION['page_name_lavel_3_term_name'] = $term->name;
$_SESSION['page_name_lavel_3_term_link']=get_term_link($term->slug, 'content-hub-category');
?>

<div id="content">
    <!-- <div class="inner-content blogs-main-wrapper"> -->
    <div class=" dynamic_option_get blog_cat_sec">
        <input type="hidden" name="tax" id="tax" value="<?php echo $term->term_id;?>">
        <div class="container_custom">
           <!--  <div class="row">  -->

            	<?php 
            		$term_thumbnail_id = get_field('add_images_hub','term_'.$term->term_id);
                    $image = wp_get_attachment_image_src($term_thumbnail_id, 'category_img');
                    if (!$term_thumbnail_id) {
                        $image[0] = get_template_directory_uri() . '/images/default_image-353x178.png';
                    }
            	?>
            	<img src="<?php echo $image[0]; ?>">
            	<div class="long_description">
            	<?php echo get_field('add_long_description','term_'.$term->term_id) ?>
            	</div>
                <?php 

                $get_category_list=get_field('select_post_category','term_'.$term->term_id);
                if($get_category_list){   ?> 
            <div class="product_cat_main row " id="post_content">
                <?php 
                
                

// $query_args = array (
//    'post_type' => 'post',
//    'posts_per_page' => -1,
//     'tax_query' => array(
//         array(
//             'taxonomy'  => 'content-hub-category',
//             'field'     => 'term_id',
//             'terms'     => $term->term_id,
//         )
//     ),
// );
  
$query_args = array (
   'post_type' => 'post',
   'post__in'=> $get_category_list,  
   'orderby'   => 'post__in',
); 

$query = new WP_Query($query_args);
                
                if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="blog_detail_content col_4">
                            <?php
                            global $post;
                            $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'post-feature');
                            $img_default = get_template_directory_uri() . '/images/default_image.png';
                            $post_date = get_the_date('j/m/Y');
                            $all_tags = array();
                            ?>
                            <div id="post-<?php the_ID(); ?>" class="blog_category_list">
                                <div class="cat_img">
                                    <?php if ($large_image_url[0]) { ?>
                                       <a href="<?php the_permalink(); ?>"> <img src="<?php echo $large_image_url[0]; ?>"></a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $img_default; ?>"></a>
                                    <?php } ?>
                                </div>
                                <div class="cat_content">
                                    <div class="entry-metadata-info">
                                   
                                        <h4 class="font16"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'themejunkie'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                                        <p><?php 

                                        $content = mb_strimwidth(wp_strip_all_tags(get_the_content()), 0, 250, '');

                                        echo $content; ?></p>
                                    </div>
                                    <div class="entry">
                                        <a href="<?php the_permalink(); ?>" class="tax_post_link button">READ MORE</a>
                                    </div>
                                </div>									
                            </div><!-- end .post --> 
                        </div>
                        <?php
                    endwhile;
                endif;
                ?><?php wp_reset_query();  ?>
                
            </div>
            <!-- end .blog-posts -->
        <?php }else{

            //echo  '<div><h3>No Post found.</h3><div>';

        } ?>
<div class="ext_desc">
             <?php echo get_field('add_description_after_category','term_'.$term->term_id); 

             //$_SESSION['page_name'] = 'content-hub-level-3-sample';
             ?>
         </div>
            

        <!-- </div> -->
    </div><!-- end .inner-content -->
    </div>
</div>
<?php get_footer(); ?>
