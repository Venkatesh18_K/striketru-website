<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" <?php language_attributes( 'xhtml' ); ?>>
<head profile="https://gmpg.org/xfn/11">
	<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/favicon.ico" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://www.striketru.com/wp-content/themes/cubelight/js/fSelect.js"></script>
	
	<?php wp_head(); ?>
	
	<!--begin of header code-->	
		<?php if(get_theme_mod('head_code_status') == "Yes") echo stripslashes(get_theme_mod('head_code')); ?>
	<!--end of header code-->
	
	<!--[if IE]>
		<style type="text/css">
			.tagline,.tagline h2, .tagline a{zoom:1;}
		</style>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.textshadow.js"></script>
		<script type="text/javascript">
			jQuery.noConflict();
			jQuery(document).ready(function($){
				$('.tagline,.tagline h2,.tagline a').textShadow({y: 1,radius: 0,color: '#333'});
			});
			
		</script>
	<![endif]-->
	
	<!--[if lt IE 7]>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pngfix.js"></script>
		<script type="text/javascript">
			DD_belatedPNG.fix('#header,#image-logo a,.light-bg, #topnav .current_page_item a,#topnav .current_page_parent a,#topnav .current_page_ancestor a, #topnav ul li.withul:hover, .swidget img, .social-list li a, #home-about img, .textwidget img, .swidget .more a');
			$(".entry").ImageAutoSize(608);
		</script>
	<![endif]-->

</head>

<body <?php body_class(); ?>>

<div id="wrapper">
	
	<div id="header">
	<div class="top-header">
		<div class="top-header-content">
			<div class="top-header-logo">
				<a href="<?php echo get_home_url(); ?>"> <img src="/wp-content/uploads/2021/07/strile-logo.png" ></a>
			</div>
			
			<div class="top-header-links">
				<a href="tel:+18323033257"><img style="margin-right:7px;"  src="/wp-content/uploads/header/tech-support.png" />+1 832 303 3257</a>
				<a href="mailto:sales@striketru.com"><i style="margin-right:7px;" class="fas fa-paper-plane"></i>sales@striketru.com</a>
			</div>
		</div>
	</div>
	<div class="inner">
	<div class="logo-group">
		<div id="topnav" >	
		<?php 
			$pagesNav = '';
			if (function_exists('wp_nav_menu')) {
				$pagesNav = wp_nav_menu( array( 'theme_location' => 'header-pages', 'menu_class' => 'topnav', 'menu_id' => 'page-nav', 'echo' => false, 'fallback_cb' => '' ) );};
			if ($pagesNav == '') { ?>
			<ul>
				<li<?php if(is_home() || is_front_page()) echo ' class="current_page_item"'; ?>><a href="<?php bloginfo('url'); ?>">Home</a></li>
				<?php wp_list_pages('title_li='); ?>
			</ul>
		<?php }
			else echo($pagesNav); 
		?>
		</div><!-- end #topnav -->
		</div>
		<div class="topnav_btns">
			<a class="button demo_btn" href="/request-demo/">REQUEST DEMO</a>
			
			
		</div>
		
		<div id="custom-search-btn" class="custom-search">
				<a class="custom-search-btn" ><i style="font-weight:600;font-size:24px;" class="fa fa-search" aria-hidden="true"></i></a>
				
				<?php get_search_form(); ?>
		</div>
		<script>
		$(document).ready(function(){
		  $(".custom-search-btn").click(function(){
			$("#searchform").slideToggle(300);
		  });
		  
		 $('html').click(function(e) {   
			if(( !$(e.target).hasClass('fa-search') ) && ( !$(e.target).hasClass('form_control search_input') ) && ( !$(e.target).hasClass('search_btn') ) )
			{
				$("#searchform").slideUp(300);
			}
			});    

		  
		});
		</script>
	</div><!-- end #header .inner -->
	</div><!-- end #header -->

	<?php 
            $blog_category_banner_attr = get_field('blog_category_banner_image', 'option');
            $blog_category_banner_image = wp_get_attachment_image_src($blog_category_banner_attr, 'full');
            
            $blog_detail_banner_attr = get_field('blog_detail_banner_image', 'option');
            $blog_detail_banner_image = wp_get_attachment_image_src($blog_detail_banner_attr, 'full');
        ?>


	<div id="promo" <?php if(is_singular( 'post' )){ ?> style="background-image:url('<?php echo $blog_detail_banner_image[0];?>') !important; " <?php } elseif(is_archive( 'bolg-category' )){?> style="background-image:url('<?php echo $blog_category_banner_image[0];?>')!important; " <?php }?> >
	<div class="inner">
		<div class="light-bg">
	
		<div class="tagline">
			<?php    if(!is_home() && !is_archive( 'bolg-category' ) && !is_singular( 'post' ) && !is_page(4075) ) { ?>
			<div class="main-image"></div>
                 <?php } elseif(is_archive( 'bolg-category' )|| is_singular( 'post' ) || is_page(4075) ) {
                    $ic = get_template_directory_uri() . '/images/blog copy.png'; 
                     ?>
                     <div class="main-image" style="background-image:url('<?php echo $ic;?>'); "></div>
               <?php  }?>
			<?php 
				global $post;
				
				$tagline = '';

				if(is_home()) {
				
					$tagline = get_theme_mod('home_tagline');
				} elseif(is_page()) {
					if ($post->post_parent)
						$parent_page_id = $post->post_parent;
					
					if($post->ancestors)
						$ancestors = end($post->ancestors);
						
					if ($ancestors != $post->post_parent)
						$top_page_id = $ancestors;
					
				//(21-04-20)	$tagline =  get_post_meta($post->ID,'tagline',true);
			// Start Page Banner Title (21-04-20)
					$f_title=get_field('title_1',$post->ID);
					$s_title=get_field('title_2',$post->ID);

					$postname =  get_the_title();

					if($f_title=='' && $s_title == ''){
						// $tagline =  get_post_meta($post->ID,'tagline',true);
						$tagline = '<h1>'.$postname.'</h1>';
					}else{
					
							$tagline='<h1>'.get_field('title_1',$post->ID).'</h1><br><h1>'.get_field('title_2',$post->ID).'</h1>';
					
					}
			// End Page Banner Title (21-04-20)

					if(!$tagline)
						$tagline =  get_post_meta($parent_page_id,'tagline',true);
						
					if(!$tagline)
						$tagline =  get_post_meta($top_page_id,'tagline',true);
					
				} 
 				elseif(is_singular( 'post' )) {
					$postname =  get_the_title();
                                      //  $tagline='<h1>'.$postname.'</h1>';
					// Start Post Banner Title (21-04-20)
                    $f_title=get_field('title_1',$post->ID);
					$s_title=get_field('title_2',$post->ID);

					

					if($f_title=='' && $s_title == ''){
						$tagline='<h1>'.$postname.'</h1>';
					}else{
							$tagline='<h1>'.get_field('title_1',$post->ID).'</h1><br><h1>'.get_field('title_2',$post->ID).'</h1>';
					}
					// End Post Banner Title (21-04-20)



				}
				elseif(is_single()) {
					$tagline =  get_post_meta($post->ID,'tagline',true);
					if(!$tagline) {
						if(tj_in_folio_cat()) {
							$tagline =  get_post_meta(get_theme_mod('folio_page'),'tagline',true);
						} else {
							$tagline =  get_post_meta(get_theme_mod('blog_page'),'tagline',true);
						}
					}
				} elseif(is_category()) {
					$tagline = category_description();
					
					if(!$tagline) {
						if(tj_is_folio_cat()) {
							$tagline =  get_post_meta(get_theme_mod('folio_page'),'tagline',true);
						} else {
							$tagline =  get_post_meta(get_theme_mod('blog_page'),'tagline',true);
						}
					}
				} elseif(is_tag()) {
					$tagline = tag_description();
					if(!$tagline)
						$tagline =  get_post_meta(get_theme_mod('blog_page'),'tagline',true);
					
				} elseif(is_search()) {
					$tagline = get_theme_mod('search_tagline');
				}elseif(is_archive( 'bolg-category' )){
                                   $current_term = single_term_title( "", false );
                                    $tagline = '<h1>'.$current_term.'</h1>';
                              
                }
				 elseif(is_archive()) {
					$tagline = get_theme_mod('archive_tagline');
				} elseif(is_404()) {
					$tagline = get_theme_mod('404_tagline');
				} else {
					$tagline =  '';
				}
				
				if($tagline)
					echo wpautop($tagline);
		 	?>
		</div>

		<?php if(is_home() && get_theme_mod('home_services_status') == 'Yes') { ?>
		<div id="home-services" style="display:none;">
			<div class="swidget" id="swidget-1">
				<h3><img alt="<?php echo get_theme_mod('fs_title_1'); ?>" src="<?php echo get_theme_mod('fs_icon_1'); ?>" /><?php echo get_theme_mod('fs_title_1'); ?></h3>
				<?php echo '<div class="swidget-inner">'.wpautop(get_theme_mod('fs_desc_1')).'</div>'; ?>
				<?php $more_link = get_theme_mod('fs_more_link_1'); 
					if($more_link) 
						echo '<span class="more"><a href="'.$more_link.'">Read More &raquo;</a></span>';
				?>
			</div>
		
			<div class="swidget" id="swidget-2">
				<h3><img alt="<?php echo get_theme_mod('fs_title_2'); ?>" src="<?php echo get_theme_mod('fs_icon_2'); ?>" /><?php echo get_theme_mod('fs_title_2'); ?></h3>
				<?php echo '<div class="swidget-inner">'.wpautop(get_theme_mod('fs_desc_2')).'</div>'; ?>
				<?php $more_link = get_theme_mod('fs_more_link_2'); 
					if($more_link) 
						echo '<span class="more"><a href="'.$more_link.'">Read More &raquo;</a></span>';
				?>
			</div>
		
			<div class="swidget" id="swidget-3">
				<h3><img  alt="<?php echo get_theme_mod('fs_title_3'); ?>" src="<?php echo get_theme_mod('fs_icon_3'); ?>" /><?php echo get_theme_mod('fs_title_3'); ?></h3>
				<?php echo '<div class="swidget-inner">'.wpautop(get_theme_mod('fs_desc_3')).'</div>'; ?>
				<?php $more_link = get_theme_mod('fs_more_link_3'); 
					if($more_link) 
						echo '<span class="more"><a href="'.$more_link.'">Read More &raquo;</a></span>';
				?>
			</div>
		</div>
		<?php }?>

		</div>
	</div><!-- end #promo .inner-->
	</div><!-- end #promo -->
	
	<div id="container">
	
	<div class="inner">
		
			<?php if(!is_home()) { ?>
			<div class="inner-content">
				<div id="toolbar"> 
					<?php if(get_theme_mod('display_portfolio_dropcats') == 'Yes' && (is_page(get_theme_mod('folio_page')) || tj_is_folio_cat())) { ?>
					<div id="category-select">
						<a id="category-toggle" href="#category-dropdown">Select Category &#9660;</a>
						<div id="category-dropdown">
						<?php 
							$cats = get_categories('include='.get_theme_mod('folio_cats')); 
							$output = '<ul>';
							foreach($cats as $cat) {
								$output .=  '<li><a href="'.get_category_link($cat->cat_ID).'">';
								$output .= $cat->cat_name;
								$output .=  '</a>';
								$subcats = get_categories('child_of='.$cat->cat_ID); 
								if($subcats)
									$output .= '<ul>'.wp_list_categories('echo=0&title_li=&child_of='.$cat->cat_ID).'</ul>';
								$output .= '</li>';
							}
							$output .= '</ul>';
							echo $output;
						?>
						</div>
					</div><!-- end .category-select -->
					<?php } ?>
					
					<div id="breadcrumb">
						<?php tj_breadcrumb(); ?>
						
					</div><!-- end #breadcrumb -->
			
				</div><!-- end #toolbar -->
			</div><!-- end .inner-content -->
			<?php } ?>
		<div id="main">
