<?php get_header(); ?>

	<div id="content">
		<div class="inner-content blogs-main-wrapper">
			<div class="blog-posts">
				<?php 
					if(is_page(get_theme_mod('blog_page'))) {
						$paged = get_query_var('paged') ? get_query_var('paged'):1;
						$folio_cat_ids = get_theme_mod('folio_cats');
						$folio_cat_arr = explode(',',$folio_cat_ids);
						query_posts(array(
							'paged' => $paged,
							'category__not_in' => $folio_cat_arr
						));
					}
					
					if (have_posts()) {
						while (have_posts()) : the_post();
						global $post; ?>
						
						<div id="post-<?php the_ID(); ?>" class="post">
									
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'themejunkie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							
							<span class="meta-comments"><!--?php comments_popup_link('0','1','%'); ?--></span>
							
							<div class="entry-meta">
								<span class="meta-date"><?php the_time(get_option('date_format')); ?></span>
								<span class="meta-author">by <?php the_author(); ?></span> 
								<span class="meta-cat">in <?php the_category(', ')?></span> 
							</div>
									
							<div class="entry">
								
								<?php global $more; $more = false; ?>
								<?php the_content('Continue Reading &raquo;',1); ?>
								<?php $more = true; ?>
							</div>
							
						</div><!-- end .post -->
				
				<?php 
						endwhile;
						
						if ( $wp_query->max_num_pages > 1 ) tj_pagenavi();
						wp_reset_query();
					} else { 
						include(TEMPLATEPATH. '/includes/not-found.php'); 
					}
				?>
			</div><!-- end .blog-posts -->
			<div class="blogs-wrapper">
				<?php get_sidebar(); ?>	
			</div>
		</div><!-- end .inner-content -->
	</div><!-- end #content -->


<?php get_footer(); ?>